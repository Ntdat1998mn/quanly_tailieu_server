/*
 Navicat Premium Data Transfer

 Source Server         : 126
 Source Server Type    : MySQL
 Source Server Version : 50560 (5.5.60-MariaDB)
 Source Host           : localhost:3306
 Source Schema         : weosID

 Target Server Type    : MySQL
 Target Server Version : 50560 (5.5.60-MariaDB)
 File Encoding         : 65001

 Date: 02/10/2023 06:55:53
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for company
-- ----------------------------
DROP TABLE IF EXISTS `company`;
CREATE TABLE `company`  (
  `company_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `company_name` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `company_officephone1` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `company_officephone2` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `company_fax` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `company_taxcode` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `company_address` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `company_director` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `company_accountant` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `company_treasurer` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `company_invoice_man1` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `company_invoice_man2` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`company_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of company
-- ----------------------------
INSERT INTO `company` VALUES (1, 'CÔNG TY TNHH NAM ANH', '0961-11-77-11      ', NULL, NULL, 'undefined', 'KHU CN Tân Bình', 'undefined', 'undefined', 'undefined', NULL, NULL);
INSERT INTO `company` VALUES (3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for dvcc_ca_lam_viec
-- ----------------------------
DROP TABLE IF EXISTS `dvcc_ca_lam_viec`;
CREATE TABLE `dvcc_ca_lam_viec`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `gio_bat_dau` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `gio_ket_thuc` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of dvcc_ca_lam_viec
-- ----------------------------
INSERT INTO `dvcc_ca_lam_viec` VALUES (1, 'A', '08:00:00', '12:00:00');
INSERT INTO `dvcc_ca_lam_viec` VALUES (2, 'B', '13:30:00', '17:30:00');
INSERT INTO `dvcc_ca_lam_viec` VALUES (5, 'Cả ngày', '08:00:00', '17:30:00');

-- ----------------------------
-- Table structure for dvcc_cham_cong
-- ----------------------------
DROP TABLE IF EXISTS `dvcc_cham_cong`;
CREATE TABLE `dvcc_cham_cong`  (
  `cham_cong_id` int(11) NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NOT NULL,
  `ngay` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `gio_den` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `gio_di` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`cham_cong_id`) USING BTREE,
  INDEX `nv_id`(`nv_id`) USING BTREE,
  CONSTRAINT `dvcc_cham_cong_ibfk_1` FOREIGN KEY (`nv_id`) REFERENCES `ns_nhanvien` (`nv_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of dvcc_cham_cong
-- ----------------------------
INSERT INTO `dvcc_cham_cong` VALUES (1, 16, '2023-09-08', '15:55:41', NULL);
INSERT INTO `dvcc_cham_cong` VALUES (2, 16, '2023-09-11', '13:53:10', NULL);
INSERT INTO `dvcc_cham_cong` VALUES (3, 16, '2023-09-21', '10:23:42', NULL);
INSERT INTO `dvcc_cham_cong` VALUES (4, 33, '2023-09-29', '08:54:38', NULL);
INSERT INTO `dvcc_cham_cong` VALUES (5, 16, '2023-09-29', '14:43:39', NULL);

-- ----------------------------
-- Table structure for dvcc_chi_nhanh
-- ----------------------------
DROP TABLE IF EXISTS `dvcc_chi_nhanh`;
CREATE TABLE `dvcc_chi_nhanh`  (
  `chi_nhanh_id` int(11) NOT NULL AUTO_INCREMENT,
  `chi_nhanh_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `chi_nhanh_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `latitude` double NULL DEFAULT NULL,
  `longitude` double NULL DEFAULT NULL,
  `chi_nhanh_status` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`chi_nhanh_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of dvcc_chi_nhanh
-- ----------------------------
INSERT INTO `dvcc_chi_nhanh` VALUES (1, 'Trụ sở', '24 Hàm Nghi, Bến Nghé, Quận 1', 10.7741184, 106.7483136, 1);
INSERT INTO `dvcc_chi_nhanh` VALUES (2, 'Chi nhánh', '200 Đường số 9, Phường tân Phú, Quận 7', 10.74673, 106.70972, 1);

-- ----------------------------
-- Table structure for dvcc_chuc_vu
-- ----------------------------
DROP TABLE IF EXISTS `dvcc_chuc_vu`;
CREATE TABLE `dvcc_chuc_vu`  (
  `chuc_vu_id` int(11) NOT NULL AUTO_INCREMENT,
  `chuc_vu_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`chuc_vu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of dvcc_chuc_vu
-- ----------------------------
INSERT INTO `dvcc_chuc_vu` VALUES (1, 'Nhân viên');
INSERT INTO `dvcc_chuc_vu` VALUES (2, 'Giám đốc');

-- ----------------------------
-- Table structure for dvcc_lich_cong_tac
-- ----------------------------
DROP TABLE IF EXISTS `dvcc_lich_cong_tac`;
CREATE TABLE `dvcc_lich_cong_tac`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NOT NULL,
  `noi_dung_cong_tac` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `noi_cong_tac` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ngay_bat_dau` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ngay_ket_thuc` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `quan_ly_truc_tiep` int(11) NULL DEFAULT NULL,
  `nguoi_duyet` int(11) NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '0: da xoa, 1 hoạt động',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `nv_id`(`nv_id`) USING BTREE,
  INDEX `quan_ly_truc_tiep`(`quan_ly_truc_tiep`) USING BTREE,
  INDEX `nguoi_duyet`(`nguoi_duyet`) USING BTREE,
  CONSTRAINT `dvcc_lich_cong_tac_ibfk_1` FOREIGN KEY (`nv_id`) REFERENCES `ns_nhanvien` (`nv_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `dvcc_lich_cong_tac_ibfk_2` FOREIGN KEY (`quan_ly_truc_tiep`) REFERENCES `ns_nhanvien` (`nv_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `dvcc_lich_cong_tac_ibfk_3` FOREIGN KEY (`nguoi_duyet`) REFERENCES `ns_nhanvien` (`nv_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of dvcc_lich_cong_tac
-- ----------------------------
INSERT INTO `dvcc_lich_cong_tac` VALUES (1, 33, 'Đi họp bàn công việc với đối tác', '77/9 Ngô Xương, Quận 5555, tp.HCM', '2023-08-19', '2023-08-25', 21, 20, 4);
INSERT INTO `dvcc_lich_cong_tac` VALUES (2, 33, 'abc', 'aa', '2023-09-17', '2023-09-17', NULL, 12, 3);
INSERT INTO `dvcc_lich_cong_tac` VALUES (3, 33, 'Đi họp bàn công việc với đối tác', '77/9 Ngô Xương, Quận 5555, tp.HCM', '2023-08-19', '2023-08-25', 21, 20, 4);
INSERT INTO `dvcc_lich_cong_tac` VALUES (4, 33, 'Đi họp bàn công việc với đối tác', '77/9 Ngô Xương, Quận 5555, tp.HCM', '2023-09-19', '2023-08-25', 21, 20, 0);

-- ----------------------------
-- Table structure for dvcc_lich_lam_viec_dang_ky
-- ----------------------------
DROP TABLE IF EXISTS `dvcc_lich_lam_viec_dang_ky`;
CREATE TABLE `dvcc_lich_lam_viec_dang_ky`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NOT NULL,
  `ngay` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ca_lam_viec_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '0: da xoa, 1 hoạt động',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `nv_id`(`nv_id`) USING BTREE,
  INDEX `ca_lam_viec_id`(`ca_lam_viec_id`) USING BTREE,
  CONSTRAINT `dvcc_lich_lam_viec_dang_ky_ibfk_2` FOREIGN KEY (`nv_id`) REFERENCES `ns_nhanvien` (`nv_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `dvcc_lich_lam_viec_dang_ky_ibfk_3` FOREIGN KEY (`ca_lam_viec_id`) REFERENCES `dvcc_ca_lam_viec` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of dvcc_lich_lam_viec_dang_ky
-- ----------------------------
INSERT INTO `dvcc_lich_lam_viec_dang_ky` VALUES (1, 33, '2023-08-21', 2, 1);
INSERT INTO `dvcc_lich_lam_viec_dang_ky` VALUES (2, 33, '2023-08-22', 1, 1);
INSERT INTO `dvcc_lich_lam_viec_dang_ky` VALUES (3, 33, '2023-08-23', 1, 1);
INSERT INTO `dvcc_lich_lam_viec_dang_ky` VALUES (4, 33, '2023-08-24', 1, 1);
INSERT INTO `dvcc_lich_lam_viec_dang_ky` VALUES (5, 33, '2023-08-25', 1, 1);
INSERT INTO `dvcc_lich_lam_viec_dang_ky` VALUES (6, 33, '2023-08-26', 1, 1);
INSERT INTO `dvcc_lich_lam_viec_dang_ky` VALUES (7, 33, '2023-08-27', 1, 1);
INSERT INTO `dvcc_lich_lam_viec_dang_ky` VALUES (8, 33, '2023-09-18', 1, 1);
INSERT INTO `dvcc_lich_lam_viec_dang_ky` VALUES (9, 33, '2023-09-19', 2, 1);
INSERT INTO `dvcc_lich_lam_viec_dang_ky` VALUES (10, 33, '2023-09-20', 1, 1);
INSERT INTO `dvcc_lich_lam_viec_dang_ky` VALUES (11, 33, '2023-09-21', 2, 1);
INSERT INTO `dvcc_lich_lam_viec_dang_ky` VALUES (12, 33, '2023-09-22', 1, 1);

-- ----------------------------
-- Table structure for dvcc_lich_lam_viec_duoc_duyet
-- ----------------------------
DROP TABLE IF EXISTS `dvcc_lich_lam_viec_duoc_duyet`;
CREATE TABLE `dvcc_lich_lam_viec_duoc_duyet`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NOT NULL,
  `ngay` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ca_lam_viec_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '0: da xoa, 1 hoạt động',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `nv_id`(`nv_id`) USING BTREE,
  INDEX `ca_lam_viec_id`(`ca_lam_viec_id`) USING BTREE,
  CONSTRAINT `dvcc_lich_lam_viec_duoc_duyet_ibfk_1` FOREIGN KEY (`nv_id`) REFERENCES `ns_nhanvien` (`nv_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `dvcc_lich_lam_viec_duoc_duyet_ibfk_2` FOREIGN KEY (`ca_lam_viec_id`) REFERENCES `dvcc_ca_lam_viec` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of dvcc_lich_lam_viec_duoc_duyet
-- ----------------------------
INSERT INTO `dvcc_lich_lam_viec_duoc_duyet` VALUES (1, 33, '2023-08-21', 2, 1);
INSERT INTO `dvcc_lich_lam_viec_duoc_duyet` VALUES (2, 33, '2023-08-21', 1, 1);

-- ----------------------------
-- Table structure for dvcc_lich_tang_ca
-- ----------------------------
DROP TABLE IF EXISTS `dvcc_lich_tang_ca`;
CREATE TABLE `dvcc_lich_tang_ca`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NOT NULL,
  `ngay_bat_dau` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ngay_ket_thuc` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `thoi_gian_bat_dau` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `thoi_gian_ket_thuc` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `quan_ly_truc_tiep` int(11) NULL DEFAULT NULL,
  `nguoi_duyet` int(11) NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '0: da xoa, 1: đợi duyệt: 2: được duyệt, 3: đã quá hạn ',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `nv_id`(`nv_id`) USING BTREE,
  INDEX `quan_ly_truc_tiep`(`quan_ly_truc_tiep`) USING BTREE,
  INDEX `nguoi_duyet`(`nguoi_duyet`) USING BTREE,
  CONSTRAINT `dvcc_lich_tang_ca_ibfk_1` FOREIGN KEY (`nv_id`) REFERENCES `ns_nhanvien` (`nv_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `dvcc_lich_tang_ca_ibfk_2` FOREIGN KEY (`quan_ly_truc_tiep`) REFERENCES `ns_nhanvien` (`nv_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `dvcc_lich_tang_ca_ibfk_3` FOREIGN KEY (`nguoi_duyet`) REFERENCES `ns_nhanvien` (`nv_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of dvcc_lich_tang_ca
-- ----------------------------
INSERT INTO `dvcc_lich_tang_ca` VALUES (1, 33, '2023-09-17', '2023-09-23', '10:42', '10:43', 15, 12, 4);
INSERT INTO `dvcc_lich_tang_ca` VALUES (2, 33, '2023-09-17', '2023-09-17', '10:48', '10:47', 12, 12, 4);
INSERT INTO `dvcc_lich_tang_ca` VALUES (3, 33, '2023-9-19', '2023-08-25', '16:44:00', '16:44:00', 21, 20, 0);
INSERT INTO `dvcc_lich_tang_ca` VALUES (4, 33, '2023-9-19', '2023-08-25', '16:44:00', '16:44:00', 21, 20, 1);

-- ----------------------------
-- Table structure for dvcc_phep_nghi
-- ----------------------------
DROP TABLE IF EXISTS `dvcc_phep_nghi`;
CREATE TABLE `dvcc_phep_nghi`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NOT NULL,
  `ngay_bat_dau` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ca_bat_dau` int(11) NOT NULL,
  `ngay_ket_thuc` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ca_ket_thuc` int(11) NOT NULL,
  `ly_do` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `quan_ly_truc_tiep` int(11) NULL DEFAULT NULL,
  `nguoi_duyet` int(11) NULL DEFAULT NULL,
  `nguoi_thay_the` int(11) NULL DEFAULT NULL,
  `phepnghi_status` int(11) NOT NULL DEFAULT 1 COMMENT '0 deleted, 1 chua chap thuan, 2 da chap thuan, 3 qua han',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `nv_id`(`nv_id`) USING BTREE,
  INDEX `quan_ly_truc_tiep`(`quan_ly_truc_tiep`) USING BTREE,
  INDEX `nguoi_duyet`(`nguoi_duyet`) USING BTREE,
  INDEX `nguoi_thay_the`(`nguoi_thay_the`) USING BTREE,
  CONSTRAINT `dvcc_phep_nghi_ibfk_1` FOREIGN KEY (`nv_id`) REFERENCES `ns_nhanvien` (`nv_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `dvcc_phep_nghi_ibfk_2` FOREIGN KEY (`quan_ly_truc_tiep`) REFERENCES `ns_nhanvien` (`nv_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `dvcc_phep_nghi_ibfk_3` FOREIGN KEY (`nguoi_duyet`) REFERENCES `ns_nhanvien` (`nv_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `dvcc_phep_nghi_ibfk_4` FOREIGN KEY (`nguoi_thay_the`) REFERENCES `ns_nhanvien` (`nv_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of dvcc_phep_nghi
-- ----------------------------
INSERT INTO `dvcc_phep_nghi` VALUES (1, 33, '2023-09-17', 1, '2023-09-23', 2, 'alo', NULL, 13, NULL, 4);
INSERT INTO `dvcc_phep_nghi` VALUES (2, 33, '2023-09-17', 1, '2023-09-22', 2, 'qqq', NULL, 12, NULL, 3);
INSERT INTO `dvcc_phep_nghi` VALUES (3, 33, '2023-09-23', 1, '2023-09-22', 2, 'Tôi muốn nghỉ', 25, 14, NULL, 4);
INSERT INTO `dvcc_phep_nghi` VALUES (4, 33, '2023-10-15', 1, '2023-10-15', 2, 'Hôm nay tôi bận', 28, 28, NULL, 0);

-- ----------------------------
-- Table structure for dvcc_xin_di_tre
-- ----------------------------
DROP TABLE IF EXISTS `dvcc_xin_di_tre`;
CREATE TABLE `dvcc_xin_di_tre`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NOT NULL,
  `ngay` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `gio_di_tre` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ly_do` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `quan_ly_truc_tiep` int(11) NULL DEFAULT NULL,
  `nguoi_duyet` int(11) NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '0: da xoa, 1: đợi duyệt: 2: được duyệt, 3: đã quá hạn ',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `nv_id`(`nv_id`) USING BTREE,
  INDEX `quan_ly_truc_tiep`(`quan_ly_truc_tiep`) USING BTREE,
  INDEX `nguoi_duyet`(`nguoi_duyet`) USING BTREE,
  CONSTRAINT `dvcc_xin_di_tre_ibfk_1` FOREIGN KEY (`nv_id`) REFERENCES `ns_nhanvien` (`nv_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `dvcc_xin_di_tre_ibfk_2` FOREIGN KEY (`quan_ly_truc_tiep`) REFERENCES `ns_nhanvien` (`nv_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `dvcc_xin_di_tre_ibfk_3` FOREIGN KEY (`nguoi_duyet`) REFERENCES `ns_nhanvien` (`nv_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of dvcc_xin_di_tre
-- ----------------------------
INSERT INTO `dvcc_xin_di_tre` VALUES (1, 16, '', '09:47', '', NULL, NULL, 1);
INSERT INTO `dvcc_xin_di_tre` VALUES (2, 33, '', '', '', NULL, NULL, 1);
INSERT INTO `dvcc_xin_di_tre` VALUES (3, 33, '2023-09-17', '', 'zxcx', NULL, NULL, 1);
INSERT INTO `dvcc_xin_di_tre` VALUES (4, 16, '', '', '', NULL, NULL, 1);
INSERT INTO `dvcc_xin_di_tre` VALUES (5, 16, '', '', '', NULL, NULL, 1);
INSERT INTO `dvcc_xin_di_tre` VALUES (6, 16, '', '', '', NULL, NULL, 1);
INSERT INTO `dvcc_xin_di_tre` VALUES (7, 16, '2023-09-23', '10:26', 'Abc', NULL, NULL, 1);
INSERT INTO `dvcc_xin_di_tre` VALUES (8, 33, '2023-09-28', '03:55:03', '134545345', NULL, NULL, 1);
INSERT INTO `dvcc_xin_di_tre` VALUES (9, 16, '', '16:44', 'tetsing', NULL, NULL, 1);

-- ----------------------------
-- Table structure for dvcc_xin_ve_som
-- ----------------------------
DROP TABLE IF EXISTS `dvcc_xin_ve_som`;
CREATE TABLE `dvcc_xin_ve_som`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NOT NULL,
  `ngay` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `gio_ve_som` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ly_do` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `quan_ly_truc_tiep` int(11) NULL DEFAULT NULL,
  `nguoi_duyet` int(11) NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '0: da xoa, 1: đợi duyệt: 2: được duyệt, 3: đã quá hạn ',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `nv_id`(`nv_id`) USING BTREE,
  INDEX `quan_ly_truc_tiep`(`quan_ly_truc_tiep`) USING BTREE,
  INDEX `nguoi_duyet`(`nguoi_duyet`) USING BTREE,
  CONSTRAINT `dvcc_xin_ve_som_ibfk_1` FOREIGN KEY (`nv_id`) REFERENCES `ns_nhanvien` (`nv_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `dvcc_xin_ve_som_ibfk_2` FOREIGN KEY (`quan_ly_truc_tiep`) REFERENCES `ns_nhanvien` (`nv_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `dvcc_xin_ve_som_ibfk_3` FOREIGN KEY (`nguoi_duyet`) REFERENCES `ns_nhanvien` (`nv_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of dvcc_xin_ve_som
-- ----------------------------
INSERT INTO `dvcc_xin_ve_som` VALUES (1, 33, '2023-09-07', '09:56', 'â', NULL, NULL, 1);

-- ----------------------------
-- Table structure for kiosk_user_permissions
-- ----------------------------
DROP TABLE IF EXISTS `kiosk_user_permissions`;
CREATE TABLE `kiosk_user_permissions`  (
  `perms_id` int(11) NOT NULL AUTO_INCREMENT,
  `perms_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `perms_type` varchar(222) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0',
  `user_id` int(11) NULL DEFAULT 0,
  PRIMARY KEY (`perms_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 140 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of kiosk_user_permissions
-- ----------------------------
INSERT INTO `kiosk_user_permissions` VALUES (52, 'NHẬP HÀNG', 'KHOTONG', 1);
INSERT INTO `kiosk_user_permissions` VALUES (53, 'XUẤT HÀNG', 'KHOTONG', 1);
INSERT INTO `kiosk_user_permissions` VALUES (54, 'KIỂM KHO', 'KHOTONG', 1);
INSERT INTO `kiosk_user_permissions` VALUES (55, 'SỔ QUỸ', 'KHOTONG', 1);
INSERT INTO `kiosk_user_permissions` VALUES (56, 'HIỆU QUẢ', 'KHOTONG', 1);
INSERT INTO `kiosk_user_permissions` VALUES (57, 'ĐỊNH DANH ĐẠI LÝ', 'KHOTONG', 1);
INSERT INTO `kiosk_user_permissions` VALUES (58, 'ĐỊNH DANH TÊN HÀNG', 'KHOTONG', 1);
INSERT INTO `kiosk_user_permissions` VALUES (59, 'ĐỊNH DANH TÊN KHO', 'KHOTONG', 1);
INSERT INTO `kiosk_user_permissions` VALUES (60, 'THANH TOÁN TIỀN ĐẠI LÝ', 'SOQUY', 1);
INSERT INTO `kiosk_user_permissions` VALUES (61, 'TÍNH TIỀN NỘP PHƠI NV', 'SOQUY', 1);
INSERT INTO `kiosk_user_permissions` VALUES (62, 'XEM ĐẶT HÀNG', 'SOQUY', 1);
INSERT INTO `kiosk_user_permissions` VALUES (63, 'XEM CÔNG NỢ ĐẠI LÝ', 'SOQUY', 1);
INSERT INTO `kiosk_user_permissions` VALUES (64, 'XEM TỔNG HỢP THU CHI', 'SOQUY', 1);
INSERT INTO `kiosk_user_permissions` VALUES (79, 'QUẢN LÝ HỢP ĐỒNG', 'INTELPLANNING', 1);
INSERT INTO `kiosk_user_permissions` VALUES (82, 'XEM BÁO CÁO CÔNG VIỆC', 'INTELPLANNING', 1);
INSERT INTO `kiosk_user_permissions` VALUES (83, 'XEM BÁO CÁO DOANH THU', 'INTELPLANNING', 1);
INSERT INTO `kiosk_user_permissions` VALUES (84, 'XEM BÁO CÁO CÔNG NỢ', 'INTELPLANNING', 1);
INSERT INTO `kiosk_user_permissions` VALUES (85, 'NHÂN VIÊN BÁO CÁO (GIAO)', 'INTELPLANNING', 1);
INSERT INTO `kiosk_user_permissions` VALUES (137, 'QUẢN LÝ GIAO VIỆC', 'INTELPLANNING', 1);
INSERT INTO `kiosk_user_permissions` VALUES (138, 'QUẢN LÝ THANH TOÁN', 'INTELPLANNING', 1);
INSERT INTO `kiosk_user_permissions` VALUES (139, 'NHÂN VIÊN BÁO CÁO (NHẬN)', 'INTELPLANNING', 1);

-- ----------------------------
-- Table structure for ns_cauhinh
-- ----------------------------
DROP TABLE IF EXISTS `ns_cauhinh`;
CREATE TABLE `ns_cauhinh`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `kyhieu` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `giatri` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT NULL,
  `hople` int(11) NULL DEFAULT NULL,
  `update_new` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_cauhinh
-- ----------------------------
INSERT INTO `ns_cauhinh` VALUES (1, 'Giờ Công Chuẩn', 'GCN', '8', NULL, NULL, NULL);
INSERT INTO `ns_cauhinh` VALUES (2, 'Công Thức Lương 1 Giờ', 'L1G', '@LCT@/(@TDAY@*8)', 1, NULL, '@LCT@/(30*8)');
INSERT INTO `ns_cauhinh` VALUES (3, 'Công Ty Đóng (BHXH)', '', '35', NULL, NULL, NULL);
INSERT INTO `ns_cauhinh` VALUES (4, 'Tự Đóng(BHXH)', NULL, '15', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for ns_danhmuc_phongban
-- ----------------------------
DROP TABLE IF EXISTS `ns_danhmuc_phongban`;
CREATE TABLE `ns_danhmuc_phongban`  (
  `danhmuc_id` int(11) NOT NULL AUTO_INCREMENT,
  `danhmuc_name` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `danhmuc_phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `danhmuc_order` int(11) NULL DEFAULT NULL,
  `danhmuc_status` int(11) NULL DEFAULT 1,
  `danhmuc_type` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `danhmuc_mota` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `danhmuc_chamcong` int(11) NOT NULL,
  `company_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `company` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`danhmuc_id`) USING BTREE,
  INDEX `company_id`(`company_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_danhmuc_phongban
-- ----------------------------
INSERT INTO `ns_danhmuc_phongban` VALUES (1, 'Ban Lãnh Đạo', NULL, 3, 2, 'VP', NULL, 0, NULL, NULL);
INSERT INTO `ns_danhmuc_phongban` VALUES (2, 'Phòng Hành Chánh Nhân Sự', NULL, 3, 2, 'VP', NULL, 1, NULL, NULL);
INSERT INTO `ns_danhmuc_phongban` VALUES (3, 'Phòng Kế Hoạch Tài Chính', NULL, 4, 2, 'VP', NULL, 1, NULL, NULL);
INSERT INTO `ns_danhmuc_phongban` VALUES (4, 'Phòng Dịch Vụ Quan Trắc Môi Trường', NULL, 5, 2, 'VP', NULL, 1, NULL, NULL);
INSERT INTO `ns_danhmuc_phongban` VALUES (6, 'Ban Lãnh Đạo', NULL, 3, 2, 'VP', NULL, 0, NULL, NULL);
INSERT INTO `ns_danhmuc_phongban` VALUES (7, 'Phòng Hành Chính Nhân sự', NULL, 4, 2, 'VP', NULL, 0, NULL, NULL);
INSERT INTO `ns_danhmuc_phongban` VALUES (8, 'Phòng Kế Toán Tài Chính', NULL, 5, 2, 'VP', NULL, 0, NULL, NULL);
INSERT INTO `ns_danhmuc_phongban` VALUES (9, 'Phòng Dịch Vụ Quan Trắc Môi Trường', NULL, 6, 2, 'VP', NULL, 0, NULL, NULL);
INSERT INTO `ns_danhmuc_phongban` VALUES (10, 'Ban Lãnh Đạo', NULL, 1, 1, 'VP', NULL, 1, 1, 1);
INSERT INTO `ns_danhmuc_phongban` VALUES (11, 'Phòng KT Tài Chính', NULL, 2, 1, 'VP', NULL, 1, 1, 1);
INSERT INTO `ns_danhmuc_phongban` VALUES (12, 'Phòng HC Nhân Sự', NULL, 3, 1, 'VP', NULL, 1, 1, 1);
INSERT INTO `ns_danhmuc_phongban` VALUES (13, 'Phòng DV Quan Trắc MT', NULL, 4, 1, 'VP', NULL, 1, 1, 1);

-- ----------------------------
-- Table structure for ns_danhmuc_phongban_excel
-- ----------------------------
DROP TABLE IF EXISTS `ns_danhmuc_phongban_excel`;
CREATE TABLE `ns_danhmuc_phongban_excel`  (
  `danhmuc_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `danhmuc_name` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `danhmuc_phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `danhmuc_order` int(11) NULL DEFAULT NULL,
  `danhmuc_status` int(11) NULL DEFAULT 1,
  `danhmuc_type` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `row_number` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`danhmuc_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_danhmuc_phongban_excel
-- ----------------------------

-- ----------------------------
-- Table structure for ns_dotbhxh
-- ----------------------------
DROP TABLE IF EXISTS `ns_dotbhxh`;
CREATE TABLE `ns_dotbhxh`  (
  `bhxh_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `bhxh_name` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `bhxh_month` date NULL DEFAULT NULL,
  `bhxh_status` int(11) NULL DEFAULT NULL,
  `bhxh_order` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`bhxh_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_dotbhxh
-- ----------------------------
INSERT INTO `ns_dotbhxh` VALUES (1, 'BHXH THÁNG MỚI', '2023-08-01', 1, 100);

-- ----------------------------
-- Table structure for ns_dotbhxh_nvthamgia
-- ----------------------------
DROP TABLE IF EXISTS `ns_dotbhxh_nvthamgia`;
CREATE TABLE `ns_dotbhxh_nvthamgia`  (
  `thamgia_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NULL DEFAULT NULL,
  `bhxh_id` int(11) NULL DEFAULT NULL,
  `nv_name` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `bhxh_chuthich` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `bhxh_ngaythamgia` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `bhxh_sosobhxh` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `bhxh_luongdongbhxh` int(11) NULL DEFAULT 0,
  `bhxh_congtydong` int(11) NULL DEFAULT 0,
  `bhxh_nhanviendong` int(11) NULL DEFAULT 0,
  PRIMARY KEY (`thamgia_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_dotbhxh_nvthamgia
-- ----------------------------
INSERT INTO `ns_dotbhxh_nvthamgia` VALUES (1, 16, 1, 'Nguyễn Thị Trang', NULL, NULL, NULL, 0, 0, 0);
INSERT INTO `ns_dotbhxh_nvthamgia` VALUES (2, 18, 1, 'Nguyễn Thị Thúy Vi', NULL, NULL, NULL, 0, 0, 0);

-- ----------------------------
-- Table structure for ns_dotcongdoan
-- ----------------------------
DROP TABLE IF EXISTS `ns_dotcongdoan`;
CREATE TABLE `ns_dotcongdoan`  (
  `congdoan_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `congdoan_name` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `congdoan_month` date NULL DEFAULT NULL,
  `congdoan_status` int(11) NULL DEFAULT NULL,
  `congdoan_order` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`congdoan_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_dotcongdoan
-- ----------------------------

-- ----------------------------
-- Table structure for ns_dotcongdoan_nvthamgia
-- ----------------------------
DROP TABLE IF EXISTS `ns_dotcongdoan_nvthamgia`;
CREATE TABLE `ns_dotcongdoan_nvthamgia`  (
  `thamgia_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NULL DEFAULT NULL,
  `congdoan_id` int(11) NULL DEFAULT NULL,
  `nv_name` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `congdoan_chuthich` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `congdoan_ngaythamgia` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `congdoan_sosocongdoan` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `congdoan_luongdongcongdoan` int(11) NULL DEFAULT 0,
  `congdoan_congtydong` int(11) NULL DEFAULT 0,
  `congdoan_nhanviendong` int(11) NULL DEFAULT 0,
  PRIMARY KEY (`thamgia_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_dotcongdoan_nvthamgia
-- ----------------------------

-- ----------------------------
-- Table structure for ns_dotksk
-- ----------------------------
DROP TABLE IF EXISTS `ns_dotksk`;
CREATE TABLE `ns_dotksk`  (
  `ksk_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ksk_name` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ksk_benhvien` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ksk_date` date NULL DEFAULT NULL,
  `ksk_status` int(11) NULL DEFAULT NULL,
  `ksk_order` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`ksk_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_dotksk
-- ----------------------------

-- ----------------------------
-- Table structure for ns_dotksk_nvthamgia
-- ----------------------------
DROP TABLE IF EXISTS `ns_dotksk_nvthamgia`;
CREATE TABLE `ns_dotksk_nvthamgia`  (
  `thamgia_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NULL DEFAULT NULL,
  `nv_name` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ksk_id` int(11) NULL DEFAULT NULL,
  `ksk_ketquakiemtra` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`thamgia_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_dotksk_nvthamgia
-- ----------------------------

-- ----------------------------
-- Table structure for ns_dotnophoi
-- ----------------------------
DROP TABLE IF EXISTS `ns_dotnophoi`;
CREATE TABLE `ns_dotnophoi`  (
  `nophoi_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nophoi_name` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nophoi_month` date NULL DEFAULT NULL,
  `nophoi_sotien` int(11) NULL DEFAULT NULL,
  `nophoi_status` int(11) NULL DEFAULT NULL,
  `nophoi_order` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`nophoi_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_dotnophoi
-- ----------------------------

-- ----------------------------
-- Table structure for ns_dotnophoi_nvthamgia
-- ----------------------------
DROP TABLE IF EXISTS `ns_dotnophoi_nvthamgia`;
CREATE TABLE `ns_dotnophoi_nvthamgia`  (
  `thamgia_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NULL DEFAULT NULL,
  `nophoi_id` int(11) NULL DEFAULT NULL,
  `nv_name` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nophoi_chuthich` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nophoi_sotien` int(11) NULL DEFAULT 0,
  PRIMARY KEY (`thamgia_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_dotnophoi_nvthamgia
-- ----------------------------

-- ----------------------------
-- Table structure for ns_dotphat
-- ----------------------------
DROP TABLE IF EXISTS `ns_dotphat`;
CREATE TABLE `ns_dotphat`  (
  `phat_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `phat_name` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phat_month` date NULL DEFAULT NULL,
  `phat_sotien` int(11) NULL DEFAULT NULL,
  `phat_status` int(11) NULL DEFAULT NULL,
  `phat_order` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`phat_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_dotphat
-- ----------------------------

-- ----------------------------
-- Table structure for ns_dotphat_nvthamgia
-- ----------------------------
DROP TABLE IF EXISTS `ns_dotphat_nvthamgia`;
CREATE TABLE `ns_dotphat_nvthamgia`  (
  `thamgia_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NULL DEFAULT NULL,
  `phat_id` int(11) NULL DEFAULT NULL,
  `nv_name` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phat_chuthich` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phat_sotien` int(11) NULL DEFAULT 0,
  PRIMARY KEY (`thamgia_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_dotphat_nvthamgia
-- ----------------------------

-- ----------------------------
-- Table structure for ns_dottaphuan
-- ----------------------------
DROP TABLE IF EXISTS `ns_dottaphuan`;
CREATE TABLE `ns_dottaphuan`  (
  `taphuan_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `taphuan_name` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `taphuan_date` date NULL DEFAULT NULL,
  `taphuan_status` int(11) NULL DEFAULT NULL,
  `taphuan_order` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`taphuan_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_dottaphuan
-- ----------------------------

-- ----------------------------
-- Table structure for ns_dottaphuan_nvthamgia
-- ----------------------------
DROP TABLE IF EXISTS `ns_dottaphuan_nvthamgia`;
CREATE TABLE `ns_dottaphuan_nvthamgia`  (
  `thamgiath_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NULL DEFAULT NULL,
  `nv_name` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `taphuan_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`thamgiath_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_dottaphuan_nvthamgia
-- ----------------------------

-- ----------------------------
-- Table structure for ns_dotthuong
-- ----------------------------
DROP TABLE IF EXISTS `ns_dotthuong`;
CREATE TABLE `ns_dotthuong`  (
  `thuong_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `thuong_name` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `thuong_month` date NULL DEFAULT NULL,
  `thuong_sotien` int(11) NULL DEFAULT NULL,
  `thuong_status` int(11) NULL DEFAULT NULL,
  `thuong_order` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`thuong_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_dotthuong
-- ----------------------------

-- ----------------------------
-- Table structure for ns_dotthuong_nvthamgia
-- ----------------------------
DROP TABLE IF EXISTS `ns_dotthuong_nvthamgia`;
CREATE TABLE `ns_dotthuong_nvthamgia`  (
  `thamgia_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NULL DEFAULT NULL,
  `thuong_id` int(11) NULL DEFAULT NULL,
  `nv_name` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `thuong_chuthich` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `thuong_sotien` int(11) NULL DEFAULT 0,
  PRIMARY KEY (`thamgia_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_dotthuong_nvthamgia
-- ----------------------------

-- ----------------------------
-- Table structure for ns_dotungluong
-- ----------------------------
DROP TABLE IF EXISTS `ns_dotungluong`;
CREATE TABLE `ns_dotungluong`  (
  `ung_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ung_name` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ung_month` date NULL DEFAULT NULL,
  `ung_sotien` int(11) NULL DEFAULT NULL,
  `ung_status` int(11) NULL DEFAULT NULL,
  `ung_order` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`ung_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_dotungluong
-- ----------------------------

-- ----------------------------
-- Table structure for ns_dotungluong_nvthamgia
-- ----------------------------
DROP TABLE IF EXISTS `ns_dotungluong_nvthamgia`;
CREATE TABLE `ns_dotungluong_nvthamgia`  (
  `thamgia_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NULL DEFAULT NULL,
  `ung_id` int(11) NULL DEFAULT NULL,
  `nv_name` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ung_chuthich` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ung_sotien` int(11) NULL DEFAULT 0,
  PRIMARY KEY (`thamgia_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_dotungluong_nvthamgia
-- ----------------------------

-- ----------------------------
-- Table structure for ns_kyquy_chitiet
-- ----------------------------
DROP TABLE IF EXISTS `ns_kyquy_chitiet`;
CREATE TABLE `ns_kyquy_chitiet`  (
  `chitiet_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NULL DEFAULT NULL,
  `nv_name` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `chitiet_ngaythang` datetime NULL DEFAULT NULL,
  `chitiet_sotien` int(11) NULL DEFAULT 0,
  `chitiet_lydo` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `chitiet_status` int(11) NULL DEFAULT NULL,
  `chitiet_nguoithutien` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `chitiet_chucvu` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `chitiet_inputdate` datetime NULL DEFAULT NULL,
  `chitiet_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`chitiet_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_kyquy_chitiet
-- ----------------------------

-- ----------------------------
-- Table structure for ns_kyquy_giaichi
-- ----------------------------
DROP TABLE IF EXISTS `ns_kyquy_giaichi`;
CREATE TABLE `ns_kyquy_giaichi`  (
  `giaichi_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NULL DEFAULT NULL,
  `giaichi_ngaythang` datetime NULL DEFAULT NULL,
  `giaichi_sotien` int(11) NULL DEFAULT 0,
  `giaichi_lydo` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `giaichi_status` int(11) NULL DEFAULT NULL,
  `giaichi_nguoithutien` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `giaichi_chucvu` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `giaichi_inputdate` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`giaichi_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_kyquy_giaichi
-- ----------------------------

-- ----------------------------
-- Table structure for ns_kyquy_nv
-- ----------------------------
DROP TABLE IF EXISTS `ns_kyquy_nv`;
CREATE TABLE `ns_kyquy_nv`  (
  `kyquy_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NULL DEFAULT NULL,
  `nv_name` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `kyquy_so_tien_phai_nop` int(11) NULL DEFAULT 0,
  `kyquy_so_tien_da_ky_quy` int(11) NULL DEFAULT 0,
  `kyquy_so_tien_con_lai` int(11) NULL DEFAULT 0,
  `kyquy_ghi_chu` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `thuc_hien_giai_chi` int(11) NULL DEFAULT NULL,
  `kyquy_status` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`kyquy_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_kyquy_nv
-- ----------------------------

-- ----------------------------
-- Table structure for ns_kyquyvp_chitiet
-- ----------------------------
DROP TABLE IF EXISTS `ns_kyquyvp_chitiet`;
CREATE TABLE `ns_kyquyvp_chitiet`  (
  `chitiet_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NULL DEFAULT NULL,
  `nv_name` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `chitiet_ngaythang` datetime NULL DEFAULT NULL,
  `chitiet_sotien` int(11) NULL DEFAULT 0,
  `chitiet_lydo` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `chitiet_status` int(11) NULL DEFAULT NULL,
  `chitiet_nguoithutien` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `chitiet_chucvu` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `chitiet_inputdate` datetime NULL DEFAULT NULL,
  `chitiet_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`chitiet_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_kyquyvp_chitiet
-- ----------------------------

-- ----------------------------
-- Table structure for ns_kyquyvp_giaichi
-- ----------------------------
DROP TABLE IF EXISTS `ns_kyquyvp_giaichi`;
CREATE TABLE `ns_kyquyvp_giaichi`  (
  `giaichi_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NULL DEFAULT NULL,
  `giaichi_ngaythang` datetime NULL DEFAULT NULL,
  `giaichi_sotien` int(11) NULL DEFAULT 0,
  `giaichi_lydo` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `giaichi_status` int(11) NULL DEFAULT NULL,
  `giaichi_nguoithutien` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `giaichi_chucvu` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `giaichi_inputdate` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`giaichi_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_kyquyvp_giaichi
-- ----------------------------

-- ----------------------------
-- Table structure for ns_kyquyvp_nv
-- ----------------------------
DROP TABLE IF EXISTS `ns_kyquyvp_nv`;
CREATE TABLE `ns_kyquyvp_nv`  (
  `kyquy_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NULL DEFAULT NULL,
  `nv_name` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `kyquy_so_tien_da_ky_quy` int(11) NULL DEFAULT 0,
  `kyquy_so_tien_con_lai` int(11) NULL DEFAULT 0,
  `kyquy_ghi_chu` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `thuc_hien_giai_chi` int(11) NULL DEFAULT NULL,
  `kyquy_status` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`kyquy_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_kyquyvp_nv
-- ----------------------------

-- ----------------------------
-- Table structure for ns_luongcodinh_thongso
-- ----------------------------
DROP TABLE IF EXISTS `ns_luongcodinh_thongso`;
CREATE TABLE `ns_luongcodinh_thongso`  (
  `thongso_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `fMonth` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `fYear` varchar(4) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `danhmuc_id` int(11) NULL DEFAULT 0,
  `thongso_luongcodinh` int(11) NULL DEFAULT 0,
  `thongso_luong1ngaycong` int(11) NULL DEFAULT 0,
  `thongso_luong1giolamthem` int(11) NULL DEFAULT 0,
  `thongso_phucaptrachnhiem` int(11) NULL DEFAULT 0,
  `thongso_phucaptienxang` int(11) NULL DEFAULT 0,
  `thongso_phucaptiencomtheongay` int(11) NULL DEFAULT 0,
  `thongso_phucaptiencomtheongay_tien` double(11, 2) NULL DEFAULT 0.00,
  `thongso_phucapdienthoai` int(11) NULL DEFAULT 0,
  `thongso_phucapcongtacphi` int(11) NULL DEFAULT NULL,
  `thongso_phucapchuyencan` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `thongso_phucapchuyencan_songay` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `thongso_phucapchuyencan_sogio` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `thongso_phucapchuyencan_mucthuong` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `thongso_baohiemxahoi` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `thongso_thuong` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `thongso_phat` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `thongso_kyquytamung` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `thongso_trunophoi` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `thongso_mcc` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`thongso_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 18 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_luongcodinh_thongso
-- ----------------------------
INSERT INTO `ns_luongcodinh_thongso` VALUES (14, '9', '2020', 28, 0, 0, 0, 0, 0, 0, 0.00, 0, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, 1);
INSERT INTO `ns_luongcodinh_thongso` VALUES (13, '8', '2020', 28, 1, 0, 0, 0, 0, 0, 0.00, 0, 0, '0', NULL, NULL, NULL, '0', '0', '0', '0', NULL, 1);
INSERT INTO `ns_luongcodinh_thongso` VALUES (12, '8', '2020', 1, 1, 0, 0, 0, 0, 0, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `ns_luongcodinh_thongso` VALUES (11, '7', '2020', 2, 0, 0, 0, 0, 0, 0, 0.00, 0, 0, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_luongcodinh_thongso` VALUES (10, '7', '2020', 1, 0, 0, 0, 0, 0, 0, 0.00, 0, 0, '0', NULL, NULL, NULL, '0', '0', '0', '0', NULL, NULL);
INSERT INTO `ns_luongcodinh_thongso` VALUES (16, '10', '2020', 29, 0, 0, 0, 0, 0, 0, 0.00, 0, NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_luongcodinh_thongso` VALUES (15, '9', '2020', 30, 0, 0, 0, 0, 0, 0, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `ns_luongcodinh_thongso` VALUES (17, '7', '2023', 28, 0, 1000000, 0, 1, 1, 1, 0.00, 1, 1, '1', NULL, NULL, NULL, '1', '1', '1', '1', NULL, 0);

-- ----------------------------
-- Table structure for ns_maychamcong
-- ----------------------------
DROP TABLE IF EXISTS `ns_maychamcong`;
CREATE TABLE `ns_maychamcong`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NULL DEFAULT NULL,
  `time_cc` datetime NULL DEFAULT NULL,
  `status` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_maychamcong
-- ----------------------------
INSERT INTO `ns_maychamcong` VALUES (1, 12, '2023-08-01 06:00:00', 'I');
INSERT INTO `ns_maychamcong` VALUES (2, 12, '2023-08-01 10:00:00', 'BO');
INSERT INTO `ns_maychamcong` VALUES (3, 12, '2023-08-02 01:01:00', 'delete_I');
INSERT INTO `ns_maychamcong` VALUES (4, 12, '2023-08-02 01:01:00', 'delete_BO');
INSERT INTO `ns_maychamcong` VALUES (5, 12, '2023-08-02 01:00:00', 'delete_I');
INSERT INTO `ns_maychamcong` VALUES (6, 12, '2023-08-02 01:00:00', 'delete_BO');
INSERT INTO `ns_maychamcong` VALUES (7, 13, '2023-08-01 07:00:00', 'I');
INSERT INTO `ns_maychamcong` VALUES (8, 13, '2023-08-01 07:00:00', 'BO');
INSERT INTO `ns_maychamcong` VALUES (9, 14, '2023-08-01 06:00:00', 'I');
INSERT INTO `ns_maychamcong` VALUES (10, 14, '2023-08-01 10:00:00', 'BO');

-- ----------------------------
-- Table structure for ns_nhanvien
-- ----------------------------
DROP TABLE IF EXISTS `ns_nhanvien`;
CREATE TABLE `ns_nhanvien`  (
  `nv_id` int(11) NOT NULL AUTO_INCREMENT,
  `nv_name` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `danhmuc_id` int(11) NULL DEFAULT NULL,
  `nv_order` int(11) NULL DEFAULT NULL,
  `nv_status` int(11) NULL DEFAULT 1,
  `nv_cmt` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_chucvu` int(11) NULL DEFAULT NULL,
  `nv_ngaysinh` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_sex` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_ngayvaolam` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_luong_codinh` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_phucap_trachnhiem` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_sdt_lienhe` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_sdt_khancap` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_nguyenvong` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_motacongviec` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `nv_lylich` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `nv_tinhtranghonnhan` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_noithuongtru` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `nv_noitamtru` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_cha` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_me` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_anhem` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `nv_vochong` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `nv_concai` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `nv_loai_hopdong` int(11) NULL DEFAULT 1,
  `nv_thoihan_hopdong` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_cmnd_ngaycap` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_cmnd_noicap` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_noisinh` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_diachithuongtru` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_khenthuong` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_kyluat` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_diachitamtru` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_cmnd_truoc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_cmnd_sau` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_username` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_password` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_chinhanh` int(11) NULL DEFAULT NULL,
  `nv_calamviec` int(11) NULL DEFAULT NULL,
  `nv_email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_ngayphep` float NULL DEFAULT NULL,
  PRIMARY KEY (`nv_id`) USING BTREE,
  INDEX `ns_nhanvien_ibfk_4`(`nv_calamviec`) USING BTREE,
  INDEX `ns_nhanvien_ibfk_1`(`nv_chinhanh`) USING BTREE,
  INDEX `ns_nhanvien_ibfk_5`(`nv_chucvu`) USING BTREE,
  INDEX `danhmuc_id`(`danhmuc_id`) USING BTREE,
  CONSTRAINT `danhmuc_id` FOREIGN KEY (`danhmuc_id`) REFERENCES `ns_danhmuc_phongban` (`danhmuc_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ns_nhanvien_ibfk_1` FOREIGN KEY (`nv_chinhanh`) REFERENCES `dvcc_chi_nhanh` (`chi_nhanh_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ns_nhanvien_ibfk_5` FOREIGN KEY (`nv_chucvu`) REFERENCES `dvcc_chuc_vu` (`chuc_vu_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 34 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_nhanvien
-- ----------------------------
INSERT INTO `ns_nhanvien` VALUES (1, 'Nguyễn Văn Thường', 1, 1000, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (2, 'Nhân viên mới', 1, 1000, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (3, NULL, 1, 1000, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (4, NULL, 1, 1000, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (5, NULL, 1, 1000, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (6, NULL, 1, 1000, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (7, NULL, 1, 1000, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (8, NULL, 1, 1000, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (9, NULL, 1, 1000, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (10, NULL, 1, 1000, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (11, NULL, 1, 1000, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (12, 'Nguyễn Văn Thường', 10, 1000, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (13, 'Võ Anh Triết', 10, 1000, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (14, 'Hoàng Hoài', 10, 1000, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (15, 'Nguyễn Huỳnh Thùy Trang', 10, 1000, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (16, 'Nguyễn Thị Trang', 11, 1000, 1, NULL, 1, NULL, NULL, '01/12/2022', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'navitek.nttrang10', '1234trang!', 2, 0, NULL, 4);
INSERT INTO `ns_nhanvien` VALUES (17, 'Hồ Hữu Hoành', 12, 1000, 1, NULL, 1, NULL, NULL, '01/03/2021', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'hhhoanh1', '1234', 1, 0, NULL, 5.5);
INSERT INTO `ns_nhanvien` VALUES (18, 'Nguyễn Thị Thúy Vi', 12, 1000, 1, NULL, 1, NULL, NULL, '13/02/2023', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'nttvi12', '1234', 1, 0, NULL, 1);
INSERT INTO `ns_nhanvien` VALUES (19, 'Nguyễn Tuấn Huy', 13, 1000, 1, NULL, 1, NULL, NULL, '01/03/2021', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'nthuy4', '1234', 1, 0, NULL, 4);
INSERT INTO `ns_nhanvien` VALUES (20, 'Lê Thị Huệ', 13, 1000, 1, NULL, 1, NULL, NULL, '01/03/2021', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'lthue2', '1234', 1, 0, NULL, 0);
INSERT INTO `ns_nhanvien` VALUES (21, 'Trần Quốc Đạt', 13, 1000, 1, NULL, 1, NULL, NULL, '04/05/2021', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'tqdat5', '1234', 1, 0, NULL, 0);
INSERT INTO `ns_nhanvien` VALUES (22, 'Hồ Thanh Xuân', 13, 1000, 1, NULL, 1, NULL, NULL, '08/05/2021', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'htxuan3', '1234', 1, 0, NULL, 0.5);
INSERT INTO `ns_nhanvien` VALUES (23, 'Lê Diệp Quốc Anh', 13, 1000, 1, NULL, 1, NULL, NULL, '10/03/2022', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ldqanh6', '1234', 1, 0, NULL, 12);
INSERT INTO `ns_nhanvien` VALUES (24, 'Trịnh Thị Hoàng Oanh', 13, 1000, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'tthoanh11', '1234', 1, 0, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (25, 'Nguyễn Khánh', 13, 1000, 2, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'nkhanh9', '1234', 1, 0, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (26, 'Khưu Bảo Như', 13, 1000, 1, NULL, 1, NULL, NULL, '05/08/2023', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'kbnhu13', '1234', 1, 0, NULL, 0);
INSERT INTO `ns_nhanvien` VALUES (27, 'Đặng Thành Nam', 13, 1000, 1, NULL, 1, NULL, '', '03/01/2022', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dtnam7', '1234', 1, 0, NULL, 2.5);
INSERT INTO `ns_nhanvien` VALUES (28, 'Trần Đăng Khôi', 13, 1000, 1, NULL, 1, NULL, NULL, '04/05/2022', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'tdkhoi8', '1234', 1, 0, NULL, 9.5);
INSERT INTO `ns_nhanvien` VALUES (29, 'Nguyễn Lâm Hoàng Anh', 13, 1000, 1, NULL, 1, NULL, NULL, '19/08/2023', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'nlhanh14', '1234', 1, 0, NULL, 0);
INSERT INTO `ns_nhanvien` VALUES (30, 'Trần Văn Nguyên', 13, 1000, 1, NULL, 1, NULL, NULL, '19/08/2023', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'tvnguyen15', '1234', 1, 0, NULL, 0);
INSERT INTO `ns_nhanvien` VALUES (31, 'Nguyễn Trần Ngọc Tấn', 13, 1000, 1, NULL, 1, NULL, NULL, '19/08/2023', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ntntan16', '1234', 1, 0, NULL, 0);
INSERT INTO `ns_nhanvien` VALUES (32, 'Nguyễn Khánh', 13, 1000, 1, NULL, 1, NULL, NULL, '07/04/2023', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'nkhanh11', '1234', 1, 0, NULL, 0);
INSERT INTO `ns_nhanvien` VALUES (33, 'IT_TEST', 13, 1000, 1, NULL, 1, '2000-02-20', NULL, '', NULL, NULL, '0337346999', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 'Địa chỉ thường trú', NULL, NULL, '', NULL, NULL, './public/upload/1696175134004-120260906-avatar-logo192.png', 'root', '12345678', 1, 0, 'root@gmail.com', NULL);

-- ----------------------------
-- Table structure for ns_nhanvien_bangcap
-- ----------------------------
DROP TABLE IF EXISTS `ns_nhanvien_bangcap`;
CREATE TABLE `ns_nhanvien_bangcap`  (
  `bangcap_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NULL DEFAULT 1,
  `bangcap_name` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `bangcap_namtotnghiep` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `bangcap_ngayhethan` date NULL DEFAULT NULL,
  `bangcap_scan_file` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `bangcap_order` int(11) NULL DEFAULT NULL,
  `bangcap_noicap` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `loai_bang_cap` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  PRIMARY KEY (`bangcap_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_nhanvien_bangcap
-- ----------------------------
INSERT INTO `ns_nhanvien_bangcap` VALUES (1, 135, NULL, NULL, NULL, 'vanbang_1.pdf', 1000, NULL, '');
INSERT INTO `ns_nhanvien_bangcap` VALUES (8, 1, 'giấy khám sức khỏe', NULL, NULL, 'vanbang_8.docx', 1000, NULL, '');
INSERT INTO `ns_nhanvien_bangcap` VALUES (7, 1, NULL, NULL, NULL, NULL, 1000, NULL, '');
INSERT INTO `ns_nhanvien_bangcap` VALUES (6, 142, NULL, NULL, NULL, NULL, 1000, NULL, 'LAIXE');
INSERT INTO `ns_nhanvien_bangcap` VALUES (9, 1, NULL, NULL, NULL, NULL, 1000, NULL, 'LAIXE');
INSERT INTO `ns_nhanvien_bangcap` VALUES (10, 27, NULL, NULL, NULL, NULL, 1000, NULL, '');

-- ----------------------------
-- Table structure for ns_nhanvien_bangluongchinh
-- ----------------------------
DROP TABLE IF EXISTS `ns_nhanvien_bangluongchinh`;
CREATE TABLE `ns_nhanvien_bangluongchinh`  (
  `luong_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nv_name` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_id` int(11) NULL DEFAULT NULL,
  `fMonth` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `fYear` varchar(4) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `danhmuc_id` int(11) NULL DEFAULT 0,
  `luong_co_dinh` int(11) NULL DEFAULT 0,
  `ngay_vao_lam` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phu_cap_trach_nhiem` int(11) NULL DEFAULT 0,
  `phu_cap_tien_xang` double(11, 2) NULL DEFAULT 0.00,
  `phu_cap_tien_com` double(11, 2) NULL DEFAULT 0.00,
  `phu_cap_dien_thoai` double(11, 2) NULL DEFAULT 0.00,
  `phu_cap_cong_tac_phi` double(11, 2) NULL DEFAULT 0.00,
  `phu_cap_chuyen_can` double(11, 2) NULL DEFAULT 0.00,
  `thuong_chuyen_can` int(11) NULL DEFAULT 0,
  `d01` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d02` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d03` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d04` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d05` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d06` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d07` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d08` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d09` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d10` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d11` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d12` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d13` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d14` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d15` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d16` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d17` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d18` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d19` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d20` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d21` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d22` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d23` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d24` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d25` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d26` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d27` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d28` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d29` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d30` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d31` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tong_cong` float NULL DEFAULT 0,
  `tong_P` float NULL DEFAULT 0,
  `tong_L` float NULL DEFAULT 0,
  `tong_khongphep` float NULL DEFAULT 0,
  `tru_luong_do_tam_ung` int(11) NULL DEFAULT 0,
  `tru_luong_do_ky_quy` int(11) NULL DEFAULT 0,
  `note_d01` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d02` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d03` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d04` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d05` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d06` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d07` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d08` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d09` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d10` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d11` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d12` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d13` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d14` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d15` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d16` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d17` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d18` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d19` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d20` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d21` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d22` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d23` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d24` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d25` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d26` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d27` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d28` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d29` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d30` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d31` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `comment_text` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `ngay_chuan` double NULL DEFAULT NULL,
  `thongso_default` int(11) NULL DEFAULT NULL,
  `luong_tang` int(11) NULL DEFAULT NULL,
  `luong_giam` int(11) NULL DEFAULT NULL,
  `ghichu_tanggiam` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `gio_tang_ca` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `mcc_hc_ditre` int(11) NULL DEFAULT NULL,
  `mcc_tc_ditre` int(11) NULL DEFAULT NULL,
  `mcc_hc_giolam` int(11) NULL DEFAULT NULL,
  `mcc_tc_giolam` int(11) NULL DEFAULT NULL,
  `mcc_ca1_ditre` int(11) NULL DEFAULT NULL,
  `mcc_ca2_ditre` int(11) NULL DEFAULT NULL,
  `mcc_ca3_ditre` int(11) NULL DEFAULT NULL,
  `mcc_ca1_giolam` float NULL DEFAULT NULL,
  `mcc_ca2_giolam` float NULL DEFAULT NULL,
  `mcc_ca3_giolam` float NULL DEFAULT NULL,
  `tong_ngaylamviec` float NULL DEFAULT 0,
  `phep_dauthang` float NULL DEFAULT 0,
  `phep_congthem` float NULL DEFAULT 0,
  PRIMARY KEY (`luong_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 320 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_nhanvien_bangluongchinh
-- ----------------------------
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (1, '', 1, '07', '2023', 3, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '1', NULL, 'P', '1', '2', 'P', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', NULL, '1', '1', '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 0, 0, 0, 0, 0, 'dsfsdf\n', NULL, 'Làm tăng ca tới 8h tối\n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (2, '', 2, '07', '2023', 3, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '1.5', NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 0, 0, 0, 0, 0, 'tăng ca 9h', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (10, '', 2, '07', '2023', 1, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (4, '', 4, '07', '2023', 3, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (5, '', 5, '07', '2023', 3, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (6, '', 6, '07', '2023', 3, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (7, '', 7, '07', '2023', 3, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (9, '', 1, '07', '2023', 1, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (11, '', 3, '07', '2023', 1, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (12, '', 4, '07', '2023', 1, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (13, '', 5, '07', '2023', 1, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (14, '', 6, '07', '2023', 1, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (15, '', 7, '07', '2023', 1, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (16, '', 8, '07', '2023', 1, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (17, '', 9, '07', '2023', 1, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (18, '', 10, '07', '2023', 1, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (19, '', 11, '07', '2023', 1, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (20, '', 12, '07', '2023', 10, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (21, '', 13, '07', '2023', 10, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (22, '', 14, '07', '2023', 10, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (23, '', 15, '07', '2023', 10, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (24, '', 16, '07', '2023', 11, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, '', '1', '1', '1', '1', '0.5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (25, '', 17, '07', '2023', 12, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (26, '', 18, '07', '2023', 12, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (27, '', 19, '07', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (28, '', 20, '07', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (29, '', 21, '07', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (30, '', 22, '07', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (31, '', 23, '07', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (32, '', 24, '07', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (33, '', 25, '07', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (34, '', 26, '07', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (35, '', 27, '07', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (36, '', 28, '07', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (37, '', 29, '07', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (38, '', 30, '07', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (39, '', 31, '07', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (157, '', 30, '08', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '1', '1', '1', '1', '1', NULL, '1', '1', '1', '1', '1', '2', '2', '1', '1', '1', '1', '1', '1', '2', '1', '1', '1', '1', '1', '2', NULL, '1', '1', '1', '1', 33, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (158, '', 31, '08', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '1', '1', '1', '1', '1', NULL, '1', '1', '1', '1', '1', '2', '2', '1', '1', '1', '1', '1', '1', NULL, '1', '1', '1', '1', '1', NULL, NULL, '1', '1', '1', '1', 29, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2\n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (156, '', 29, '08', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '1', '1', '1', '1', '1', NULL, '1', '1', '1', '1', 'O', '', NULL, '1', '1', '1', '1', '1', '1', '2', '1', '1', '1', '1', '1', '2', NULL, '1', '1', '1', '1', 28, 0, 0, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (155, '', 28, '08', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 'P', '1', '1', '1', '1', NULL, '1', '1', '1', '1', '1', NULL, NULL, '1', '1', '1', '1', '1', '1', NULL, '1', '1', '1', '1', '1', NULL, NULL, '1', '1', '1', '1', 24, 1, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (154, '', 27, '08', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '1', '1', '1', '1', '1', NULL, '1', '1', '1', '1', '1', NULL, NULL, '1', '1', '1', '1', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 16, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (148, '', 21, '08', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '1', '1', '1', '1', 'P', NULL, '1', '1', '0.5', '1', 'P', NULL, NULL, '1', '1', '1', '1', '1', '0.5', NULL, '1', '1', '1', '1', '1', NULL, NULL, '1', '1', '1', '1', 22, 2, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (149, '', 22, '08', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '1', '1', '1', '1', '1', NULL, '1', '1', '1', '1', '1', NULL, NULL, '1', '1', '1', '1', '1', '1', NULL, '1', '1', '1', '1', '1', NULL, NULL, 'P', '1', '1', '1', 24, 1, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (150, '', 23, '08', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '1', '1', '1', '1', '1', NULL, '1', '1', '1', '1', '1', NULL, NULL, '1', '1', '1', '1', 'P', '1', NULL, '1', '1', '1', '1', '1', NULL, NULL, '1', '1', '1', '1', 24, 1, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (151, '', 24, '08', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '1', '1', '1', '1', '1', NULL, '1', '1', '1', '1', '1', NULL, NULL, '1', '1', '1', '1', '1', '1', NULL, '1', '1', '1', '1', '1', NULL, NULL, '1', '1', '1', 'P', 24, 1, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (152, '', 25, '08', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '1', '1', '1', '1', '1', NULL, '1', '1', '1', '1', '1', NULL, NULL, '1', '1', '1', '1', '1', '1', NULL, '1', '1', '1', '1', '1', NULL, NULL, 'P', '1', '1', '1', 24, 1, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (153, '', 26, '08', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '1', '1', '1', '1', '1', NULL, '1', '1', '1', '1', '1', NULL, NULL, '1', '1', '1', '1', '0.5', '1', NULL, '1', '1', '1', '1', '1', NULL, NULL, '1', '1', '1', '1', 25, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (147, '', 20, '08', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '1', '1', '1', '1', '0.5', NULL, 'P', '1', '1', '1', '1', '2', NULL, '1', '1', '1', '1', '1', '1', NULL, '1', '1', '1', '1', '1', NULL, NULL, '1', '1', '1', '1', 26, 1, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (146, '', 19, '08', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '1', '1', '1', '1', '1', NULL, '1', 'P', 'P', '1', '1', NULL, NULL, '1', '1', '1', '1', 'P', '1', NULL, '1', '1', '1', '1', '1', NULL, NULL, '1', '1', '1', '1', 22, 3, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (145, '', 18, '08', '2023', 12, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '1', '1', '1', '1', '1', NULL, '1', '1', '1', '1', '1', NULL, NULL, '1', '1', '1', '1', '1', '1', NULL, '1', '1', '1', '1', '1', NULL, NULL, '1', '1', '1', '1', 25, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (122, '', 1, '08', '2023', 3, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 1, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (123, '', 2, '08', '2023', 3, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 1, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (124, '', 2, '08', '2023', 1, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 1, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (125, '', 4, '08', '2023', 3, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 1, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (126, '', 5, '08', '2023', 3, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 1, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (127, '', 6, '08', '2023', 3, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (128, '', 7, '08', '2023', 3, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (129, '', 1, '08', '2023', 1, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (130, '', 3, '08', '2023', 1, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (131, '', 4, '08', '2023', 1, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (132, '', 5, '08', '2023', 1, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (133, '', 6, '08', '2023', 1, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (134, '', 7, '08', '2023', 1, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (135, '', 8, '08', '2023', 1, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (136, '', 9, '08', '2023', 1, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (137, '', 10, '08', '2023', 1, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (138, '', 11, '08', '2023', 1, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (140, '', 13, '08', '2023', 10, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'L', '1', 'O', 'O', '1', 'P', '1', '1', '1', 5, 1, 1, 2, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 360, 0, 0, -2, 0, 0, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (141, '', 14, '08', '2023', 10, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '1', '1', '1', '1', '1', '1', 7, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 120, 0, 0, 2, 0, 0, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (142, '', 15, '08', '2023', 10, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '2', 'O', '1', '1', '1', '1', '2', 'P', 10, 1, 0, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (143, '', 16, '08', '2023', 11, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '1', '1', '1', '1', '1', NULL, '1', '1', '1', '1', '1', NULL, NULL, '1', '1', '1', '1', '1', '1', NULL, '1', '1', '1', '1', '1', NULL, NULL, '1', '1', '1', '1', 25, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (144, '', 17, '08', '2023', 12, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '1', '1', '1', '1', 'P', NULL, '1', '1', '1', '1', '1', NULL, NULL, '1', '1', '1', '1', 'P', '1', NULL, '1', '1', '1', '1', '1', NULL, NULL, '1', '1', '1', '1', 23, 2, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (97, '', 12, '09', '2023', 10, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, '', NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '', NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (98, '', 13, '09', '2023', 10, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 23.5, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (99, '', 14, '09', '2023', 10, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 23.5, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (100, '', 15, '09', '2023', 10, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 23.5, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (101, '', 16, '09', '2023', 11, 0, '01/12/2022', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 'P', 'L', NULL, 'L', '1', '1', '1', '1', '1', NULL, '1', '1.75', '1.375', '1', '1', '1', NULL, '1', '1', '1', '1', '1', '', NULL, '1', '1', '1', '1', '1', '1', NULL, 26.125, 1, 2, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 23.125, 4, 1);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (102, '', 17, '09', '2023', 12, 0, '01/03/2021', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '', '', NULL, '', '1', '1', '1', '1', '', NULL, '1', '1', '1', '1', '1', '', '', '1', '1', '1', '1', '1', NULL, NULL, '1', '1', '', '', NULL, NULL, NULL, 16, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 23.5, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 16, 5.5, 1);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (103, '', 18, '09', '2023', 12, 0, '13/02/2023', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 23.5, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 1, 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (104, '', 19, '09', '2023', 13, 0, '01/03/2021', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 4, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (105, '', 20, '09', '2023', 13, 0, '01/03/2021', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 23.5, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (106, '', 21, '09', '2023', 13, 0, '04/05/2021', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 'P', 'L', '', 'L', '1', '1', '1', '1', NULL, NULL, '0.5_0.5P', '1', '1', '1', '1', '1', NULL, '0.5_0.5P', '1', '1', '1', '1', NULL, NULL, 'P', '1', '1', '', '', '', NULL, 21, 3, 2, -1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 16, 0, 1);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (107, '', 22, '09', '2023', 13, 0, '08/05/2021', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 23.5, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0.5, 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (108, '', 23, '09', '2023', 13, 0, '10/03/2022', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '1', 'L', '', 'L', '1', '1', '1', '1', NULL, NULL, '1', '1', '1', '1', '1', '1', NULL, '1', '1', '1', '1', '1', '2', NULL, '1', '1', '1', NULL, NULL, NULL, NULL, 23, 0, 2, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 23.5, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 21, 12, 1);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (109, '', 24, '09', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 23.5, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (111, '', 26, '09', '2023', 13, 0, '05/08/2023', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '1', NULL, NULL, '', '1', '1', '1', '1', NULL, NULL, '1', '1', '1', '1', '1', '1', NULL, '1', '1', '1', '1', '1', NULL, NULL, '1', '1', '1', NULL, NULL, NULL, NULL, 19, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 23.5, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 19, 0, 1);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (112, '', 27, '09', '2023', 13, 0, '03/01/2022', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 23.5, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 2.5, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (113, '', 28, '09', '2023', 13, 0, '04/05/2022', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 23.5, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 9.5, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (114, '', 29, '09', '2023', 13, 0, '19/08/2023', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '0.5_0.5P', 'L', NULL, 'L', '1', '1', '1', '1', NULL, NULL, '1', '1', '1', '1', '1', '0.5_0.5P', NULL, '1', '1', '1', '1', '1', '2', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, 21, 1, 2, -1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 23.5, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 0, 1);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (115, '', 30, '09', '2023', 13, 0, '19/08/2023', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 23.5, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (116, '', 31, '09', '2023', 13, 0, '19/08/2023', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 23.5, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (117, '', 32, '09', '2023', 13, 0, '07/04/2023', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 23.5, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (279, '', 12, '10', '2023', 10, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (280, '', 13, '10', '2023', 10, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (281, '', 14, '10', '2023', 10, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (282, '', 15, '10', '2023', 10, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (283, '', 16, '10', '2023', 11, 0, '01/12/2022', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 4, 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (284, '', 17, '10', '2023', 12, 0, '01/03/2021', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (285, '', 18, '10', '2023', 12, 0, '13/02/2023', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (286, '', 19, '10', '2023', 13, 0, '01/03/2021', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (287, '', 20, '10', '2023', 13, 0, '01/03/2021', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (288, '', 21, '10', '2023', 13, 0, '04/05/2021', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (289, '', 22, '10', '2023', 13, 0, '08/05/2021', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (290, '', 23, '10', '2023', 13, 0, '10/03/2022', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 13, 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (291, '', 24, '10', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (293, '', 26, '10', '2023', 13, 0, '05/08/2023', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (294, '', 27, '10', '2023', 13, 0, '03/01/2022', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (295, '', 28, '10', '2023', 13, 0, '04/05/2022', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (296, '', 29, '10', '2023', 13, 0, '19/08/2023', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (297, '', 30, '10', '2023', 13, 0, '19/08/2023', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (298, '', 31, '10', '2023', 13, 0, '19/08/2023', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (299, '', 32, '10', '2023', 13, 0, '07/04/2023', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (300, '', 12, '11', '2023', 10, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (301, '', 13, '11', '2023', 10, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (302, '', 14, '11', '2023', 10, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (303, '', 15, '11', '2023', 10, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (304, '', 16, '11', '2023', 11, 0, '01/12/2022', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 4, 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (305, '', 17, '11', '2023', 12, 0, '01/03/2021', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (306, '', 18, '11', '2023', 12, 0, '13/02/2023', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (307, '', 19, '11', '2023', 13, 0, '01/03/2021', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (308, '', 20, '11', '2023', 13, 0, '01/03/2021', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (309, '', 21, '11', '2023', 13, 0, '04/05/2021', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (310, '', 22, '11', '2023', 13, 0, '08/05/2021', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (311, '', 23, '11', '2023', 13, 0, '10/03/2022', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 13, 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (312, '', 24, '11', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (313, '', 26, '11', '2023', 13, 0, '05/08/2023', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (314, '', 27, '11', '2023', 13, 0, '03/01/2022', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (315, '', 28, '11', '2023', 13, 0, '04/05/2022', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (316, '', 29, '11', '2023', 13, 0, '19/08/2023', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (317, '', 30, '11', '2023', 13, 0, '19/08/2023', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (318, '', 31, '11', '2023', 13, 0, '19/08/2023', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (319, '', 32, '11', '2023', 13, 0, '07/04/2023', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0);

-- ----------------------------
-- Table structure for ns_tamung_chitiet
-- ----------------------------
DROP TABLE IF EXISTS `ns_tamung_chitiet`;
CREATE TABLE `ns_tamung_chitiet`  (
  `chitiet_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NULL DEFAULT NULL,
  `chitiet_ngaythang` datetime NULL DEFAULT NULL,
  `chitiet_loai` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `chitiet_sotien` int(11) NULL DEFAULT 0,
  `chitiet_ghichu` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `chitiet_status` int(11) NULL DEFAULT NULL,
  `chitiet_nguoithutien` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `chitiet_inputdate` datetime NULL DEFAULT NULL,
  `chitiet_mathanhtoan` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`chitiet_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_tamung_chitiet
-- ----------------------------
INSERT INTO `ns_tamung_chitiet` VALUES (7, 12, '2020-09-06 00:23:38', 'TAM_UNG', -2222, 'TẠM ỨNG', 1, 'BỘ PHẬN KẾ TOÁN', '2020-09-06 00:23:38', 'TU');
INSERT INTO `ns_tamung_chitiet` VALUES (6, -1, '2020-08-21 11:42:51', 'TAM_UNG', 0, 'TẠM ỨNG', 1, 'BỘ PHẬN KẾ TOÁN', '2020-08-21 11:42:51', 'TU');

-- ----------------------------
-- Table structure for ns_tamung_nv
-- ----------------------------
DROP TABLE IF EXISTS `ns_tamung_nv`;
CREATE TABLE `ns_tamung_nv`  (
  `tamung_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NULL DEFAULT NULL,
  `nv_name` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `no_dau_ky` int(11) NULL DEFAULT 0,
  `no_tang_them` int(11) NULL DEFAULT 0,
  `no_cuoi_ky` int(11) NULL DEFAULT NULL,
  `tamung_thang` date NULL DEFAULT NULL,
  `tamung_status` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`tamung_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 329 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_tamung_nv
-- ----------------------------
INSERT INTO `ns_tamung_nv` VALUES (328, 12, 'Nguyễn Văn Thường[Ban Lãnh Đạo]', 0, 0, 0, '2020-09-01', 1);
INSERT INTO `ns_tamung_nv` VALUES (294, -1, '', 0, 0, 0, '2020-08-01', 1);

-- ----------------------------
-- Table structure for ns_taobangluong
-- ----------------------------
DROP TABLE IF EXISTS `ns_taobangluong`;
CREATE TABLE `ns_taobangluong`  (
  `tbl_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tbl_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tbl_month` int(11) NULL DEFAULT NULL,
  `tbl_year` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`tbl_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 43 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_taobangluong
-- ----------------------------
INSERT INTO `ns_taobangluong` VALUES (33, 'BẢNG LƯƠNG THÁNG MỚI', 8, 2020);
INSERT INTO `ns_taobangluong` VALUES (34, 'BẢNG LƯƠNG THÁNG MỚI', 9, 2020);
INSERT INTO `ns_taobangluong` VALUES (35, 'BẢNG LƯƠNG THÁNG MỚI', 7, 2020);
INSERT INTO `ns_taobangluong` VALUES (36, 'BẢNG LƯƠNG THÁNG MỚI', 11, 2020);
INSERT INTO `ns_taobangluong` VALUES (37, 'BẢNG LƯƠNG THÁNG MỚI', 10, 2020);
INSERT INTO `ns_taobangluong` VALUES (38, 'BẢNG LƯƠNG THÁNG MỚI', 7, 2023);
INSERT INTO `ns_taobangluong` VALUES (40, 'BẢNG LƯƠNG THÁNG MỚI', 10, 2023);
INSERT INTO `ns_taobangluong` VALUES (41, 'BẢNG LƯƠNG THÁNG MỚI', 8, 2023);
INSERT INTO `ns_taobangluong` VALUES (42, 'BẢNG LƯƠNG THÁNG MỚI', 9, 2023);

-- ----------------------------
-- Table structure for ns_taobangluong_cauhinh
-- ----------------------------
DROP TABLE IF EXISTS `ns_taobangluong_cauhinh`;
CREATE TABLE `ns_taobangluong_cauhinh`  (
  `cus_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cus_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cus_status` int(11) NULL DEFAULT NULL,
  `cus_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cus_note` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `cus_h` int(11) NULL DEFAULT NULL,
  `cus_i` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`cus_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_taobangluong_cauhinh
-- ----------------------------
INSERT INTO `ns_taobangluong_cauhinh` VALUES (1, 'BHXH', 1, NULL, NULL, NULL, NULL);
INSERT INTO `ns_taobangluong_cauhinh` VALUES (2, 'Ký Quỹ', 1, NULL, NULL, NULL, NULL);
INSERT INTO `ns_taobangluong_cauhinh` VALUES (3, 'Công Nợ', 1, NULL, NULL, NULL, NULL);
INSERT INTO `ns_taobangluong_cauhinh` VALUES (4, 'Kỹ Luật', 1, NULL, NULL, NULL, NULL);
INSERT INTO `ns_taobangluong_cauhinh` VALUES (5, 'Đào Tạo', 1, NULL, NULL, NULL, NULL);
INSERT INTO `ns_taobangluong_cauhinh` VALUES (6, 'Hồ Sơ', 1, NULL, NULL, NULL, NULL);
INSERT INTO `ns_taobangluong_cauhinh` VALUES (7, 'Nhắc Nhở', 1, NULL, NULL, NULL, NULL);
INSERT INTO `ns_taobangluong_cauhinh` VALUES (8, 'Máy chấm công', 1, NULL, NULL, NULL, NULL);
INSERT INTO `ns_taobangluong_cauhinh` VALUES (9, 'Ca 1 Vào', NULL, NULL, NULL, 6, 0);
INSERT INTO `ns_taobangluong_cauhinh` VALUES (10, 'Ca 1 Ra', NULL, NULL, NULL, 12, 0);
INSERT INTO `ns_taobangluong_cauhinh` VALUES (11, 'Ca 2 Vào', NULL, NULL, NULL, 13, 0);
INSERT INTO `ns_taobangluong_cauhinh` VALUES (12, 'Ca 2 Ra', NULL, NULL, NULL, 17, 0);
INSERT INTO `ns_taobangluong_cauhinh` VALUES (13, 'Ca 3 Vào', NULL, NULL, NULL, 18, 0);
INSERT INTO `ns_taobangluong_cauhinh` VALUES (14, 'Ca 3 Ra', NULL, NULL, NULL, 21, 0);
INSERT INTO `ns_taobangluong_cauhinh` VALUES (15, 'hc_h_cong_chuan', NULL, NULL, NULL, 8, NULL);
INSERT INTO `ns_taobangluong_cauhinh` VALUES (16, 'tangca_h_cong_chuan', NULL, NULL, NULL, 3, NULL);
INSERT INTO `ns_taobangluong_cauhinh` VALUES (17, 'ca1_h_cong_chuan', NULL, NULL, NULL, 4, NULL);
INSERT INTO `ns_taobangluong_cauhinh` VALUES (18, 'ca2_h_cong_chuan', NULL, NULL, NULL, 4, NULL);
INSERT INTO `ns_taobangluong_cauhinh` VALUES (19, 'ca3_h_cong_chuan', NULL, NULL, NULL, 3, NULL);

-- ----------------------------
-- Table structure for ns_taobangluong_chitiet
-- ----------------------------
DROP TABLE IF EXISTS `ns_taobangluong_chitiet`;
CREATE TABLE `ns_taobangluong_chitiet`  (
  `bl_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tbl_id` int(11) NULL DEFAULT NULL,
  `bl_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `bl_status` int(11) NULL DEFAULT NULL,
  `bl_kyhieu` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `bl_type` int(11) NULL DEFAULT NULL,
  `bl_congthuc` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `bl_default` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `hople` int(11) NULL DEFAULT NULL,
  `sd_toan_cty` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`bl_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 49 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_taobangluong_chitiet
-- ----------------------------
INSERT INTO `ns_taobangluong_chitiet` VALUES (20, 33, 'Lương Cơ Bản', 0, NULL, 1, '@LCB@', NULL, 1, NULL);
INSERT INTO `ns_taobangluong_chitiet` VALUES (21, 0, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL);
INSERT INTO `ns_taobangluong_chitiet` VALUES (22, 33, 'Chuyên Cần', 0, NULL, 0, '@GC@ * 1000', NULL, 1, 0);
INSERT INTO `ns_taobangluong_chitiet` VALUES (23, 33, NULL, 0, NULL, 1, NULL, NULL, NULL, 0);
INSERT INTO `ns_taobangluong_chitiet` VALUES (24, 34, 'Giờ làm', 0, NULL, 0, '@TCGL@', NULL, 1, 1);
INSERT INTO `ns_taobangluong_chitiet` VALUES (25, 35, NULL, 0, NULL, 1, NULL, NULL, NULL, NULL);
INSERT INTO `ns_taobangluong_chitiet` VALUES (26, 33, NULL, 0, NULL, 1, NULL, NULL, NULL, NULL);
INSERT INTO `ns_taobangluong_chitiet` VALUES (27, 33, NULL, 0, NULL, 1, NULL, NULL, NULL, 0);
INSERT INTO `ns_taobangluong_chitiet` VALUES (28, 34, '', 0, NULL, 0, '', NULL, 1, NULL);
INSERT INTO `ns_taobangluong_chitiet` VALUES (29, 34, 'Ca', 0, NULL, 0, '', NULL, 1, NULL);
INSERT INTO `ns_taobangluong_chitiet` VALUES (30, 34, 'Ca - Đi trể', 0, NULL, 0, '', NULL, 1, NULL);
INSERT INTO `ns_taobangluong_chitiet` VALUES (31, 36, NULL, 0, NULL, 1, NULL, NULL, NULL, NULL);
INSERT INTO `ns_taobangluong_chitiet` VALUES (32, 36, NULL, 0, NULL, 1, NULL, NULL, NULL, NULL);
INSERT INTO `ns_taobangluong_chitiet` VALUES (33, 36, NULL, 0, NULL, 1, NULL, NULL, NULL, NULL);
INSERT INTO `ns_taobangluong_chitiet` VALUES (34, 37, 'Lương Bốc hàng', 1, NULL, 0, '@LCB@', NULL, 1, NULL);
INSERT INTO `ns_taobangluong_chitiet` VALUES (35, 38, 'lương ngày thường', 0, NULL, 1, '@LCB@ - @U', NULL, 0, 0);
INSERT INTO `ns_taobangluong_chitiet` VALUES (36, 38, NULL, 0, NULL, 1, NULL, NULL, NULL, NULL);
INSERT INTO `ns_taobangluong_chitiet` VALUES (37, 38, NULL, 0, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `ns_taobangluong_chitiet` VALUES (38, 38, NULL, 0, NULL, 1, NULL, NULL, NULL, NULL);
INSERT INTO `ns_taobangluong_chitiet` VALUES (39, 38, NULL, 0, NULL, 1, NULL, NULL, NULL, NULL);
INSERT INTO `ns_taobangluong_chitiet` VALUES (40, 38, NULL, 0, NULL, 1, NULL, NULL, NULL, NULL);
INSERT INTO `ns_taobangluong_chitiet` VALUES (41, 38, NULL, 0, NULL, 1, NULL, NULL, NULL, NULL);
INSERT INTO `ns_taobangluong_chitiet` VALUES (42, 38, NULL, 0, NULL, 1, NULL, NULL, NULL, NULL);
INSERT INTO `ns_taobangluong_chitiet` VALUES (43, 39, 'sdsadsad', 0, NULL, 0, NULL, NULL, NULL, 0);
INSERT INTO `ns_taobangluong_chitiet` VALUES (44, 39, NULL, 0, NULL, 0, NULL, NULL, NULL, 0);
INSERT INTO `ns_taobangluong_chitiet` VALUES (45, 40, NULL, 0, NULL, 0, '', NULL, 1, NULL);
INSERT INTO `ns_taobangluong_chitiet` VALUES (46, 41, 'ABCD', 1, NULL, 1, NULL, NULL, NULL, NULL);
INSERT INTO `ns_taobangluong_chitiet` VALUES (47, 40, NULL, 0, NULL, 1, NULL, NULL, NULL, NULL);
INSERT INTO `ns_taobangluong_chitiet` VALUES (48, 42, 'ABCD', 1, NULL, 1, '', NULL, 1, NULL);

-- ----------------------------
-- Table structure for ns_thetu
-- ----------------------------
DROP TABLE IF EXISTS `ns_thetu`;
CREATE TABLE `ns_thetu`  (
  `thetu_id` int(11) NOT NULL AUTO_INCREMENT,
  `thetu_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `nv_id` int(11) NULL DEFAULT NULL,
  `thetu_status` bit(1) NOT NULL,
  `danhmuc_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`thetu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_thetu
-- ----------------------------
INSERT INTO `ns_thetu` VALUES (1, '012345678', 16, b'1', 11);
INSERT INTO `ns_thetu` VALUES (2, '032423524', 17, b'0', 12);
INSERT INTO `ns_thetu` VALUES (3, '032442535', 18, b'0', 12);
INSERT INTO `ns_thetu` VALUES (8, '032423434', 19, b'0', 13);
INSERT INTO `ns_thetu` VALUES (10, '021435343', 17, b'1', 12);
INSERT INTO `ns_thetu` VALUES (11, '0564736457', 21, b'1', 13);
INSERT INTO `ns_thetu` VALUES (12, '0965467856', 20, b'0', 13);
INSERT INTO `ns_thetu` VALUES (13, '674434543', 12, b'0', 10);

-- ----------------------------
-- Table structure for qltl_items
-- ----------------------------
DROP TABLE IF EXISTS `qltl_items`;
CREATE TABLE `qltl_items`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `ns_nhanvien` int(11) NULL DEFAULT NULL,
  `link_file` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `parent_id` int(11) NULL DEFAULT NULL,
  `isDirectory` int(11) NOT NULL DEFAULT 1,
  `status` int(11) NULL DEFAULT 1,
  `created_at` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `deleted_at` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `parent_id`(`parent_id`) USING BTREE,
  INDEX `ns_nhanvien`(`ns_nhanvien`) USING BTREE,
  CONSTRAINT `qltl_items_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `qltl_items` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `qltl_items_ibfk_2` FOREIGN KEY (`ns_nhanvien`) REFERENCES `ns_nhanvien` (`nv_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qltl_items
-- ----------------------------

-- ----------------------------
-- Table structure for qltl_quyen_sua
-- ----------------------------
DROP TABLE IF EXISTS `qltl_quyen_sua`;
CREATE TABLE `qltl_quyen_sua`  (
  `ns_nhanvien` int(11) NOT NULL,
  `items_id` int(11) NOT NULL,
  `status` int(11) NULL DEFAULT 1,
  `created_at` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `deleted_at` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ns_nhanvien`, `items_id`) USING BTREE,
  INDEX `items_id`(`items_id`) USING BTREE,
  INDEX `ns_nhanvien`(`ns_nhanvien`) USING BTREE,
  CONSTRAINT `qltl_quyen_sua_ibfk_1` FOREIGN KEY (`items_id`) REFERENCES `qltl_items` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `qltl_quyen_sua_ibfk_2` FOREIGN KEY (`ns_nhanvien`) REFERENCES `ns_nhanvien` (`nv_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qltl_quyen_sua
-- ----------------------------

-- ----------------------------
-- Table structure for qltl_quyen_xem
-- ----------------------------
DROP TABLE IF EXISTS `qltl_quyen_xem`;
CREATE TABLE `qltl_quyen_xem`  (
  `ns_nhanvien` int(11) NOT NULL,
  `items_id` int(11) NOT NULL,
  `status` int(11) NULL DEFAULT 1,
  `created_at` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `deleted_at` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ns_nhanvien`, `items_id`) USING BTREE,
  INDEX `items_id`(`items_id`) USING BTREE,
  INDEX `ns_nhanvien`(`ns_nhanvien`) USING BTREE,
  CONSTRAINT `qltl_quyen_xem_ibfk_1` FOREIGN KEY (`items_id`) REFERENCES `qltl_items` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `qltl_quyen_xem_ibfk_2` FOREIGN KEY (`ns_nhanvien`) REFERENCES `ns_nhanvien` (`nv_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qltl_quyen_xem
-- ----------------------------

-- ----------------------------
-- Table structure for qltl_trao_doi
-- ----------------------------
DROP TABLE IF EXISTS `qltl_trao_doi`;
CREATE TABLE `qltl_trao_doi`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `nv_id` int(11) NOT NULL,
  `noi_dung` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `trao_doi_status` int(11) NOT NULL DEFAULT 1,
  `created_at` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `deleted_at` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `nv_id`(`nv_id`) USING BTREE,
  INDEX `item_id`(`item_id`) USING BTREE,
  CONSTRAINT `item_id` FOREIGN KEY (`item_id`) REFERENCES `qltl_items` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `nv_id` FOREIGN KEY (`nv_id`) REFERENCES `ns_nhanvien` (`nv_id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qltl_trao_doi
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_notice
-- ----------------------------
DROP TABLE IF EXISTS `tbl_notice`;
CREATE TABLE `tbl_notice`  (
  `notice_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `notice_text` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`notice_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tbl_notice
-- ----------------------------
INSERT INTO `tbl_notice` VALUES (1, 'Phần mềm quản lý kho hàng và chấm công');

-- ----------------------------
-- Table structure for tbl_users
-- ----------------------------
DROP TABLE IF EXISTS `tbl_users`;
CREATE TABLE `tbl_users`  (
  `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_username` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_company` int(11) NULL DEFAULT 0,
  `user_password` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '123456',
  `user_fullname` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_status` int(11) NULL DEFAULT NULL,
  `user_level` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'AGENT',
  `user_createdate` datetime NULL DEFAULT NULL,
  `user_address` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_officephone` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_phone` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cp_access` int(11) NULL DEFAULT NULL,
  `user_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_longname` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_email` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_zone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_quyen_ketoan` int(11) NULL DEFAULT 0,
  `user_quyen_quanlynhanvien` int(11) NULL DEFAULT 0,
  `user_quyen_dangtai` int(11) NULL DEFAULT 0,
  `user_zone_phuochoa` int(11) NULL DEFAULT 0,
  `user_zone_miendong` int(11) NULL DEFAULT 0,
  `user_zone_phatloc` int(11) NULL DEFAULT 0,
  `user_zone_mientay` int(11) NULL DEFAULT 0,
  `user_zone_phatdat` int(11) NULL DEFAULT 0,
  `user_zone_budang` int(11) NULL DEFAULT 0,
  `user_zone_phuoclong` int(11) NULL DEFAULT 0,
  `user_zone_dongxoai` int(11) NULL DEFAULT 0,
  `user_zone_vungtau` int(11) NULL DEFAULT 0,
  `user_zone_xulyvipham` int(11) NULL DEFAULT 0,
  `user_zone_hosoxe` int(11) NULL DEFAULT 0,
  `user_zone_bunho` int(11) NULL DEFAULT 0,
  `user_zone_kiosk` int(11) NULL DEFAULT 0,
  `user_banhang_kiosk` int(11) NULL DEFAULT 0,
  `user_zone_nhansu` int(11) NULL DEFAULT 0,
  `user_ketoan_kiosk` int(11) NULL DEFAULT 0,
  `user_zone_binhduong` int(11) NULL DEFAULT 0,
  `user_zone_phurieng` int(11) NULL DEFAULT 0,
  `user_zone_phattai` int(11) NULL DEFAULT 0,
  `user_zone_chamcong` int(11) NULL DEFAULT 0,
  `showmap` int(11) NOT NULL DEFAULT 0,
  `lat` decimal(11, 7) NULL DEFAULT NULL,
  `lng` decimal(11, 7) NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'Somewhere',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `user_wuid` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tbl_users
-- ----------------------------
INSERT INTO `tbl_users` VALUES (1, 'root', 1, '123456', 'Võ Anh Triết', 1, '0', '2012-05-09 15:08:26', 'Vận hành hệ thống', NULL, '0938.056.558', 1, 'boss', 'Hoang', '-', 'MD', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, 'Somewhere', '2020-06-19 13:44:42', '2023-08-25 01:36:13', NULL, NULL);
INSERT INTO `tbl_users` VALUES (2, 'trolygd', 1, '1234', 'Võ Anh Triết', 1, '0', NULL, 'Vận hành hệ thống', NULL, NULL, 1, 'boss', '', '-', 'MD', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, 'Somewhere', NULL, '2023-09-29 14:43:36', NULL, 2);
INSERT INTO `tbl_users` VALUES (10, 'dev', 1, '123456', 'IT', 1, '0', NULL, NULL, NULL, NULL, 1, 'boss', NULL, NULL, 'MD', 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 'Somewhere', NULL, '2023-09-29 13:55:52', NULL, 5);
INSERT INTO `tbl_users` VALUES (11, 'dev', 1, '123456', 'Hoàng Huy Cảnh', 1, '0', NULL, NULL, NULL, NULL, 1, 'boss', NULL, NULL, 'MD', 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 'Somewhere', NULL, '2023-09-29 13:55:52', NULL, 12);

-- ----------------------------
-- Table structure for user_preferences
-- ----------------------------
DROP TABLE IF EXISTS `user_preferences`;
CREATE TABLE `user_preferences`  (
  `pref_user` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `pref_name` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `pref_value` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`pref_user`, `pref_name`) USING BTREE,
  INDEX `pref_user`(`pref_user`, `pref_name`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of user_preferences
-- ----------------------------
INSERT INTO `user_preferences` VALUES ('0', 'LOCALE', 'en');
INSERT INTO `user_preferences` VALUES ('0', 'TABVIEW', '1');
INSERT INTO `user_preferences` VALUES ('0', 'SHDATEFORMAT', '%d/%m/%Y');
INSERT INTO `user_preferences` VALUES ('0', 'TIMEFORMAT', '%I:%M:%p');
INSERT INTO `user_preferences` VALUES ('0', 'UISTYLE', 'default');
INSERT INTO `user_preferences` VALUES ('0', 'SHTIMEFORMAT', '%d/%m/%Y %I:%M:%p');
INSERT INTO `user_preferences` VALUES ('0', 'KEY', 'becd99d013b7b1e791061e686d48523c');
INSERT INTO `user_preferences` VALUES ('0', 'ACTIVATION', '');
INSERT INTO `user_preferences` VALUES ('0', 'NEEDRELOAD', 'false');

SET FOREIGN_KEY_CHECKS = 1;
