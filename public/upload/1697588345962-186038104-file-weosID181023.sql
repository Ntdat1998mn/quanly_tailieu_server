/*
 Navicat Premium Data Transfer

 Source Server         : 126
 Source Server Type    : MySQL
 Source Server Version : 50560 (5.5.60-MariaDB)
 Source Host           : localhost:3306
 Source Schema         : weosID

 Target Server Type    : MySQL
 Target Server Version : 50560 (5.5.60-MariaDB)
 File Encoding         : 65001

 Date: 18/10/2023 00:38:23
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for company
-- ----------------------------
DROP TABLE IF EXISTS `company`;
CREATE TABLE `company`  (
  `company_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `company_name` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `company_officephone1` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `company_officephone2` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `company_fax` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `company_taxcode` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `company_address` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `company_director` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `company_accountant` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `company_treasurer` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `company_invoice_man1` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `company_invoice_man2` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `company_ref_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`company_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 37 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of company
-- ----------------------------
INSERT INTO `company` VALUES (1, 'dasda', '0961-11-77-11      ', NULL, NULL, 'undefined', 'KHU CN Tân Bình', 'undefined', 'undefined', 'undefined', NULL, NULL, '0f21df2d27f2895aae3fe5404c9e346e');
INSERT INTO `company` VALUES (30, 'cong ty van tien', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO `company` VALUES (31, 'an', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7b0a9a50f88a1ac25740dece661377c6');
INSERT INTO `company` VALUES (32, 'dasda', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0f21df2d27f2895aae3fe5404c9e346e');
INSERT INTO `company` VALUES (33, 'Gn24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '21cf639455079dbfdb166eac5f3136ab');
INSERT INTO `company` VALUES (34, 'gn24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '633bc607a0e47632b781331bdb3f0b77');
INSERT INTO `company` VALUES (35, 'gn24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '02c33d93ebb9e13f6e37e4e4eab09d15');
INSERT INTO `company` VALUES (36, 'gn24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '4bcc79ca837fc5fb97a2ae164a7d372c');

-- ----------------------------
-- Table structure for dvcc_ca_lam_viec
-- ----------------------------
DROP TABLE IF EXISTS `dvcc_ca_lam_viec`;
CREATE TABLE `dvcc_ca_lam_viec`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `gio_bat_dau` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `gio_ket_thuc` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `loai_ca` int(1) NULL DEFAULT 0 COMMENT '0: Hành Chính, 1 Tăng ca, 2 Theo ca',
  `gio_cong_chuan` int(11) NULL DEFAULT NULL,
  `ca_status` int(1) NULL DEFAULT 1,
  `company_id` int(11) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of dvcc_ca_lam_viec
-- ----------------------------
INSERT INTO `dvcc_ca_lam_viec` VALUES (1, 'A', '08:00:00', '12:00:00', 2, NULL, 1, 1);
INSERT INTO `dvcc_ca_lam_viec` VALUES (2, 'B', '13:30:00', '17:30:00', 2, NULL, 1, 1);
INSERT INTO `dvcc_ca_lam_viec` VALUES (5, 'Cả ngày', '08:30:00', '17:30:00', 0, NULL, 1, 1);

-- ----------------------------
-- Table structure for dvcc_cham_cong
-- ----------------------------
DROP TABLE IF EXISTS `dvcc_cham_cong`;
CREATE TABLE `dvcc_cham_cong`  (
  `cham_cong_id` int(11) NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NOT NULL,
  `thoi_gian` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `loai` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `lat` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `lng` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `address` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`cham_cong_id`) USING BTREE,
  INDEX `nv_id`(`nv_id`) USING BTREE,
  CONSTRAINT `dvcc_cham_cong_ibfk_1` FOREIGN KEY (`nv_id`) REFERENCES `ns_nhanvien` (`nv_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of dvcc_cham_cong
-- ----------------------------
INSERT INTO `dvcc_cham_cong` VALUES (1, 33, '2023-10-13 14:13:05', 'VAO', '22.23242', '23.423223', NULL);
INSERT INTO `dvcc_cham_cong` VALUES (2, 33, '2023-10-13 14:13:10', 'RA', '22.23242', '23.423223', NULL);
INSERT INTO `dvcc_cham_cong` VALUES (3, 33, '2023-10-13 14:13:44', 'VAO', '22.23242', '23.423223', NULL);
INSERT INTO `dvcc_cham_cong` VALUES (4, 33, '2023-10-13 14:13:46', 'RA', '22.23242', '23.423223', NULL);
INSERT INTO `dvcc_cham_cong` VALUES (5, 63, '2023-10-14 09:25:41', 'TD', '10.641823129553751', '106.73286560154608', NULL);
INSERT INTO `dvcc_cham_cong` VALUES (6, 63, '2023-10-14 12:14:22', 'TD', '10.678799615441354', '106.7066450796859', NULL);
INSERT INTO `dvcc_cham_cong` VALUES (7, 63, '2023-10-14 12:14:32', 'VAO', '10.67877197265625', '106.70668669054909', NULL);
INSERT INTO `dvcc_cham_cong` VALUES (8, 63, '2023-10-14 12:14:35', 'RA', '10.678802490234375', '106.70672650458916', NULL);
INSERT INTO `dvcc_cham_cong` VALUES (9, 64, '2023-10-15 06:41:44', 'TD', '10.6788330078125', '106.70675055353664', NULL);
INSERT INTO `dvcc_cham_cong` VALUES (10, 64, '2023-10-15 15:35:24', 'TD', '10.678718566894531', '106.7066629879301', NULL);
INSERT INTO `dvcc_cham_cong` VALUES (11, 64, '2023-10-15 15:35:34', 'RA', '10.678794860839844', '106.7066979952741', NULL);
INSERT INTO `dvcc_cham_cong` VALUES (12, 64, '2023-10-15 15:35:43', 'VAO', '10.678756713867188', '106.70672750841355', NULL);

-- ----------------------------
-- Table structure for dvcc_cham_cong_Dat
-- ----------------------------
DROP TABLE IF EXISTS `dvcc_cham_cong_Dat`;
CREATE TABLE `dvcc_cham_cong_Dat`  (
  `cham_cong_id` int(11) NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NOT NULL,
  `ngay` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `gio_den` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `gio_di` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`cham_cong_id`) USING BTREE,
  INDEX `nv_id`(`nv_id`) USING BTREE,
  CONSTRAINT `dvcc_cham_cong_Dat_ibfk_1` FOREIGN KEY (`nv_id`) REFERENCES `ns_nhanvien` (`nv_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of dvcc_cham_cong_Dat
-- ----------------------------
INSERT INTO `dvcc_cham_cong_Dat` VALUES (1, 16, '2023-09-08', '15:55:41', NULL);
INSERT INTO `dvcc_cham_cong_Dat` VALUES (2, 16, '2023-09-11', '13:53:10', NULL);
INSERT INTO `dvcc_cham_cong_Dat` VALUES (3, 16, '2023-09-21', '10:23:42', NULL);
INSERT INTO `dvcc_cham_cong_Dat` VALUES (4, 33, '2023-09-29', '08:54:38', NULL);
INSERT INTO `dvcc_cham_cong_Dat` VALUES (5, 16, '2023-09-29', '14:43:39', NULL);
INSERT INTO `dvcc_cham_cong_Dat` VALUES (6, 16, '2023-10-05', '13:05:29', NULL);

-- ----------------------------
-- Table structure for dvcc_chi_nhanh
-- ----------------------------
DROP TABLE IF EXISTS `dvcc_chi_nhanh`;
CREATE TABLE `dvcc_chi_nhanh`  (
  `chi_nhanh_id` int(11) NOT NULL AUTO_INCREMENT,
  `chi_nhanh_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `chi_nhanh_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `latitude` double NULL DEFAULT NULL,
  `longitude` double NULL DEFAULT NULL,
  `chi_nhanh_status` int(11) NOT NULL DEFAULT 1,
  `company_id` int(10) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`chi_nhanh_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of dvcc_chi_nhanh
-- ----------------------------
INSERT INTO `dvcc_chi_nhanh` VALUES (1, 'Trụ sở', '24 Hàm Nghi, Bến Nghé, Quận 1', 10.7711284, 106.7050269, 1, 1);
INSERT INTO `dvcc_chi_nhanh` VALUES (2, 'Chi nhánh Quận 7', '200 Đường số 9, Phường tân Phú, Quận 7', 10.7362199, 106.7151648, 1, 1);
INSERT INTO `dvcc_chi_nhanh` VALUES (3, 'Chi nhánh', '3', 10.822, 106.6257, 0, 1);
INSERT INTO `dvcc_chi_nhanh` VALUES (4, 'Trụ Sở Tân Bình 2', '200 Đường Số 9, phường Tân Phú Quận 77', 10.73633741248895, 106.7150981274011, 0, 1);
INSERT INTO `dvcc_chi_nhanh` VALUES (5, 'Trụ Sở Phú Nhuận 23', '09 Hoa Sứ Phường 7, phú Nhuận, TP HCM', 10.73634004774542, 106.7150954451922, 0, 1);
INSERT INTO `dvcc_chi_nhanh` VALUES (6, 'Chi Nhánh Phú Nhuận', '09 Hoa Sứ, phường 7, Quận Phú Nhuận', 10.7989265, NULL, 0, 1);
INSERT INTO `dvcc_chi_nhanh` VALUES (7, 'Chi Nhánh Phú Nhuận', '09 Hoa Sứ, phường 7, Quận Phú Nhuận', 10.7989265, NULL, 0, 1);
INSERT INTO `dvcc_chi_nhanh` VALUES (8, 'Chi Nhánh Phú Nhuận', '09 Hoa Sứ, phường 7, Quận Phú Nhuận', 10.7989265, 106.6887926, 1, 1);

-- ----------------------------
-- Table structure for dvcc_chuc_vu
-- ----------------------------
DROP TABLE IF EXISTS `dvcc_chuc_vu`;
CREATE TABLE `dvcc_chuc_vu`  (
  `chuc_vu_id` int(11) NOT NULL AUTO_INCREMENT,
  `chuc_vu_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`chuc_vu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of dvcc_chuc_vu
-- ----------------------------
INSERT INTO `dvcc_chuc_vu` VALUES (1, 'Nhân viên');
INSERT INTO `dvcc_chuc_vu` VALUES (2, 'Giám đốc');

-- ----------------------------
-- Table structure for dvcc_lich_cong_tac
-- ----------------------------
DROP TABLE IF EXISTS `dvcc_lich_cong_tac`;
CREATE TABLE `dvcc_lich_cong_tac`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NOT NULL,
  `noi_dung_cong_tac` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `noi_cong_tac` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ngay_bat_dau` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ngay_ket_thuc` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `quan_ly_truc_tiep` int(11) NULL DEFAULT NULL,
  `nguoi_duyet` int(11) NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '0: da xoa, 1 hoạt động',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `nv_id`(`nv_id`) USING BTREE,
  INDEX `quan_ly_truc_tiep`(`quan_ly_truc_tiep`) USING BTREE,
  INDEX `nguoi_duyet`(`nguoi_duyet`) USING BTREE,
  CONSTRAINT `dvcc_lich_cong_tac_ibfk_1` FOREIGN KEY (`nv_id`) REFERENCES `ns_nhanvien` (`nv_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `dvcc_lich_cong_tac_ibfk_2` FOREIGN KEY (`quan_ly_truc_tiep`) REFERENCES `ns_nhanvien` (`nv_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `dvcc_lich_cong_tac_ibfk_3` FOREIGN KEY (`nguoi_duyet`) REFERENCES `ns_nhanvien` (`nv_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of dvcc_lich_cong_tac
-- ----------------------------
INSERT INTO `dvcc_lich_cong_tac` VALUES (1, 33, 'Đi họp bàn công việc với đối tác', '77/9 Ngô Xương, Quận 5555, tp.HCM', '2023-08-19', '2023-08-25', 21, 20, 4);
INSERT INTO `dvcc_lich_cong_tac` VALUES (2, 33, 'abc', 'aa', '2023-09-17', '2023-09-17', NULL, 12, 3);
INSERT INTO `dvcc_lich_cong_tac` VALUES (3, 33, 'Đi họp bàn công việc với đối tác', '77/9 Ngô Xương, Quận 5555, tp.HCM', '2023-08-19', '2023-08-25', 21, 20, 4);
INSERT INTO `dvcc_lich_cong_tac` VALUES (4, 33, 'Đi họp bàn công việc với đối tác', '77/9 Ngô Xương, Quận 5555, tp.HCM', '2023-09-19', '2023-08-25', 21, 20, 0);

-- ----------------------------
-- Table structure for dvcc_lich_lam_viec_dang_ky
-- ----------------------------
DROP TABLE IF EXISTS `dvcc_lich_lam_viec_dang_ky`;
CREATE TABLE `dvcc_lich_lam_viec_dang_ky`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NOT NULL,
  `ngay` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ca_lam_viec_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '0: da xoa, 1 hoạt động',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `nv_id`(`nv_id`) USING BTREE,
  INDEX `ca_lam_viec_id`(`ca_lam_viec_id`) USING BTREE,
  CONSTRAINT `dvcc_lich_lam_viec_dang_ky_ibfk_2` FOREIGN KEY (`nv_id`) REFERENCES `ns_nhanvien` (`nv_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `dvcc_lich_lam_viec_dang_ky_ibfk_3` FOREIGN KEY (`ca_lam_viec_id`) REFERENCES `dvcc_ca_lam_viec` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of dvcc_lich_lam_viec_dang_ky
-- ----------------------------
INSERT INTO `dvcc_lich_lam_viec_dang_ky` VALUES (1, 33, '2023-08-21', 2, 1);
INSERT INTO `dvcc_lich_lam_viec_dang_ky` VALUES (2, 33, '2023-08-22', 1, 1);
INSERT INTO `dvcc_lich_lam_viec_dang_ky` VALUES (3, 33, '2023-08-23', 1, 1);
INSERT INTO `dvcc_lich_lam_viec_dang_ky` VALUES (4, 33, '2023-08-24', 1, 1);
INSERT INTO `dvcc_lich_lam_viec_dang_ky` VALUES (5, 33, '2023-08-25', 1, 1);
INSERT INTO `dvcc_lich_lam_viec_dang_ky` VALUES (6, 33, '2023-08-26', 1, 1);
INSERT INTO `dvcc_lich_lam_viec_dang_ky` VALUES (7, 33, '2023-08-27', 1, 1);
INSERT INTO `dvcc_lich_lam_viec_dang_ky` VALUES (8, 33, '2023-09-18', 1, 1);
INSERT INTO `dvcc_lich_lam_viec_dang_ky` VALUES (9, 33, '2023-09-19', 2, 1);
INSERT INTO `dvcc_lich_lam_viec_dang_ky` VALUES (10, 33, '2023-09-20', 1, 1);
INSERT INTO `dvcc_lich_lam_viec_dang_ky` VALUES (11, 33, '2023-09-21', 2, 1);
INSERT INTO `dvcc_lich_lam_viec_dang_ky` VALUES (12, 33, '2023-09-22', 1, 1);
INSERT INTO `dvcc_lich_lam_viec_dang_ky` VALUES (13, 63, '2023-10-16', 5, 1);
INSERT INTO `dvcc_lich_lam_viec_dang_ky` VALUES (14, 63, '2023-10-17', 5, 1);
INSERT INTO `dvcc_lich_lam_viec_dang_ky` VALUES (15, 63, '2023-10-18', 1, 1);
INSERT INTO `dvcc_lich_lam_viec_dang_ky` VALUES (16, 63, '2023-10-19', 2, 1);
INSERT INTO `dvcc_lich_lam_viec_dang_ky` VALUES (17, 63, '2023-10-20', 5, 1);
INSERT INTO `dvcc_lich_lam_viec_dang_ky` VALUES (18, 63, '2023-10-21', 5, 1);

-- ----------------------------
-- Table structure for dvcc_lich_lam_viec_duoc_duyet
-- ----------------------------
DROP TABLE IF EXISTS `dvcc_lich_lam_viec_duoc_duyet`;
CREATE TABLE `dvcc_lich_lam_viec_duoc_duyet`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NOT NULL,
  `ngay` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ca_lam_viec_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '0: da xoa, 1 hoạt động',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `nv_id`(`nv_id`) USING BTREE,
  INDEX `ca_lam_viec_id`(`ca_lam_viec_id`) USING BTREE,
  CONSTRAINT `dvcc_lich_lam_viec_duoc_duyet_ibfk_1` FOREIGN KEY (`nv_id`) REFERENCES `ns_nhanvien` (`nv_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `dvcc_lich_lam_viec_duoc_duyet_ibfk_2` FOREIGN KEY (`ca_lam_viec_id`) REFERENCES `dvcc_ca_lam_viec` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of dvcc_lich_lam_viec_duoc_duyet
-- ----------------------------
INSERT INTO `dvcc_lich_lam_viec_duoc_duyet` VALUES (1, 33, '2023-08-21', 2, 1);
INSERT INTO `dvcc_lich_lam_viec_duoc_duyet` VALUES (2, 33, '2023-08-21', 1, 1);

-- ----------------------------
-- Table structure for dvcc_lich_tang_ca
-- ----------------------------
DROP TABLE IF EXISTS `dvcc_lich_tang_ca`;
CREATE TABLE `dvcc_lich_tang_ca`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NOT NULL,
  `ngay_bat_dau` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ngay_ket_thuc` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `thoi_gian_bat_dau` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `thoi_gian_ket_thuc` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `quan_ly_truc_tiep` int(11) NULL DEFAULT NULL,
  `nguoi_duyet` int(11) NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '0: da xoa, 1: đợi duyệt: 2: được duyệt, 3: đã quá hạn ',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `nv_id`(`nv_id`) USING BTREE,
  INDEX `quan_ly_truc_tiep`(`quan_ly_truc_tiep`) USING BTREE,
  INDEX `nguoi_duyet`(`nguoi_duyet`) USING BTREE,
  CONSTRAINT `dvcc_lich_tang_ca_ibfk_1` FOREIGN KEY (`nv_id`) REFERENCES `ns_nhanvien` (`nv_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `dvcc_lich_tang_ca_ibfk_2` FOREIGN KEY (`quan_ly_truc_tiep`) REFERENCES `ns_nhanvien` (`nv_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `dvcc_lich_tang_ca_ibfk_3` FOREIGN KEY (`nguoi_duyet`) REFERENCES `ns_nhanvien` (`nv_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of dvcc_lich_tang_ca
-- ----------------------------
INSERT INTO `dvcc_lich_tang_ca` VALUES (1, 33, '2023-09-17', '2023-09-23', '10:42', '10:43', 15, 12, 4);
INSERT INTO `dvcc_lich_tang_ca` VALUES (2, 33, '2023-09-17', '2023-09-17', '10:48', '10:47', 12, 12, 4);
INSERT INTO `dvcc_lich_tang_ca` VALUES (3, 33, '2023-9-19', '2023-08-25', '16:44:00', '16:44:00', 21, 20, 0);
INSERT INTO `dvcc_lich_tang_ca` VALUES (4, 33, '2023-9-19', '2023-08-25', '16:44:00', '16:44:00', 21, 20, 1);

-- ----------------------------
-- Table structure for dvcc_phep_nghi
-- ----------------------------
DROP TABLE IF EXISTS `dvcc_phep_nghi`;
CREATE TABLE `dvcc_phep_nghi`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NOT NULL,
  `ngay_bat_dau` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ca_bat_dau` int(11) NOT NULL,
  `ngay_ket_thuc` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ca_ket_thuc` int(11) NOT NULL,
  `ly_do` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `quan_ly_truc_tiep` int(11) NULL DEFAULT NULL,
  `nguoi_duyet` int(11) NULL DEFAULT NULL,
  `nguoi_thay_the` int(11) NULL DEFAULT NULL,
  `phepnghi_status` int(11) NOT NULL DEFAULT 1 COMMENT '0 deleted, 1 chua chap thuan, 2 da chap thuan, 3 qua han',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `nv_id`(`nv_id`) USING BTREE,
  INDEX `quan_ly_truc_tiep`(`quan_ly_truc_tiep`) USING BTREE,
  INDEX `nguoi_duyet`(`nguoi_duyet`) USING BTREE,
  INDEX `nguoi_thay_the`(`nguoi_thay_the`) USING BTREE,
  CONSTRAINT `dvcc_phep_nghi_ibfk_1` FOREIGN KEY (`nv_id`) REFERENCES `ns_nhanvien` (`nv_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `dvcc_phep_nghi_ibfk_2` FOREIGN KEY (`quan_ly_truc_tiep`) REFERENCES `ns_nhanvien` (`nv_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `dvcc_phep_nghi_ibfk_3` FOREIGN KEY (`nguoi_duyet`) REFERENCES `ns_nhanvien` (`nv_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `dvcc_phep_nghi_ibfk_4` FOREIGN KEY (`nguoi_thay_the`) REFERENCES `ns_nhanvien` (`nv_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of dvcc_phep_nghi
-- ----------------------------
INSERT INTO `dvcc_phep_nghi` VALUES (1, 33, '2023-09-17', 1, '2023-09-23', 2, 'alo', NULL, 13, NULL, 4);
INSERT INTO `dvcc_phep_nghi` VALUES (2, 33, '2023-09-17', 1, '2023-09-22', 2, 'qqq', NULL, 12, NULL, 3);
INSERT INTO `dvcc_phep_nghi` VALUES (3, 33, '2023-09-23', 1, '2023-09-22', 2, 'Tôi muốn nghỉ', 25, 14, NULL, 4);
INSERT INTO `dvcc_phep_nghi` VALUES (4, 33, '2023-10-15', 1, '2023-10-15', 2, 'Hôm nay tôi bận', 28, 28, NULL, 0);

-- ----------------------------
-- Table structure for dvcc_tinh_cong
-- ----------------------------
DROP TABLE IF EXISTS `dvcc_tinh_cong`;
CREATE TABLE `dvcc_tinh_cong`  (
  `tinh_cong_id` int(11) NOT NULL AUTO_INCREMENT,
  `tinh_cong_date` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `tinh_cong_nv_id` int(11) NOT NULL,
  `tinh_cong_value` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tinh_cong_edited_value` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tinh_cong_time_edited` timestamp NULL DEFAULT NULL,
  `tinh_cong_offical_value` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `luong_id` int(11) NOT NULL,
  PRIMARY KEY (`tinh_cong_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of dvcc_tinh_cong
-- ----------------------------
INSERT INTO `dvcc_tinh_cong` VALUES (1, '2023-10-13', 33, '1', NULL, NULL, '1', 337);
INSERT INTO `dvcc_tinh_cong` VALUES (2, '2023-10-14', 63, '1', NULL, NULL, '1', 338);
INSERT INTO `dvcc_tinh_cong` VALUES (3, '2023-10-15', 64, '1', NULL, NULL, '1', 339);

-- ----------------------------
-- Table structure for dvcc_xin_di_tre
-- ----------------------------
DROP TABLE IF EXISTS `dvcc_xin_di_tre`;
CREATE TABLE `dvcc_xin_di_tre`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NOT NULL,
  `ngay` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `gio_di_tre` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ly_do` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `quan_ly_truc_tiep` int(11) NULL DEFAULT NULL,
  `nguoi_duyet` int(11) NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '0: da xoa, 1: đợi duyệt: 2: được duyệt, 3: đã quá hạn ',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `nv_id`(`nv_id`) USING BTREE,
  INDEX `quan_ly_truc_tiep`(`quan_ly_truc_tiep`) USING BTREE,
  INDEX `nguoi_duyet`(`nguoi_duyet`) USING BTREE,
  CONSTRAINT `dvcc_xin_di_tre_ibfk_1` FOREIGN KEY (`nv_id`) REFERENCES `ns_nhanvien` (`nv_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `dvcc_xin_di_tre_ibfk_2` FOREIGN KEY (`quan_ly_truc_tiep`) REFERENCES `ns_nhanvien` (`nv_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `dvcc_xin_di_tre_ibfk_3` FOREIGN KEY (`nguoi_duyet`) REFERENCES `ns_nhanvien` (`nv_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of dvcc_xin_di_tre
-- ----------------------------
INSERT INTO `dvcc_xin_di_tre` VALUES (1, 16, '', '09:47', '', NULL, NULL, 1);
INSERT INTO `dvcc_xin_di_tre` VALUES (2, 33, '', '', '', NULL, NULL, 1);
INSERT INTO `dvcc_xin_di_tre` VALUES (3, 33, '2023-09-17', '', 'zxcx', NULL, NULL, 1);
INSERT INTO `dvcc_xin_di_tre` VALUES (4, 16, '', '', '', NULL, NULL, 1);
INSERT INTO `dvcc_xin_di_tre` VALUES (5, 16, '', '', '', NULL, NULL, 1);
INSERT INTO `dvcc_xin_di_tre` VALUES (6, 16, '', '', '', NULL, NULL, 1);
INSERT INTO `dvcc_xin_di_tre` VALUES (7, 16, '2023-09-23', '10:26', 'Abc', NULL, NULL, 1);
INSERT INTO `dvcc_xin_di_tre` VALUES (8, 33, '2023-09-28', '03:55:03', '134545345', NULL, NULL, 1);
INSERT INTO `dvcc_xin_di_tre` VALUES (9, 16, '', '16:44', 'tetsing', NULL, NULL, 1);
INSERT INTO `dvcc_xin_di_tre` VALUES (10, 63, '2023-10-13', '12:38:51', 'Mệt\n', NULL, NULL, 1);

-- ----------------------------
-- Table structure for dvcc_xin_ve_som
-- ----------------------------
DROP TABLE IF EXISTS `dvcc_xin_ve_som`;
CREATE TABLE `dvcc_xin_ve_som`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NOT NULL,
  `ngay` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `gio_ve_som` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ly_do` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `quan_ly_truc_tiep` int(11) NULL DEFAULT NULL,
  `nguoi_duyet` int(11) NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '0: da xoa, 1: đợi duyệt: 2: được duyệt, 3: đã quá hạn ',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `nv_id`(`nv_id`) USING BTREE,
  INDEX `quan_ly_truc_tiep`(`quan_ly_truc_tiep`) USING BTREE,
  INDEX `nguoi_duyet`(`nguoi_duyet`) USING BTREE,
  CONSTRAINT `dvcc_xin_ve_som_ibfk_1` FOREIGN KEY (`nv_id`) REFERENCES `ns_nhanvien` (`nv_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `dvcc_xin_ve_som_ibfk_2` FOREIGN KEY (`quan_ly_truc_tiep`) REFERENCES `ns_nhanvien` (`nv_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `dvcc_xin_ve_som_ibfk_3` FOREIGN KEY (`nguoi_duyet`) REFERENCES `ns_nhanvien` (`nv_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of dvcc_xin_ve_som
-- ----------------------------
INSERT INTO `dvcc_xin_ve_som` VALUES (1, 33, '2023-09-07', '09:56', 'â', NULL, NULL, 1);

-- ----------------------------
-- Table structure for kiosk_user_permissions
-- ----------------------------
DROP TABLE IF EXISTS `kiosk_user_permissions`;
CREATE TABLE `kiosk_user_permissions`  (
  `perms_id` int(11) NOT NULL AUTO_INCREMENT,
  `perms_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `perms_type` varchar(222) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0',
  `user_id` int(11) NULL DEFAULT 0,
  PRIMARY KEY (`perms_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 140 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of kiosk_user_permissions
-- ----------------------------
INSERT INTO `kiosk_user_permissions` VALUES (52, 'NHẬP HÀNG', 'KHOTONG', 1);
INSERT INTO `kiosk_user_permissions` VALUES (53, 'XUẤT HÀNG', 'KHOTONG', 1);
INSERT INTO `kiosk_user_permissions` VALUES (54, 'KIỂM KHO', 'KHOTONG', 1);
INSERT INTO `kiosk_user_permissions` VALUES (55, 'SỔ QUỸ', 'KHOTONG', 1);
INSERT INTO `kiosk_user_permissions` VALUES (56, 'HIỆU QUẢ', 'KHOTONG', 1);
INSERT INTO `kiosk_user_permissions` VALUES (57, 'ĐỊNH DANH ĐẠI LÝ', 'KHOTONG', 1);
INSERT INTO `kiosk_user_permissions` VALUES (58, 'ĐỊNH DANH TÊN HÀNG', 'KHOTONG', 1);
INSERT INTO `kiosk_user_permissions` VALUES (59, 'ĐỊNH DANH TÊN KHO', 'KHOTONG', 1);
INSERT INTO `kiosk_user_permissions` VALUES (60, 'THANH TOÁN TIỀN ĐẠI LÝ', 'SOQUY', 1);
INSERT INTO `kiosk_user_permissions` VALUES (61, 'TÍNH TIỀN NỘP PHƠI NV', 'SOQUY', 1);
INSERT INTO `kiosk_user_permissions` VALUES (62, 'XEM ĐẶT HÀNG', 'SOQUY', 1);
INSERT INTO `kiosk_user_permissions` VALUES (63, 'XEM CÔNG NỢ ĐẠI LÝ', 'SOQUY', 1);
INSERT INTO `kiosk_user_permissions` VALUES (64, 'XEM TỔNG HỢP THU CHI', 'SOQUY', 1);
INSERT INTO `kiosk_user_permissions` VALUES (79, 'QUẢN LÝ HỢP ĐỒNG', 'INTELPLANNING', 1);
INSERT INTO `kiosk_user_permissions` VALUES (82, 'XEM BÁO CÁO CÔNG VIỆC', 'INTELPLANNING', 1);
INSERT INTO `kiosk_user_permissions` VALUES (83, 'XEM BÁO CÁO DOANH THU', 'INTELPLANNING', 1);
INSERT INTO `kiosk_user_permissions` VALUES (84, 'XEM BÁO CÁO CÔNG NỢ', 'INTELPLANNING', 1);
INSERT INTO `kiosk_user_permissions` VALUES (85, 'NHÂN VIÊN BÁO CÁO (GIAO)', 'INTELPLANNING', 1);
INSERT INTO `kiosk_user_permissions` VALUES (137, 'QUẢN LÝ GIAO VIỆC', 'INTELPLANNING', 1);
INSERT INTO `kiosk_user_permissions` VALUES (138, 'QUẢN LÝ THANH TOÁN', 'INTELPLANNING', 1);
INSERT INTO `kiosk_user_permissions` VALUES (139, 'NHÂN VIÊN BÁO CÁO (NHẬN)', 'INTELPLANNING', 1);

-- ----------------------------
-- Table structure for ns_cauhinh
-- ----------------------------
DROP TABLE IF EXISTS `ns_cauhinh`;
CREATE TABLE `ns_cauhinh`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `kyhieu` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `giatri` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT NULL,
  `hople` int(11) NULL DEFAULT NULL,
  `update_new` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_cauhinh
-- ----------------------------
INSERT INTO `ns_cauhinh` VALUES (1, 'Giờ Công Chuẩn', 'GCN', '8', NULL, NULL, NULL);
INSERT INTO `ns_cauhinh` VALUES (2, 'Công Thức Lương 1 Giờ', 'L1G', '@LCT@/(@TDAY@*8)', 1, NULL, '@LCT@/(30*8)');
INSERT INTO `ns_cauhinh` VALUES (3, 'Công Ty Đóng (BHXH)', '', '35', NULL, NULL, NULL);
INSERT INTO `ns_cauhinh` VALUES (4, 'Tự Đóng(BHXH)', NULL, '15', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for ns_danhmuc_phongban
-- ----------------------------
DROP TABLE IF EXISTS `ns_danhmuc_phongban`;
CREATE TABLE `ns_danhmuc_phongban`  (
  `danhmuc_id` int(11) NOT NULL AUTO_INCREMENT,
  `danhmuc_name` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `danhmuc_phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `danhmuc_order` int(11) NULL DEFAULT NULL,
  `danhmuc_status` int(11) NULL DEFAULT 1,
  `danhmuc_type` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `danhmuc_mota` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `danhmuc_chamcong` int(11) NOT NULL,
  `company` int(10) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`danhmuc_id`) USING BTREE,
  INDEX `company`(`company`) USING BTREE,
  CONSTRAINT `ns_danhmuc_phongban_ibfk_1` FOREIGN KEY (`company`) REFERENCES `company` (`company_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE = InnoDB AUTO_INCREMENT = 104 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_danhmuc_phongban
-- ----------------------------
INSERT INTO `ns_danhmuc_phongban` VALUES (1, 'Ban Lãnh Đạo', NULL, 3, 2, 'VP', NULL, 0, NULL);
INSERT INTO `ns_danhmuc_phongban` VALUES (10, 'Ban Lãnh Đạo', NULL, 1, 1, 'VP', NULL, 1, 1);
INSERT INTO `ns_danhmuc_phongban` VALUES (11, 'Phòng KT Tài Chính', NULL, 2, 1, 'VP', NULL, 1, 1);
INSERT INTO `ns_danhmuc_phongban` VALUES (12, 'Phòng HC Nhân Sự', NULL, 3, 1, 'VP', NULL, 1, 1);
INSERT INTO `ns_danhmuc_phongban` VALUES (13, 'Phòng DV QT MT', NULL, 4, 1, 'VP', NULL, 1, 1);
INSERT INTO `ns_danhmuc_phongban` VALUES (58, 'Ban lãnh đạo', NULL, NULL, 1, NULL, NULL, 0, 30);
INSERT INTO `ns_danhmuc_phongban` VALUES (59, 'Hành chính', NULL, NULL, 1, NULL, NULL, 0, 30);
INSERT INTO `ns_danhmuc_phongban` VALUES (60, 'Kế toán', NULL, NULL, 1, NULL, NULL, 0, 30);
INSERT INTO `ns_danhmuc_phongban` VALUES (61, 'Kho', NULL, NULL, 1, NULL, NULL, 0, 30);
INSERT INTO `ns_danhmuc_phongban` VALUES (62, 'alo', NULL, NULL, 1, NULL, NULL, 0, 30);
INSERT INTO `ns_danhmuc_phongban` VALUES (63, 'Ban lãnh đạo', NULL, NULL, 1, NULL, NULL, 0, 31);
INSERT INTO `ns_danhmuc_phongban` VALUES (64, 'Hành chính', NULL, NULL, 1, NULL, NULL, 0, 31);
INSERT INTO `ns_danhmuc_phongban` VALUES (65, 'Kế toán', NULL, NULL, 1, NULL, NULL, 0, 31);
INSERT INTO `ns_danhmuc_phongban` VALUES (66, 'Kho', NULL, NULL, 1, NULL, NULL, 0, 31);
INSERT INTO `ns_danhmuc_phongban` VALUES (67, 'Ban Lãnh Đạo', NULL, NULL, 1, NULL, NULL, 0, 32);
INSERT INTO `ns_danhmuc_phongban` VALUES (68, 'Phòng Hành chính', NULL, NULL, 1, NULL, NULL, 0, 32);
INSERT INTO `ns_danhmuc_phongban` VALUES (69, 'Phòng Kế toán', NULL, NULL, 1, NULL, NULL, 0, 32);
INSERT INTO `ns_danhmuc_phongban` VALUES (70, 'Kho Vật Tư', NULL, NULL, 1, NULL, NULL, 0, 32);
INSERT INTO `ns_danhmuc_phongban` VALUES (71, 'Ban lãnh đạo', NULL, NULL, 1, NULL, NULL, 0, 33);
INSERT INTO `ns_danhmuc_phongban` VALUES (72, 'Hành chính', NULL, NULL, 1, NULL, NULL, 0, 33);
INSERT INTO `ns_danhmuc_phongban` VALUES (73, 'Kế toán', NULL, NULL, 1, NULL, NULL, 0, 33);
INSERT INTO `ns_danhmuc_phongban` VALUES (74, 'Kho', NULL, NULL, 1, NULL, NULL, 0, 33);
INSERT INTO `ns_danhmuc_phongban` VALUES (75, 'Ban lãnh đạo', NULL, NULL, 1, NULL, NULL, 0, 33);
INSERT INTO `ns_danhmuc_phongban` VALUES (76, 'Hành chính', NULL, NULL, 1, NULL, NULL, 0, 33);
INSERT INTO `ns_danhmuc_phongban` VALUES (77, 'Kế toán', NULL, NULL, 1, NULL, NULL, 0, 33);
INSERT INTO `ns_danhmuc_phongban` VALUES (78, 'Kho', NULL, NULL, 1, NULL, NULL, 0, 33);
INSERT INTO `ns_danhmuc_phongban` VALUES (79, 'Phòng Cung Ứng', NULL, NULL, 1, NULL, NULL, 0, 32);
INSERT INTO `ns_danhmuc_phongban` VALUES (80, 'Phòng Nhân Sự', NULL, NULL, 0, NULL, NULL, 0, 32);
INSERT INTO `ns_danhmuc_phongban` VALUES (81, 'Phòng IT', NULL, NULL, 1, NULL, NULL, 0, 32);
INSERT INTO `ns_danhmuc_phongban` VALUES (82, 'Phòng Xây Dựng', NULL, NULL, 1, NULL, NULL, 0, 32);
INSERT INTO `ns_danhmuc_phongban` VALUES (83, 'Phòng TNMT', NULL, NULL, 1, NULL, NULL, 0, 32);
INSERT INTO `ns_danhmuc_phongban` VALUES (84, 'Phòng Chăm Sóc Khách Hàng', NULL, NULL, 1, NULL, NULL, 0, 32);
INSERT INTO `ns_danhmuc_phongban` VALUES (85, 'Phòng GD & ĐT', NULL, NULL, 1, NULL, NULL, 0, 32);
INSERT INTO `ns_danhmuc_phongban` VALUES (86, 'Phòng Hành Chính', NULL, NULL, 0, NULL, NULL, 0, 32);
INSERT INTO `ns_danhmuc_phongban` VALUES (87, 'Phòng Thanh Tra', NULL, NULL, 1, NULL, NULL, 0, 32);
INSERT INTO `ns_danhmuc_phongban` VALUES (88, 'Phòng Giám Sát', NULL, NULL, 1, NULL, NULL, 0, 32);
INSERT INTO `ns_danhmuc_phongban` VALUES (89, 'Khối Bán Lẻ', NULL, NULL, 1, NULL, NULL, 0, 32);
INSERT INTO `ns_danhmuc_phongban` VALUES (90, 'Phòng Lao Động TB-XH', NULL, NULL, 1, NULL, NULL, 0, 32);
INSERT INTO `ns_danhmuc_phongban` VALUES (91, 'AABB', NULL, NULL, 0, NULL, NULL, 0, 32);
INSERT INTO `ns_danhmuc_phongban` VALUES (92, 'Ban lãnh đạo', NULL, NULL, 1, NULL, NULL, 0, 35);
INSERT INTO `ns_danhmuc_phongban` VALUES (93, 'Hành chính', NULL, NULL, 1, NULL, NULL, 0, 35);
INSERT INTO `ns_danhmuc_phongban` VALUES (94, 'Kế toán', NULL, NULL, 1, NULL, NULL, 0, 35);
INSERT INTO `ns_danhmuc_phongban` VALUES (95, 'Kho', NULL, NULL, 1, NULL, NULL, 0, 35);
INSERT INTO `ns_danhmuc_phongban` VALUES (96, 'Ban lãnh đạo', NULL, NULL, 1, NULL, NULL, 0, 34);
INSERT INTO `ns_danhmuc_phongban` VALUES (97, 'Hành chính', NULL, NULL, 1, NULL, NULL, 0, 34);
INSERT INTO `ns_danhmuc_phongban` VALUES (98, 'Kế toán', NULL, NULL, 1, NULL, NULL, 0, 34);
INSERT INTO `ns_danhmuc_phongban` VALUES (99, 'Kho', NULL, NULL, 1, NULL, NULL, 0, 34);
INSERT INTO `ns_danhmuc_phongban` VALUES (100, 'Đội Hiện Trường', NULL, NULL, 0, NULL, NULL, 0, 1);
INSERT INTO `ns_danhmuc_phongban` VALUES (101, 'Ban lãnh đạo', NULL, NULL, 1, NULL, NULL, 0, 36);
INSERT INTO `ns_danhmuc_phongban` VALUES (102, 'Hành chính', NULL, NULL, 1, NULL, NULL, 0, 36);
INSERT INTO `ns_danhmuc_phongban` VALUES (103, 'Kế toán', NULL, NULL, 1, NULL, NULL, 0, 36);

-- ----------------------------
-- Table structure for ns_danhmuc_phongban_excel
-- ----------------------------
DROP TABLE IF EXISTS `ns_danhmuc_phongban_excel`;
CREATE TABLE `ns_danhmuc_phongban_excel`  (
  `danhmuc_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `danhmuc_name` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `danhmuc_phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `danhmuc_order` int(11) NULL DEFAULT NULL,
  `danhmuc_status` int(11) NULL DEFAULT 1,
  `danhmuc_type` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `row_number` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`danhmuc_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_danhmuc_phongban_excel
-- ----------------------------

-- ----------------------------
-- Table structure for ns_dot_hoso
-- ----------------------------
DROP TABLE IF EXISTS `ns_dot_hoso`;
CREATE TABLE `ns_dot_hoso`  (
  `hoso_id` int(11) NOT NULL AUTO_INCREMENT,
  `hoso_date` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `hoso_hang` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `hoso_loaixe` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `hoso_taitrong` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `hoso_hinhthuc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `hoso_status` int(1) NULL DEFAULT 1,
  `ns_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`hoso_id`) USING BTREE,
  INDEX `ns_id`(`ns_id`) USING BTREE,
  CONSTRAINT `ns_id` FOREIGN KEY (`ns_id`) REFERENCES `ns_nhanvien` (`nv_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ns_dot_hoso
-- ----------------------------
INSERT INTO `ns_dot_hoso` VALUES (1, '2023-10-12', 'A1', 'xe máy', '2 người', 'không thời hạn', 1, 40);
INSERT INTO `ns_dot_hoso` VALUES (2, '2023-10-12', 'E1', 'xe khách', '38 chỗ', NULL, 1, 40);
INSERT INTO `ns_dot_hoso` VALUES (3, NULL, 'B1', NULL, NULL, NULL, 0, 40);
INSERT INTO `ns_dot_hoso` VALUES (4, NULL, 'B1', NULL, NULL, NULL, 0, 40);

-- ----------------------------
-- Table structure for ns_dot_tngt
-- ----------------------------
DROP TABLE IF EXISTS `ns_dot_tngt`;
CREATE TABLE `ns_dot_tngt`  (
  `tngt_id` int(11) NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NULL DEFAULT NULL,
  `tngt_thoigian` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tngt_diadiem` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tngt_thiethai` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tngt_nguyennhan` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tngt_status` int(1) NULL DEFAULT 1,
  PRIMARY KEY (`tngt_id`) USING BTREE,
  INDEX `nv_id`(`nv_id`) USING BTREE,
  CONSTRAINT `nv_id` FOREIGN KEY (`nv_id`) REFERENCES `ns_nhanvien` (`nv_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ns_dot_tngt
-- ----------------------------
INSERT INTO `ns_dot_tngt` VALUES (1, 40, '2013-10-01', 'Thủ Đức', 'không có', 'Quá Tốc Độ', 1);
INSERT INTO `ns_dot_tngt` VALUES (2, 60, '2023-10-01', 'Thủ Đức', 'Hỏng đèn hậu', 'Xe phía sau không giữ khoảng cách an toàn', 1);
INSERT INTO `ns_dot_tngt` VALUES (3, 60, '2023-10-01', 'Suối Tiên', '12345', '12345', 0);
INSERT INTO `ns_dot_tngt` VALUES (4, 60, '2023-10-01', 'Ngã Tư Thủ Đức', '12345', '12345', 0);
INSERT INTO `ns_dot_tngt` VALUES (5, 3, NULL, '12345', NULL, NULL, 1);

-- ----------------------------
-- Table structure for ns_dotbhxh
-- ----------------------------
DROP TABLE IF EXISTS `ns_dotbhxh`;
CREATE TABLE `ns_dotbhxh`  (
  `bhxh_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `bhxh_name` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `bhxh_month` date NULL DEFAULT NULL,
  `bhxh_status` int(11) NULL DEFAULT NULL,
  `bhxh_order` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`bhxh_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_dotbhxh
-- ----------------------------
INSERT INTO `ns_dotbhxh` VALUES (1, 'BHXH THÁNG MỚI', '2023-08-01', 1, 100);
INSERT INTO `ns_dotbhxh` VALUES (2, 'BHXH-Canh', '2023-09-01', 1, NULL);

-- ----------------------------
-- Table structure for ns_dotbhxh_nvthamgia
-- ----------------------------
DROP TABLE IF EXISTS `ns_dotbhxh_nvthamgia`;
CREATE TABLE `ns_dotbhxh_nvthamgia`  (
  `thamgia_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NULL DEFAULT NULL,
  `bhxh_id` int(11) NULL DEFAULT NULL,
  `nv_name` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `bhxh_chuthich` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `bhxh_ngaythamgia` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `bhxh_sosobhxh` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `bhxh_luongdongbhxh` int(11) NULL DEFAULT 0,
  `bhxh_congtydong` int(11) NULL DEFAULT 0,
  `bhxh_nhanviendong` int(11) NULL DEFAULT 0,
  PRIMARY KEY (`thamgia_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_dotbhxh_nvthamgia
-- ----------------------------
INSERT INTO `ns_dotbhxh_nvthamgia` VALUES (1, 16, 1, 'Nguyễn Thị Trang', NULL, NULL, NULL, 0, 0, 0);
INSERT INTO `ns_dotbhxh_nvthamgia` VALUES (2, 18, 1, 'Nguyễn Thị Thúy Vi', NULL, NULL, NULL, 0, 0, 0);
INSERT INTO `ns_dotbhxh_nvthamgia` VALUES (3, 18, 2, 'Nguyễn Thị Thúy Vi', NULL, NULL, NULL, 0, 200000, 354000);

-- ----------------------------
-- Table structure for ns_dotcongdoan
-- ----------------------------
DROP TABLE IF EXISTS `ns_dotcongdoan`;
CREATE TABLE `ns_dotcongdoan`  (
  `congdoan_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `congdoan_name` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `congdoan_month` date NULL DEFAULT NULL,
  `congdoan_status` int(11) NULL DEFAULT NULL,
  `congdoan_order` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`congdoan_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_dotcongdoan
-- ----------------------------

-- ----------------------------
-- Table structure for ns_dotcongdoan_nvthamgia
-- ----------------------------
DROP TABLE IF EXISTS `ns_dotcongdoan_nvthamgia`;
CREATE TABLE `ns_dotcongdoan_nvthamgia`  (
  `thamgia_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NULL DEFAULT NULL,
  `congdoan_id` int(11) NULL DEFAULT NULL,
  `nv_name` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `congdoan_chuthich` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `congdoan_ngaythamgia` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `congdoan_sosocongdoan` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `congdoan_luongdongcongdoan` int(11) NULL DEFAULT 0,
  `congdoan_congtydong` int(11) NULL DEFAULT 0,
  `congdoan_nhanviendong` int(11) NULL DEFAULT 0,
  PRIMARY KEY (`thamgia_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_dotcongdoan_nvthamgia
-- ----------------------------

-- ----------------------------
-- Table structure for ns_dotksk
-- ----------------------------
DROP TABLE IF EXISTS `ns_dotksk`;
CREATE TABLE `ns_dotksk`  (
  `ksk_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ksk_name` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ksk_benhvien` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ksk_date` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ksk_status` int(11) NULL DEFAULT 1,
  `ksk_order` int(11) NULL DEFAULT NULL,
  `company_id` int(10) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`ksk_id`) USING BTREE,
  INDEX `company_id`(`company_id`) USING BTREE,
  CONSTRAINT `company_id` FOREIGN KEY (`company_id`) REFERENCES `company` (`company_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_dotksk
-- ----------------------------
INSERT INTO `ns_dotksk` VALUES (1, NULL, 'Bệnh Viện Thủ Đức', '2023-10-13', 1, NULL, 1);
INSERT INTO `ns_dotksk` VALUES (2, NULL, 'Bệnh Viện Đa Khoa Quận 7', '2023-10-13', 1, NULL, 1);

-- ----------------------------
-- Table structure for ns_dotksk_nvthamgia
-- ----------------------------
DROP TABLE IF EXISTS `ns_dotksk_nvthamgia`;
CREATE TABLE `ns_dotksk_nvthamgia`  (
  `thamgia_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NULL DEFAULT NULL,
  `nv_name` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ksk_id` int(11) UNSIGNED NULL DEFAULT NULL,
  `ksk_ketquakiemtra` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ksk_status` int(1) NULL DEFAULT 1,
  PRIMARY KEY (`thamgia_id`) USING BTREE,
  INDEX `sks_id`(`ksk_id`) USING BTREE,
  CONSTRAINT `sks_id` FOREIGN KEY (`ksk_id`) REFERENCES `ns_dotksk` (`ksk_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_dotksk_nvthamgia
-- ----------------------------
INSERT INTO `ns_dotksk_nvthamgia` VALUES (1, 60, NULL, 1, 'Đạt', 1);
INSERT INTO `ns_dotksk_nvthamgia` VALUES (3, 60, NULL, 2, 'Chưa Đạt 222', 0);
INSERT INTO `ns_dotksk_nvthamgia` VALUES (4, 60, NULL, 2, 'Đạt Sức Khoẻ Loại 2', 1);
INSERT INTO `ns_dotksk_nvthamgia` VALUES (5, 12, NULL, 1, NULL, 1);

-- ----------------------------
-- Table structure for ns_dotnophoi
-- ----------------------------
DROP TABLE IF EXISTS `ns_dotnophoi`;
CREATE TABLE `ns_dotnophoi`  (
  `nophoi_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nophoi_name` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nophoi_month` date NULL DEFAULT NULL,
  `nophoi_sotien` int(11) NULL DEFAULT NULL,
  `nophoi_status` int(11) NULL DEFAULT NULL,
  `nophoi_order` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`nophoi_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_dotnophoi
-- ----------------------------

-- ----------------------------
-- Table structure for ns_dotnophoi_nvthamgia
-- ----------------------------
DROP TABLE IF EXISTS `ns_dotnophoi_nvthamgia`;
CREATE TABLE `ns_dotnophoi_nvthamgia`  (
  `thamgia_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NULL DEFAULT NULL,
  `nophoi_id` int(11) NULL DEFAULT NULL,
  `nv_name` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nophoi_chuthich` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nophoi_sotien` int(11) NULL DEFAULT 0,
  PRIMARY KEY (`thamgia_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_dotnophoi_nvthamgia
-- ----------------------------

-- ----------------------------
-- Table structure for ns_dotphat
-- ----------------------------
DROP TABLE IF EXISTS `ns_dotphat`;
CREATE TABLE `ns_dotphat`  (
  `phat_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `phat_name` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phat_month` date NULL DEFAULT NULL,
  `phat_sotien` int(11) NULL DEFAULT NULL,
  `phat_status` int(11) NULL DEFAULT NULL,
  `phat_order` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`phat_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_dotphat
-- ----------------------------

-- ----------------------------
-- Table structure for ns_dotphat_nvthamgia
-- ----------------------------
DROP TABLE IF EXISTS `ns_dotphat_nvthamgia`;
CREATE TABLE `ns_dotphat_nvthamgia`  (
  `thamgia_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NULL DEFAULT NULL,
  `phat_id` int(11) NULL DEFAULT NULL,
  `nv_name` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phat_chuthich` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phat_sotien` int(11) NULL DEFAULT 0,
  `phat_xuly` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phat_status` int(1) NULL DEFAULT 1,
  `phat_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phat_thoigian` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`thamgia_id`) USING BTREE,
  INDEX `nv_id`(`nv_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_dotphat_nvthamgia
-- ----------------------------
INSERT INTO `ns_dotphat_nvthamgia` VALUES (1, 40, NULL, NULL, 'Biển Số Xe: 59E1-99999', 0, 'Phạt tiền: 500000', 1, 'Lấn làn', '2023-10-11');
INSERT INTO `ns_dotphat_nvthamgia` VALUES (2, 40, NULL, NULL, 'aaaa', 0, 'aaaa', 0, 'Đi Ngược Chiềua', '2023-10-10');
INSERT INTO `ns_dotphat_nvthamgia` VALUES (3, 40, NULL, NULL, 'aa', 0, 'aaaaa', 0, 'Chở 3aa', '2023-10-11');

-- ----------------------------
-- Table structure for ns_dottaphuan
-- ----------------------------
DROP TABLE IF EXISTS `ns_dottaphuan`;
CREATE TABLE `ns_dottaphuan`  (
  `taphuan_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `taphuan_name` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `taphuan_date` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `taphuan_status` int(11) NULL DEFAULT NULL,
  `taphuan_order` int(11) NULL DEFAULT NULL,
  `taphuan_donvi` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `taphuan_hethan` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `company_id` int(10) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`taphuan_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_dottaphuan
-- ----------------------------
INSERT INTO `ns_dottaphuan` VALUES (5, 'Đạo đức nghề nghiệp', '2023-10-13', 1, NULL, 'anbinh', '2024-10-13', 1);
INSERT INTO `ns_dottaphuan` VALUES (6, 'Tác Phong Chăm sóc khách hàng', '2023-10-13', 1, NULL, 'anbinh', '2024-10-13', 1);
INSERT INTO `ns_dottaphuan` VALUES (7, 'Kỹ năng xử lý tình huống', '2023-10-10', 1, NULL, 'anbinh', '2024-10-10', 1);

-- ----------------------------
-- Table structure for ns_dottaphuan_nvthamgia
-- ----------------------------
DROP TABLE IF EXISTS `ns_dottaphuan_nvthamgia`;
CREATE TABLE `ns_dottaphuan_nvthamgia`  (
  `thamgiath_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NULL DEFAULT NULL,
  `nv_name` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `taphuan_id` int(11) UNSIGNED NULL DEFAULT NULL,
  `taphuan_status` int(1) NULL DEFAULT 1,
  PRIMARY KEY (`thamgiath_id`) USING BTREE,
  INDEX `taphuan_id`(`taphuan_id`) USING BTREE,
  CONSTRAINT `taphuan_id` FOREIGN KEY (`taphuan_id`) REFERENCES `ns_dottaphuan` (`taphuan_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_dottaphuan_nvthamgia
-- ----------------------------
INSERT INTO `ns_dottaphuan_nvthamgia` VALUES (1, 60, NULL, 5, 1);
INSERT INTO `ns_dottaphuan_nvthamgia` VALUES (2, 60, NULL, 6, 0);
INSERT INTO `ns_dottaphuan_nvthamgia` VALUES (3, 60, NULL, 7, 0);
INSERT INTO `ns_dottaphuan_nvthamgia` VALUES (4, 60, NULL, 6, 0);
INSERT INTO `ns_dottaphuan_nvthamgia` VALUES (5, 60, NULL, 6, 1);
INSERT INTO `ns_dottaphuan_nvthamgia` VALUES (6, 60, NULL, 7, 1);
INSERT INTO `ns_dottaphuan_nvthamgia` VALUES (7, 60, NULL, 6, 1);
INSERT INTO `ns_dottaphuan_nvthamgia` VALUES (8, 12, NULL, 5, 1);

-- ----------------------------
-- Table structure for ns_dotthuong
-- ----------------------------
DROP TABLE IF EXISTS `ns_dotthuong`;
CREATE TABLE `ns_dotthuong`  (
  `thuong_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `thuong_name` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `thuong_month` date NULL DEFAULT NULL,
  `thuong_sotien` int(11) NULL DEFAULT NULL,
  `thuong_status` int(11) NULL DEFAULT NULL,
  `thuong_order` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`thuong_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_dotthuong
-- ----------------------------

-- ----------------------------
-- Table structure for ns_dotthuong_nvthamgia
-- ----------------------------
DROP TABLE IF EXISTS `ns_dotthuong_nvthamgia`;
CREATE TABLE `ns_dotthuong_nvthamgia`  (
  `thamgia_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NULL DEFAULT NULL,
  `thuong_id` int(11) NULL DEFAULT NULL,
  `nv_name` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `thuong_chuthich` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `thuong_sotien` int(11) NULL DEFAULT 0,
  PRIMARY KEY (`thamgia_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_dotthuong_nvthamgia
-- ----------------------------

-- ----------------------------
-- Table structure for ns_dotungluong
-- ----------------------------
DROP TABLE IF EXISTS `ns_dotungluong`;
CREATE TABLE `ns_dotungluong`  (
  `ung_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ung_name` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ung_month` date NULL DEFAULT NULL,
  `ung_sotien` int(11) NULL DEFAULT NULL,
  `ung_status` int(11) NULL DEFAULT NULL,
  `ung_order` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`ung_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_dotungluong
-- ----------------------------

-- ----------------------------
-- Table structure for ns_dotungluong_nvthamgia
-- ----------------------------
DROP TABLE IF EXISTS `ns_dotungluong_nvthamgia`;
CREATE TABLE `ns_dotungluong_nvthamgia`  (
  `thamgia_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NULL DEFAULT NULL,
  `ung_id` int(11) NULL DEFAULT NULL,
  `nv_name` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ung_chuthich` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ung_sotien` int(11) NULL DEFAULT 0,
  PRIMARY KEY (`thamgia_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_dotungluong_nvthamgia
-- ----------------------------

-- ----------------------------
-- Table structure for ns_kyquy_chitiet
-- ----------------------------
DROP TABLE IF EXISTS `ns_kyquy_chitiet`;
CREATE TABLE `ns_kyquy_chitiet`  (
  `chitiet_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NULL DEFAULT NULL,
  `nv_name` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `chitiet_ngaythang` datetime NULL DEFAULT NULL,
  `chitiet_sotien` int(11) NULL DEFAULT 0,
  `chitiet_lydo` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `chitiet_status` int(11) NULL DEFAULT NULL,
  `chitiet_nguoithutien` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `chitiet_chucvu` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `chitiet_inputdate` datetime NULL DEFAULT NULL,
  `chitiet_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`chitiet_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_kyquy_chitiet
-- ----------------------------

-- ----------------------------
-- Table structure for ns_kyquy_giaichi
-- ----------------------------
DROP TABLE IF EXISTS `ns_kyquy_giaichi`;
CREATE TABLE `ns_kyquy_giaichi`  (
  `giaichi_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NULL DEFAULT NULL,
  `giaichi_ngaythang` datetime NULL DEFAULT NULL,
  `giaichi_sotien` int(11) NULL DEFAULT 0,
  `giaichi_lydo` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `giaichi_status` int(11) NULL DEFAULT NULL,
  `giaichi_nguoithutien` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `giaichi_chucvu` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `giaichi_inputdate` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`giaichi_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_kyquy_giaichi
-- ----------------------------

-- ----------------------------
-- Table structure for ns_kyquy_nv
-- ----------------------------
DROP TABLE IF EXISTS `ns_kyquy_nv`;
CREATE TABLE `ns_kyquy_nv`  (
  `kyquy_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NULL DEFAULT NULL,
  `nv_name` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `kyquy_so_tien_phai_nop` int(11) NULL DEFAULT 0,
  `kyquy_so_tien_da_ky_quy` int(11) NULL DEFAULT 0,
  `kyquy_so_tien_con_lai` int(11) NULL DEFAULT 0,
  `kyquy_ghi_chu` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `thuc_hien_giai_chi` int(11) NULL DEFAULT NULL,
  `kyquy_status` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`kyquy_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_kyquy_nv
-- ----------------------------

-- ----------------------------
-- Table structure for ns_kyquyvp_chitiet
-- ----------------------------
DROP TABLE IF EXISTS `ns_kyquyvp_chitiet`;
CREATE TABLE `ns_kyquyvp_chitiet`  (
  `chitiet_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NULL DEFAULT NULL,
  `nv_name` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `chitiet_ngaythang` datetime NULL DEFAULT NULL,
  `chitiet_sotien` int(11) NULL DEFAULT 0,
  `chitiet_lydo` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `chitiet_status` int(11) NULL DEFAULT NULL,
  `chitiet_nguoithutien` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `chitiet_chucvu` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `chitiet_inputdate` datetime NULL DEFAULT NULL,
  `chitiet_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`chitiet_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_kyquyvp_chitiet
-- ----------------------------

-- ----------------------------
-- Table structure for ns_kyquyvp_giaichi
-- ----------------------------
DROP TABLE IF EXISTS `ns_kyquyvp_giaichi`;
CREATE TABLE `ns_kyquyvp_giaichi`  (
  `giaichi_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NULL DEFAULT NULL,
  `giaichi_ngaythang` datetime NULL DEFAULT NULL,
  `giaichi_sotien` int(11) NULL DEFAULT 0,
  `giaichi_lydo` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `giaichi_status` int(11) NULL DEFAULT NULL,
  `giaichi_nguoithutien` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `giaichi_chucvu` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `giaichi_inputdate` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`giaichi_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_kyquyvp_giaichi
-- ----------------------------

-- ----------------------------
-- Table structure for ns_kyquyvp_nv
-- ----------------------------
DROP TABLE IF EXISTS `ns_kyquyvp_nv`;
CREATE TABLE `ns_kyquyvp_nv`  (
  `kyquy_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NULL DEFAULT NULL,
  `nv_name` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `kyquy_so_tien_da_ky_quy` int(11) NULL DEFAULT 0,
  `kyquy_so_tien_con_lai` int(11) NULL DEFAULT 0,
  `kyquy_ghi_chu` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `thuc_hien_giai_chi` int(11) NULL DEFAULT NULL,
  `kyquy_status` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`kyquy_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_kyquyvp_nv
-- ----------------------------

-- ----------------------------
-- Table structure for ns_logs_chamcong
-- ----------------------------
DROP TABLE IF EXISTS `ns_logs_chamcong`;
CREATE TABLE `ns_logs_chamcong`  (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `luong_id` int(11) NOT NULL,
  `log_field` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `log_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `log_content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`log_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 27 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ns_logs_chamcong
-- ----------------------------
INSERT INTO `ns_logs_chamcong` VALUES (1, 336, NULL, '2023-10-11 14:51:10', 'Vào ca');
INSERT INTO `ns_logs_chamcong` VALUES (2, 336, NULL, '2023-10-11 14:51:48', 'Ra ca');
INSERT INTO `ns_logs_chamcong` VALUES (3, 336, NULL, '2023-10-11 15:02:27', 'Thay đổi giá trị cũ \"1\" thành giá trị mới \"2\"');
INSERT INTO `ns_logs_chamcong` VALUES (4, 336, NULL, '2023-10-11 15:03:38', 'Thay đổi giá trị cũ \"2\" thành giá trị mới \"0.5_0.5P\"');
INSERT INTO `ns_logs_chamcong` VALUES (5, 336, NULL, '2023-10-11 15:26:31', 'Thay đổi giá trị cũ \"1\" thành giá trị mới \"0.5_0.5P\"');
INSERT INTO `ns_logs_chamcong` VALUES (6, 336, NULL, '2023-10-11 15:27:39', 'Thay đổi giá trị cũ \"0\" thành giá trị mới \"1\"');
INSERT INTO `ns_logs_chamcong` VALUES (7, 336, 'd13', '2023-10-13 11:13:07', 'Vào ca');
INSERT INTO `ns_logs_chamcong` VALUES (8, 336, NULL, '2023-10-13 11:13:32', 'Thay đổi giá trị cũ \"1\" thành giá trị mới \"2\"');
INSERT INTO `ns_logs_chamcong` VALUES (9, 336, 'd13', '2023-10-13 11:16:00', 'Vào ca');
INSERT INTO `ns_logs_chamcong` VALUES (10, 336, 'd13', '2023-10-13 11:16:02', 'Ra ca');
INSERT INTO `ns_logs_chamcong` VALUES (11, 336, 'd13', '2023-10-13 11:25:46', 'Thay đổi giá trị cũ \"1\" thành giá trị mới \"2\"');
INSERT INTO `ns_logs_chamcong` VALUES (12, 336, 'd13', '2023-10-13 13:58:55', 'Thay đổi giá trị cũ \"2\" thành giá trị mới \"1\"');
INSERT INTO `ns_logs_chamcong` VALUES (13, 336, 'd13', '2023-10-13 13:59:00', 'Thay đổi giá trị cũ \"1\" thành giá trị mới \"2\"');
INSERT INTO `ns_logs_chamcong` VALUES (14, 336, 'd13', '2023-10-13 14:07:16', 'Vào ca');
INSERT INTO `ns_logs_chamcong` VALUES (15, 336, 'd13', '2023-10-13 14:07:27', 'Ra ca');
INSERT INTO `ns_logs_chamcong` VALUES (16, 336, 'd13', '2023-10-13 14:11:54', 'Vào ca');
INSERT INTO `ns_logs_chamcong` VALUES (17, 336, 'd13', '2023-10-13 14:11:57', 'Ra ca');
INSERT INTO `ns_logs_chamcong` VALUES (18, 336, 'd13', '2023-10-13 14:12:16', 'Vào ca');
INSERT INTO `ns_logs_chamcong` VALUES (19, 336, 'd13', '2023-10-13 14:12:19', 'Ra ca');
INSERT INTO `ns_logs_chamcong` VALUES (20, 337, 'd13', '2023-10-13 14:13:05', 'Vào ca');
INSERT INTO `ns_logs_chamcong` VALUES (21, 337, 'd13', '2023-10-13 14:13:10', 'Ra ca');
INSERT INTO `ns_logs_chamcong` VALUES (22, 337, 'd13', '2023-10-13 14:13:44', 'Vào ca');
INSERT INTO `ns_logs_chamcong` VALUES (23, 337, 'd13', '2023-10-13 14:13:46', 'Ra ca');
INSERT INTO `ns_logs_chamcong` VALUES (24, 338, 'd14', '2023-10-14 12:14:32', 'Vào ca');
INSERT INTO `ns_logs_chamcong` VALUES (25, 338, 'd14', '2023-10-14 12:14:35', 'Ra ca');
INSERT INTO `ns_logs_chamcong` VALUES (26, 339, 'd15', '2023-10-15 15:35:43', 'Vào ca');

-- ----------------------------
-- Table structure for ns_luongcodinh_thongso
-- ----------------------------
DROP TABLE IF EXISTS `ns_luongcodinh_thongso`;
CREATE TABLE `ns_luongcodinh_thongso`  (
  `thongso_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `fMonth` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `fYear` varchar(4) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `danhmuc_id` int(11) NULL DEFAULT 0,
  `thongso_luongcodinh` int(11) NULL DEFAULT 0,
  `thongso_luong1ngaycong` int(11) NULL DEFAULT 0,
  `thongso_luong1giolamthem` int(11) NULL DEFAULT 0,
  `thongso_phucaptrachnhiem` int(11) NULL DEFAULT 0,
  `thongso_phucaptienxang` int(11) NULL DEFAULT 0,
  `thongso_phucaptiencomtheongay` int(11) NULL DEFAULT 0,
  `thongso_phucaptiencomtheongay_tien` double(11, 2) NULL DEFAULT 0.00,
  `thongso_phucapdienthoai` int(11) NULL DEFAULT 0,
  `thongso_phucapcongtacphi` int(11) NULL DEFAULT NULL,
  `thongso_phucapchuyencan` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `thongso_phucapchuyencan_songay` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `thongso_phucapchuyencan_sogio` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `thongso_phucapchuyencan_mucthuong` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `thongso_baohiemxahoi` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `thongso_thuong` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `thongso_phat` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `thongso_kyquytamung` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `thongso_trunophoi` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `thongso_mcc` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`thongso_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 18 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_luongcodinh_thongso
-- ----------------------------
INSERT INTO `ns_luongcodinh_thongso` VALUES (14, '9', '2020', 28, 0, 0, 0, 0, 0, 0, 0.00, 0, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, 1);
INSERT INTO `ns_luongcodinh_thongso` VALUES (13, '8', '2020', 28, 1, 0, 0, 0, 0, 0, 0.00, 0, 0, '0', NULL, NULL, NULL, '0', '0', '0', '0', NULL, 1);
INSERT INTO `ns_luongcodinh_thongso` VALUES (12, '8', '2020', 1, 1, 0, 0, 0, 0, 0, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `ns_luongcodinh_thongso` VALUES (11, '7', '2020', 2, 0, 0, 0, 0, 0, 0, 0.00, 0, 0, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_luongcodinh_thongso` VALUES (10, '7', '2020', 1, 0, 0, 0, 0, 0, 0, 0.00, 0, 0, '0', NULL, NULL, NULL, '0', '0', '0', '0', NULL, NULL);
INSERT INTO `ns_luongcodinh_thongso` VALUES (16, '10', '2020', 29, 0, 0, 0, 0, 0, 0, 0.00, 0, NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_luongcodinh_thongso` VALUES (15, '9', '2020', 30, 0, 0, 0, 0, 0, 0, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `ns_luongcodinh_thongso` VALUES (17, '7', '2023', 28, 0, 1000000, 0, 1, 1, 1, 0.00, 1, 1, '1', NULL, NULL, NULL, '1', '1', '1', '1', NULL, 0);

-- ----------------------------
-- Table structure for ns_maychamcong
-- ----------------------------
DROP TABLE IF EXISTS `ns_maychamcong`;
CREATE TABLE `ns_maychamcong`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NULL DEFAULT NULL,
  `time_cc` datetime NULL DEFAULT NULL,
  `status` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_maychamcong
-- ----------------------------
INSERT INTO `ns_maychamcong` VALUES (1, 12, '2023-08-01 06:00:00', 'I');
INSERT INTO `ns_maychamcong` VALUES (2, 12, '2023-08-01 10:00:00', 'BO');
INSERT INTO `ns_maychamcong` VALUES (3, 12, '2023-08-02 01:01:00', 'delete_I');
INSERT INTO `ns_maychamcong` VALUES (4, 12, '2023-08-02 01:01:00', 'delete_BO');
INSERT INTO `ns_maychamcong` VALUES (5, 12, '2023-08-02 01:00:00', 'delete_I');
INSERT INTO `ns_maychamcong` VALUES (6, 12, '2023-08-02 01:00:00', 'delete_BO');
INSERT INTO `ns_maychamcong` VALUES (7, 13, '2023-08-01 07:00:00', 'I');
INSERT INTO `ns_maychamcong` VALUES (8, 13, '2023-08-01 07:00:00', 'BO');
INSERT INTO `ns_maychamcong` VALUES (9, 14, '2023-08-01 06:00:00', 'I');
INSERT INTO `ns_maychamcong` VALUES (10, 14, '2023-08-01 10:00:00', 'BO');

-- ----------------------------
-- Table structure for ns_nhanvien
-- ----------------------------
DROP TABLE IF EXISTS `ns_nhanvien`;
CREATE TABLE `ns_nhanvien`  (
  `nv_id` int(11) NOT NULL AUTO_INCREMENT,
  `nv_name` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `danhmuc_id` int(11) NULL DEFAULT NULL,
  `nv_order` int(11) NULL DEFAULT NULL,
  `nv_status` int(11) NULL DEFAULT 1,
  `nv_cmt` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_chucvu` int(11) NULL DEFAULT NULL,
  `nv_ngaysinh` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_sex` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_ngayvaolam` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_luong_codinh` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_phucap_trachnhiem` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_sdt_lienhe` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_sdt_khancap` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_nguyenvong` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_motacongviec` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `nv_lylich` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `nv_tinhtranghonnhan` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_noithuongtru` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `nv_noitamtru` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_cha` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_me` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_anhem` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `nv_vochong` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `nv_concai` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `nv_loai_hopdong` int(11) NULL DEFAULT 1,
  `nv_thoihan_hopdong` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_cmnd_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_cmnd_ngaycap` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_cmnd_noicap` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_noisinh` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_diachithuongtru` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_khenthuong` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_kyluat` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_diachitamtru` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_cmnd_truoc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_cmnd_sau` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_username` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_password` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_chinhanh` int(11) NULL DEFAULT NULL,
  `nv_calamviec` int(11) NULL DEFAULT NULL,
  `nv_email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_chucvunew` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_gender` int(1) NULL DEFAULT 0 COMMENT '0: nữ 1:nam',
  `nv_hocvan` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_dantoc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_tongiao` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_hopdong` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_cmnd_hethan` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_gplx_truoc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_gplx_sau` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_gplx_hethan` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_thuong_new` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `nv_kyluat_new` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`nv_id`) USING BTREE,
  INDEX `ns_nhanvien_ibfk_1`(`nv_chinhanh`) USING BTREE,
  INDEX `danhmuc_id`(`danhmuc_id`) USING BTREE,
  INDEX `ns_nhanvien_ibfk_3`(`nv_calamviec`) USING BTREE,
  INDEX `ns_nhanvien_ibfk_4`(`nv_chucvu`) USING BTREE,
  CONSTRAINT `danhmuc_id` FOREIGN KEY (`danhmuc_id`) REFERENCES `ns_danhmuc_phongban` (`danhmuc_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ns_nhanvien_ibfk_1` FOREIGN KEY (`nv_chinhanh`) REFERENCES `dvcc_chi_nhanh` (`chi_nhanh_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ns_nhanvien_ibfk_5` FOREIGN KEY (`nv_chucvu`) REFERENCES `dvcc_chuc_vu` (`chuc_vu_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE = InnoDB AUTO_INCREMENT = 66 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_nhanvien
-- ----------------------------
INSERT INTO `ns_nhanvien` VALUES (1, 'Nguyễn Văn Thường', 1, 1000, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (2, 'Nhân viên mới', 1, 1000, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (3, NULL, 1, 1000, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (4, NULL, 1, 1000, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (5, NULL, 1, 1000, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (6, NULL, 1, 1000, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (7, NULL, 1, 1000, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (8, NULL, 1, 1000, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (9, NULL, 1, 1000, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (10, NULL, 1, 1000, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (11, NULL, 1, 1000, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (12, 'Nguyễn Văn Thường', 10, 1000, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (13, 'Võ Anh Triết', 10, 1000, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dasda00.triet', '123456', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (14, 'Hoàng Hoài', 10, 1000, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (15, 'Nguyễn Huỳnh Thùy Trang', 10, 1000, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (16, 'Nguyễn Thị Trang', 11, 1000, 1, NULL, 1, NULL, NULL, '01/12/2022', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'navitek.nttrang10', '1234trang!', 2, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (17, 'Hồ Hữu Hoành', 12, 1000, 1, NULL, 1, NULL, NULL, '01/03/2021', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'hhhoanh1', '1234', 2, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (18, 'Nguyễn Thị Thúy Vi', 12, 1000, 1, NULL, 1, NULL, NULL, '13/02/2023', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'nttvi12', '1234', 1, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (19, 'Nguyễn Tuấn Huy', 13, 1000, 1, NULL, 1, NULL, NULL, '01/03/2021', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'nthuy4', '1234', 1, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (20, 'Lê Thị Huệ', 13, 1000, 1, NULL, 1, NULL, NULL, '01/03/2021', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'lthue2', '1234', 1, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (21, 'Trần Quốc Đạt', 13, 1000, 1, NULL, 1, NULL, NULL, '04/05/2021', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'tqdat5', '1234', 1, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (22, 'Hồ Thanh Xuân', 13, 1000, 1, NULL, 1, NULL, NULL, '08/05/2021', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'htxuan3', '1234', 1, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (23, 'Lê Diệp Quốc Anh', 13, 1000, 1, NULL, 1, NULL, NULL, '10/03/2022', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ldqanh6', '1234', 1, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (24, 'Trịnh Thị Hoàng Oanh', 13, 1000, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'tthoanh11', '1234', 1, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (25, 'Nguyễn Khánh', 13, 1000, 2, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'nkhanh9', '1234', 1, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (26, 'Khưu Bảo Như', 13, 1000, 1, NULL, 1, NULL, NULL, '05/08/2023', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'kbnhu13', '1234', 1, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (27, 'Đặng Thành Nam', 13, 1000, 1, NULL, 1, NULL, '', '03/01/2022', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dtnam7', '1234', 1, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (28, 'Trần Đăng Khôi', 13, 1000, 1, NULL, 1, NULL, NULL, '04/05/2022', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'tdkhoi8', '1234', 1, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (29, 'Nguyễn Lâm Hoàng Anh', 13, 1000, 1, NULL, 1, NULL, NULL, '19/08/2023', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'nlhanh14', '1234', 1, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (30, 'Trần Văn Nguyên', 13, 1000, 1, NULL, 1, NULL, NULL, '19/08/2023', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'tvnguyen15', '1234', 1, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (31, 'Nguyễn Trần Ngọc Tấn', 13, 1000, 1, NULL, 1, NULL, NULL, '19/08/2023', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ntntan16', '1234', 1, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (32, 'Nguyễn Khánh', 13, 1000, 1, NULL, 1, NULL, NULL, '07/04/2023', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'nkhanh11', '1234', 1, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (33, 'IT_TEST', 13, 1000, 1, NULL, 1, '2000-02-20', NULL, '2023-10-10', NULL, NULL, '0337346999', '0337346999', NULL, 'nhân viên IT', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 'Địa chỉ thường trú', NULL, NULL, '', NULL, NULL, './public/upload/1696175134004-120260906-avatar-logo192.png', 'root', '12345678', 1, 0, 'root@gmail.com', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (40, 'Triều Vĩ', 67, NULL, 1, NULL, 1, '2000-01-01', NULL, '2023-10-11', NULL, NULL, '0993339999', '0993339999', 'lương cao', 'nhân viên IT, lập trình web', 'Cha:\nMẹ:\nAnh:\nChị:\nEm:', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, '2023-10-20', '051095000000', '2023-10-10', 'CAND', '200 Đường Số 9', '200 Đường Số 9', NULL, NULL, '200 Đường Số 9', 'https://apihr.weos.vn/./public/cccd/dasda00_1697013365611.jpg', NULL, 'https://apihr.weos.vn/./public/avatar/dasda00_1697012461875.jpg', 'dasda00.trieuvi', '12345', NULL, NULL, NULL, 'Quản Lý', NULL, '12/12', 'Kinh', 'Không', 'https://apihr.weos.vn/./public/hopDong/dasda00_1697012777175.pdf', '2023-10-12', 'https://apihr.weos.vn/./public/gplx/dasda00_1697016104856.png', 'https://apihr.weos.vn/./public/gplx/dasda00_1697016554728.jpg', '2023-10-14', NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (41, 'John Cena', 68, NULL, 1, NULL, NULL, '2000-01-02', NULL, '2023-10-12', NULL, NULL, '0123456789', '0123456789', NULL, 'nhân viên IT', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '051095000011', '2023-10-07', 'Quảng Ngãi', '200 Đường Số 9', '200 Đường Số 9', NULL, NULL, '200 Đường Số 9', NULL, NULL, 'https://apihr.weos.vn/./public/avatar/dasda00_1697002073461.png', NULL, NULL, NULL, NULL, NULL, 'Nhân Viên', 1, 'Đại Học', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (42, 'Hoàng A Mã', 69, NULL, 1, NULL, NULL, '2000-01-03', NULL, '2023-10-13', NULL, NULL, '0997779999', '0997779999', NULL, 'nhân viên IT', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '051095000022', '2023-05-05', 'CAND', '200 Đường Số 9', '200 Đường Số 9', NULL, NULL, '200 Đường Số 9', NULL, NULL, 'https://apihr.weos.vn/./public/avatar/dasda00_1697002164539.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Cao Đẳng', 'Kinh', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (43, 'Ngũ A Ca', 70, NULL, 1, NULL, NULL, '2000-01-04', NULL, '2023-10-14', NULL, NULL, '0398756421', '0398756421', NULL, 'nhân viên bán vé', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '051095000033', '2023-05-05', 'Quảng Ngãi', '200 Đường Số 9', '200 Đường Số 9', NULL, NULL, '200 Đường Số 9', NULL, NULL, 'https://apihr.weos.vn/./public/avatar/dasda00_1697006036115.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Thạc Sỹ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (44, 'Nhĩ Khang', 67, NULL, 1, NULL, NULL, '2000-01-05', NULL, '2023-10-15', NULL, NULL, '0331523647', '0331523647', NULL, 'nhân viên bán vé', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '051095000044', '2023-10-07', 'CAND', '200 Đường Số 9', '200 Đường Số 9', NULL, NULL, '200 Đường Số 9', 'https://apihr.weos.vn/./public/cccd/dasda00_1697013867294.jpg', 'https://apihr.weos.vn/./public/cccd/dasda00_1697013956123.jpg', 'https://apihr.weos.vn/./public/avatar/dasda00_1697084860186.png', 'dasda00.nhikhang', '1234', NULL, NULL, NULL, NULL, 0, '12/12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (45, 'Kim Toả', 67, NULL, 1, NULL, NULL, '2000-01-06', NULL, '2023-10-16', NULL, NULL, '0123787459', '0123787459', NULL, 'nhân viên bán vé', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '051095000055', '2023-01-01', 'Quảng Ngãi', '200 Đường Số 9', '200 Đường Số 9', NULL, NULL, '200 Đường Số 9', NULL, NULL, 'https://apihr.weos.vn/./public/avatar/dasda00_1697006458913.jpg', 'dasda00.kimtoa', '1234', NULL, NULL, NULL, NULL, NULL, '12/12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (46, 'Huấn Rose', 67, NULL, 1, NULL, NULL, '2000-01-07', NULL, '2023-10-17', NULL, NULL, NULL, NULL, NULL, 'nhân viên bán vé', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '200 Đường Số 9', '200 Đường Số 9', NULL, NULL, '200 Đường Số 9', NULL, NULL, 'https://apihr.weos.vn/public/avatar/avatar.png', 'dasda00.huan', '1234', NULL, NULL, NULL, NULL, NULL, '12/12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (47, 'Myke Tyson', 67, NULL, 0, NULL, NULL, '2000-01-08', NULL, '2023-10-18', NULL, NULL, '', NULL, NULL, 'nhân viên bán vé', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, '200 Đường Số 9', '200 Đường Số 9', NULL, NULL, '200 Đường Số 9', NULL, NULL, 'https://apihr.weos.vn/public/avatar/avatar.png', NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Đại Học', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (48, 'Hạ Tử Vy', 67, NULL, 1, NULL, NULL, '2000-01-09', NULL, '2023-10-19', NULL, NULL, NULL, NULL, NULL, 'nhân viên bán vé', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'https://apihr.weos.vn/public/avatar/avatar.png', 'dasda00.tuvy', '1334', NULL, NULL, NULL, NULL, 0, 'Đại Học', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (49, 'Nhân Viên', 79, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'nhân viên bán vé', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'https://apihr.weos.vn/public/avatar/avatar.png', NULL, NULL, NULL, NULL, NULL, NULL, 0, 'Đại Học', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (50, 'Bảo Bảo', 81, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'https://apihr.weos.vn/public/avatar/avatar.png', NULL, NULL, NULL, NULL, NULL, NULL, 1, 'Đại Học', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (51, 'Cảnh', 67, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'https://apihr.weos.vn/public/avatar/avatar.png', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (59, 'Nhân Viên', 83, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'https://apihr.weos.vn/public/avatar/avatar.png', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (60, 'Hoàng A Mã', 63, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'https://apihr.weos.vn/./public/avatar/an53_1697199722737.png', 'an53.hoangama', '1234', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Thưởng Chuyên Cần', 'Phạt đi trễ');
INSERT INTO `ns_nhanvien` VALUES (61, 'Phan Thu Hoài', 63, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'https://apihr.weos.vn/public/avatar/avatar.png', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (62, 'Phan  Thu Hoài', 63, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'https://apihr.weos.vn/public/avatar/avatar.png', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (63, 'Trang  Thành  Được', 66, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'https://apihr.weos.vn/public/avatar/avatar.png', 'an53.thanhduoc', '12345', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (64, 'Trang  Thành  Đượcc', 64, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'https://apihr.weos.vn/public/avatar/avatar.png', 'an53.duoc', '112233', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ns_nhanvien` VALUES (65, 'Nguyễn Tiến Minh', 100, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'https://apihr.weos.vn/public/avatar/avatar.png', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for ns_nhanvien_bangcap
-- ----------------------------
DROP TABLE IF EXISTS `ns_nhanvien_bangcap`;
CREATE TABLE `ns_nhanvien_bangcap`  (
  `bangcap_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NULL DEFAULT 1,
  `bangcap_name` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `bangcap_namtotnghiep` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `bangcap_ngayhethan` date NULL DEFAULT NULL,
  `bangcap_scan_file` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `bangcap_order` int(11) NULL DEFAULT NULL,
  `bangcap_noicap` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `loai_bang_cap` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `bangcap_hethan` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`bangcap_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 16 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_nhanvien_bangcap
-- ----------------------------
INSERT INTO `ns_nhanvien_bangcap` VALUES (1, 135, NULL, NULL, NULL, 'vanbang_1.pdf', 1000, NULL, '', NULL);
INSERT INTO `ns_nhanvien_bangcap` VALUES (2, 142, NULL, NULL, '0000-00-00', 'cmnd_142_sau.png', 1000, NULL, '', NULL);
INSERT INTO `ns_nhanvien_bangcap` VALUES (8, 1, 'giấy khám sức khỏe', NULL, NULL, 'vanbang_8.docx', 1000, NULL, '', NULL);
INSERT INTO `ns_nhanvien_bangcap` VALUES (7, 1, NULL, NULL, NULL, NULL, 1000, NULL, '', NULL);
INSERT INTO `ns_nhanvien_bangcap` VALUES (6, 142, NULL, NULL, NULL, NULL, 1000, NULL, 'LAIXE', NULL);
INSERT INTO `ns_nhanvien_bangcap` VALUES (9, 1, NULL, NULL, NULL, NULL, 1000, NULL, 'LAIXE', NULL);
INSERT INTO `ns_nhanvien_bangcap` VALUES (10, 27, NULL, NULL, NULL, NULL, 1000, NULL, '', NULL);
INSERT INTO `ns_nhanvien_bangcap` VALUES (11, 40, 'Bằng Đại Học', '2013-09-01', NULL, 'https://apihr.weos.vn/./public/chungChi/dasda00_1697083418479.jpg', NULL, NULL, '', '2024-12-15');
INSERT INTO `ns_nhanvien_bangcap` VALUES (12, 40, 'Chứng Chỉ Lập Trình', '2023-10-13', NULL, 'https://apihr.weos.vn/./public/chungChi/dasda00_1697084373973.jpg', NULL, NULL, '', '2024-12-12');
INSERT INTO `ns_nhanvien_bangcap` VALUES (13, 40, 'Chứng Chỉ Tiếng Anh', '2023-10-12', NULL, NULL, NULL, NULL, '', NULL);
INSERT INTO `ns_nhanvien_bangcap` VALUES (14, 44, 'Bằng Tốt Nghiệp', NULL, NULL, NULL, NULL, NULL, '', NULL);
INSERT INTO `ns_nhanvien_bangcap` VALUES (15, 44, 'Chứng Chỉ Kinh Doanh', NULL, NULL, NULL, NULL, NULL, '', NULL);

-- ----------------------------
-- Table structure for ns_nhanvien_bangluongchinh
-- ----------------------------
DROP TABLE IF EXISTS `ns_nhanvien_bangluongchinh`;
CREATE TABLE `ns_nhanvien_bangluongchinh`  (
  `luong_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nv_name` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nv_id` int(11) NULL DEFAULT NULL,
  `fMonth` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `fYear` varchar(4) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `danhmuc_id` int(11) NULL DEFAULT 0,
  `luong_co_dinh` int(11) NULL DEFAULT 0,
  `ngay_vao_lam` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phu_cap_trach_nhiem` int(11) NULL DEFAULT 0,
  `phu_cap_tien_xang` double(11, 2) NULL DEFAULT 0.00,
  `phu_cap_tien_com` double(11, 2) NULL DEFAULT 0.00,
  `phu_cap_dien_thoai` double(11, 2) NULL DEFAULT 0.00,
  `phu_cap_cong_tac_phi` double(11, 2) NULL DEFAULT 0.00,
  `phu_cap_chuyen_can` double(11, 2) NULL DEFAULT 0.00,
  `thuong_chuyen_can` int(11) NULL DEFAULT 0,
  `d01` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d02` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d03` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d04` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d05` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d06` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d07` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d08` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d09` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d10` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d11` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d12` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d13` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d14` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d15` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d16` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d17` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d18` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d19` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d20` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d21` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d22` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d23` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d24` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d25` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d26` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d27` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d28` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d29` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d30` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `d31` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tru_luong_do_tam_ung` int(11) NULL DEFAULT 0,
  `tru_luong_do_ky_quy` int(11) NULL DEFAULT 0,
  `note_d01` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d02` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d03` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d04` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d05` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d06` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d07` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d08` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d09` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d10` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d11` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d12` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d13` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d14` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d15` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d16` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d17` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d18` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d19` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d20` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d21` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d22` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d23` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d24` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d25` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d26` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d27` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d28` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d29` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d30` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note_d31` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `comment_text` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `ngay_chuan` double NULL DEFAULT NULL,
  `thongso_default` int(11) NULL DEFAULT NULL,
  `luong_tang` int(11) NULL DEFAULT NULL,
  `luong_giam` int(11) NULL DEFAULT NULL,
  `ghichu_tanggiam` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `gio_tang_ca` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `mcc_hc_ditre` int(11) NULL DEFAULT NULL,
  `mcc_tc_ditre` int(11) NULL DEFAULT NULL,
  `mcc_hc_giolam` int(11) NULL DEFAULT NULL,
  `mcc_tc_giolam` int(11) NULL DEFAULT NULL,
  `mcc_ca1_ditre` int(11) NULL DEFAULT NULL,
  `mcc_ca2_ditre` int(11) NULL DEFAULT NULL,
  `mcc_ca3_ditre` int(11) NULL DEFAULT NULL,
  `mcc_ca1_giolam` int(11) NULL DEFAULT NULL,
  `mcc_ca2_giolam` int(11) NULL DEFAULT NULL,
  `mcc_ca3_giolam` int(11) NULL DEFAULT NULL,
  `tong_cong` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0',
  `tong_P` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0',
  `tong_L` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0',
  `tong_khongphep` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0',
  `tong_ngaylamviec` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0',
  `phep_congthem` int(1) NULL DEFAULT 0,
  PRIMARY KEY (`luong_id`) USING BTREE,
  INDEX `nv_id`(`nv_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 340 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_nhanvien_bangluongchinh
-- ----------------------------
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (1, '', 1, '07', '2023', 3, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '1', NULL, 'P', '1', '2', 'P', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', NULL, '1', '1', '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'dsfsdf\n', NULL, 'Làm tăng ca tới 8h tối\n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (2, '', 2, '07', '2023', 3, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '1.5', NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 'tăng ca 9h', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (4, '', 4, '07', '2023', 3, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (5, '', 5, '07', '2023', 3, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (6, '', 6, '07', '2023', 3, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (7, '', 7, '07', '2023', 3, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (9, '', 1, '07', '2023', 1, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (10, '', 2, '07', '2023', 1, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (11, '', 3, '07', '2023', 1, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (12, '', 4, '07', '2023', 1, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (13, '', 5, '07', '2023', 1, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (14, '', 6, '07', '2023', 1, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (15, '', 7, '07', '2023', 1, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (16, '', 8, '07', '2023', 1, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (17, '', 9, '07', '2023', 1, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (18, '', 10, '07', '2023', 1, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (19, '', 11, '07', '2023', 1, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (20, '', 12, '07', '2023', 10, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (21, '', 13, '07', '2023', 10, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (22, '', 14, '07', '2023', 10, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (23, '', 15, '07', '2023', 10, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (24, '', 16, '07', '2023', 11, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, '', '1', '1', '1', '1', '0.5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (25, '', 17, '07', '2023', 12, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (26, '', 18, '07', '2023', 12, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (27, '', 19, '07', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (28, '', 20, '07', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (29, '', 21, '07', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (30, '', 22, '07', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (31, '', 23, '07', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (32, '', 24, '07', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (33, '', 25, '07', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (34, '', 26, '07', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (35, '', 27, '07', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (36, '', 28, '07', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (37, '', 29, '07', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (38, '', 30, '07', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (39, '', 31, '07', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (97, '', 12, '09', '2023', 10, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, '', NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '', NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (98, '', 13, '09', '2023', 10, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 23.5, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (99, '', 14, '09', '2023', 10, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 23.5, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (100, '', 15, '09', '2023', 10, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 23.5, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (101, '', 16, '09', '2023', 11, 0, '01/12/2022', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 'P', 'L', NULL, 'L', '1', '1', '1', '1', '1', NULL, '1', '1.75', '1.375', '1', '1', '1', NULL, '1', '1', '1', '1', '1', '', NULL, '1', '1', '1', '1', '1', '1', NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (102, '', 17, '09', '2023', 12, 0, '01/03/2021', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '', '', NULL, '', '1', '1', '1', '1', '', NULL, '1', '1', '1', '1', '1', '', '', '1', '1', '1', '1', '1', NULL, NULL, '1', '1', '', '', NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 23.5, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (103, '', 18, '09', '2023', 12, 0, '13/02/2023', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 23.5, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (104, '', 19, '09', '2023', 13, 0, '01/03/2021', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (105, '', 20, '09', '2023', 13, 0, '01/03/2021', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 23.5, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (106, '', 21, '09', '2023', 13, 0, '04/05/2021', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 'P', 'L', '', 'L', '1', '1', '1', '1', NULL, NULL, '0.5_0.5P', '1', '1', '1', '1', '1', NULL, '0.5_0.5P', '1', '1', '1', '1', NULL, NULL, 'P', '1', '1', '', '', '', NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (107, '', 22, '09', '2023', 13, 0, '08/05/2021', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 23.5, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (108, '', 23, '09', '2023', 13, 0, '10/03/2022', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '1', 'L', '', 'L', '1', '1', '1', '1', NULL, NULL, '1', '1', '1', '1', '1', '1', NULL, '1', '1', '1', '1', '1', '2', NULL, '1', '1', '1', NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 23.5, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (109, '', 24, '09', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 23.5, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (111, '', 26, '09', '2023', 13, 0, '05/08/2023', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '1', NULL, NULL, '', '1', '1', '1', '1', NULL, NULL, '1', '1', '1', '1', '1', '1', NULL, '1', '1', '1', '1', '1', NULL, NULL, '1', '1', '1', NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 23.5, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (112, '', 27, '09', '2023', 13, 0, '03/01/2022', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 23.5, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (113, '', 28, '09', '2023', 13, 0, '04/05/2022', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 23.5, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (114, '', 29, '09', '2023', 13, 0, '19/08/2023', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '0.5_0.5P', 'L', NULL, 'L', '1', '1', '1', '1', NULL, NULL, '1', '1', '1', '1', '1', '0.5_0.5P', NULL, '1', '1', '1', '1', '1', '2', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 23.5, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (115, '', 30, '09', '2023', 13, 0, '19/08/2023', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 23.5, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (116, '', 31, '09', '2023', 13, 0, '19/08/2023', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 23.5, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (117, '', 32, '09', '2023', 13, 0, '07/04/2023', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 23.5, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (122, '', 1, '08', '2023', 3, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (123, '', 2, '08', '2023', 3, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (124, '', 2, '08', '2023', 1, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (125, '', 4, '08', '2023', 3, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (126, '', 5, '08', '2023', 3, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (127, '', 6, '08', '2023', 3, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (128, '', 7, '08', '2023', 3, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (129, '', 1, '08', '2023', 1, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (130, '', 3, '08', '2023', 1, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (131, '', 4, '08', '2023', 1, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (132, '', 5, '08', '2023', 1, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (133, '', 6, '08', '2023', 1, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (134, '', 7, '08', '2023', 1, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (135, '', 8, '08', '2023', 1, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (136, '', 9, '08', '2023', 1, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (137, '', 10, '08', '2023', 1, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (138, '', 11, '08', '2023', 1, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (140, '', 13, '08', '2023', 10, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'L', '1', 'O', 'O', '1', 'P', '1', '1', '1', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 360, 0, 0, -2, 0, 0, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (141, '', 14, '08', '2023', 10, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '1', '1', '1', '1', '1', '1', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 120, 0, 0, 2, 0, 0, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (142, '', 15, '08', '2023', 10, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '2', 'O', '1', '1', '1', '1', '2', 'P', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (143, '', 16, '08', '2023', 11, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '1', '1', '1', '1', '1', NULL, '1', '1', '1', '1', '1', NULL, NULL, '1', '1', '1', '1', '1', '1', NULL, '1', '1', '1', '1', '1', NULL, NULL, '1', '1', '1', '1', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, '0', '0', '0', '0', '0', 1);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (144, '', 17, '08', '2023', 12, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '1', '1', '1', '1', 'P', NULL, '1', '1', '1', '1', '1', NULL, NULL, '1', '1', '1', '1', 'P', '1', NULL, '1', '1', '1', '1', '1', NULL, NULL, '1', '1', '1', '1', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (145, '', 18, '08', '2023', 12, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '1', '1', '1', '1', '1', NULL, '1', '1', '1', '1', '1', NULL, NULL, '1', '1', '1', '1', '1', '1', NULL, '1', '1', '1', '1', '1', NULL, NULL, '1', '1', '1', '1', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (146, '', 19, '08', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '1', '1', '1', '1', '1', NULL, '1', 'P', 'P', '1', '1', NULL, NULL, '1', '1', '1', '1', 'P', '1', NULL, '1', '1', '1', '1', '1', NULL, NULL, '1', '1', '1', '1', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (147, '', 20, '08', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '1', '1', '1', '1', '0.5', NULL, 'P', '1', '1', '1', '1', '2', NULL, '1', '1', '1', '1', '1', '1', NULL, '1', '1', '1', '1', '1', NULL, NULL, '1', '1', '1', '1', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (148, '', 21, '08', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '1', '1', '1', '1', 'P', NULL, '1', '1', '0.5', '1', 'P', NULL, NULL, '1', '1', '1', '1', '1', '0.5', NULL, '1', '1', '1', '1', '1', NULL, NULL, '1', '1', '1', '1', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (149, '', 22, '08', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '1', '1', '1', '1', '1', NULL, '1', '1', '1', '1', '1', NULL, NULL, '1', '1', '1', '1', '1', '1', NULL, '1', '1', '1', '1', '1', NULL, NULL, 'P', '1', '1', '1', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (150, '', 23, '08', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '1', '1', '1', '1', '1', NULL, '1', '1', '1', '1', '1', NULL, NULL, '1', '1', '1', '1', 'P', '1', NULL, '1', '1', '1', '1', '1', NULL, NULL, '1', '1', '1', '1', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (151, '', 24, '08', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '1', '1', '1', '1', '1', NULL, '1', '1', '1', '1', '1', NULL, NULL, '1', '1', '1', '1', '1', '1', NULL, '1', '1', '1', '1', '1', NULL, NULL, '1', '1', '1', 'P', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (152, '', 25, '08', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '1', '1', '1', '1', '1', NULL, '1', '1', '1', '1', '1', NULL, NULL, '1', '1', '1', '1', '1', '1', NULL, '1', '1', '1', '1', '1', NULL, NULL, 'P', '1', '1', '1', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (153, '', 26, '08', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '1', '1', '1', '1', '1', NULL, '1', '1', '1', '1', '1', NULL, NULL, '1', '1', '1', '1', '0.5', '1', NULL, '1', '1', '1', '1', '1', NULL, NULL, '1', '1', '1', '1', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (154, '', 27, '08', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '1', '1', '1', '1', '1', NULL, '1', '1', '1', '1', '1', NULL, NULL, '1', '1', '1', '1', '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (155, '', 28, '08', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 'P', '1', '1', '1', '1', NULL, '1', '1', '1', '1', '1', NULL, NULL, '1', '1', '1', '1', '1', '1', NULL, '1', '1', '1', '1', '1', NULL, NULL, '1', '1', '1', '1', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (156, '', 29, '08', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '1', '1', '1', '1', '1', NULL, '1', '1', '1', '1', 'O', '', NULL, '1', '1', '1', '1', '1', '1', '2', '1', '1', '1', '1', '1', '2', NULL, '1', '1', '1', '1', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (157, '', 30, '08', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '1', '1', '1', '1', '1', NULL, '1', '1', '1', '1', '1', '2', '2', '1', '1', '1', '1', '1', '1', '2', '1', '1', '1', '1', '1', '2', NULL, '1', '1', '1', '1', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (158, '', 31, '08', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '1', '1', '1', '1', '1', NULL, '1', '1', '1', '1', '1', '2', '2', '1', '1', '1', '1', '1', '1', NULL, '1', '1', '1', '1', '1', NULL, NULL, '1', '1', '1', '1', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2\n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (279, '', 12, '10', '2023', 10, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (280, '', 13, '10', '2023', 10, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (281, '', 14, '10', '2023', 10, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (282, '', 15, '10', '2023', 10, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (283, '', 16, '10', '2023', 11, 0, '01/12/2022', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (284, '', 17, '10', '2023', 12, 0, '01/03/2021', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (285, '', 18, '10', '2023', 12, 0, '13/02/2023', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (286, '', 19, '10', '2023', 13, 0, '01/03/2021', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (287, '', 20, '10', '2023', 13, 0, '01/03/2021', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (288, '', 21, '10', '2023', 13, 0, '04/05/2021', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (289, '', 22, '10', '2023', 13, 0, '08/05/2021', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (290, '', 23, '10', '2023', 13, 0, '10/03/2022', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (291, '', 24, '10', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (293, '', 26, '10', '2023', 13, 0, '05/08/2023', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (294, '', 27, '10', '2023', 13, 0, '03/01/2022', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (295, '', 28, '10', '2023', 13, 0, '04/05/2022', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (296, '', 29, '10', '2023', 13, 0, '19/08/2023', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (297, '', 30, '10', '2023', 13, 0, '19/08/2023', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (298, '', 31, '10', '2023', 13, 0, '19/08/2023', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (299, '', 32, '10', '2023', 13, 0, '07/04/2023', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (300, '', 12, '11', '2023', 10, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (301, '', 13, '11', '2023', 10, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (302, '', 14, '11', '2023', 10, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (303, '', 15, '11', '2023', 10, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (304, '', 16, '11', '2023', 11, 0, '01/12/2022', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (305, '', 17, '11', '2023', 12, 0, '01/03/2021', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (306, '', 18, '11', '2023', 12, 0, '13/02/2023', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (307, '', 19, '11', '2023', 13, 0, '01/03/2021', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (308, '', 20, '11', '2023', 13, 0, '01/03/2021', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (309, '', 21, '11', '2023', 13, 0, '04/05/2021', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (310, '', 22, '11', '2023', 13, 0, '08/05/2021', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (311, '', 23, '11', '2023', 13, 0, '10/03/2022', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (312, '', 24, '11', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (313, '', 26, '11', '2023', 13, 0, '05/08/2023', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (314, '', 27, '11', '2023', 13, 0, '03/01/2022', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (315, '', 28, '11', '2023', 13, 0, '04/05/2022', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (316, '', 29, '11', '2023', 13, 0, '19/08/2023', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (317, '', 30, '11', '2023', 13, 0, '19/08/2023', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (318, '', 31, '11', '2023', 13, 0, '19/08/2023', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (319, '', 32, '11', '2023', 13, 0, '07/04/2023', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (323, '', -1, '', '', 10, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', NULL, '0', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (337, NULL, 33, '10', '2023', 13, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '0', '0', '1', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (338, NULL, 63, '10', '2023', 66, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '0', '0', '1', 0);
INSERT INTO `ns_nhanvien_bangluongchinh` VALUES (339, NULL, 64, '10', '2023', 64, 0, NULL, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '0', '0', '1', 0);

-- ----------------------------
-- Table structure for ns_tamung_chitiet
-- ----------------------------
DROP TABLE IF EXISTS `ns_tamung_chitiet`;
CREATE TABLE `ns_tamung_chitiet`  (
  `chitiet_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NULL DEFAULT NULL,
  `chitiet_ngaythang` datetime NULL DEFAULT NULL,
  `chitiet_loai` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `chitiet_sotien` int(11) NULL DEFAULT 0,
  `chitiet_ghichu` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `chitiet_status` int(11) NULL DEFAULT NULL,
  `chitiet_nguoithutien` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `chitiet_inputdate` datetime NULL DEFAULT NULL,
  `chitiet_mathanhtoan` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`chitiet_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_tamung_chitiet
-- ----------------------------
INSERT INTO `ns_tamung_chitiet` VALUES (7, 12, '2020-09-06 00:23:38', 'TAM_UNG', -2222, 'TẠM ỨNG', 1, 'BỘ PHẬN KẾ TOÁN', '2020-09-06 00:23:38', 'TU');
INSERT INTO `ns_tamung_chitiet` VALUES (6, -1, '2020-08-21 11:42:51', 'TAM_UNG', 0, 'TẠM ỨNG', 1, 'BỘ PHẬN KẾ TOÁN', '2020-08-21 11:42:51', 'TU');

-- ----------------------------
-- Table structure for ns_tamung_nv
-- ----------------------------
DROP TABLE IF EXISTS `ns_tamung_nv`;
CREATE TABLE `ns_tamung_nv`  (
  `tamung_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nv_id` int(11) NULL DEFAULT NULL,
  `nv_name` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `no_dau_ky` int(11) NULL DEFAULT 0,
  `no_tang_them` int(11) NULL DEFAULT 0,
  `no_cuoi_ky` int(11) NULL DEFAULT NULL,
  `tamung_thang` date NULL DEFAULT NULL,
  `tamung_status` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`tamung_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 329 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_tamung_nv
-- ----------------------------
INSERT INTO `ns_tamung_nv` VALUES (328, 12, 'Nguyễn Văn Thường[Ban Lãnh Đạo]', 0, 0, 0, '2020-09-01', 1);
INSERT INTO `ns_tamung_nv` VALUES (294, -1, '', 0, 0, 0, '2020-08-01', 1);

-- ----------------------------
-- Table structure for ns_taobangluong
-- ----------------------------
DROP TABLE IF EXISTS `ns_taobangluong`;
CREATE TABLE `ns_taobangluong`  (
  `tbl_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tbl_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tbl_month` int(11) NULL DEFAULT NULL,
  `tbl_year` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`tbl_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 43 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_taobangluong
-- ----------------------------
INSERT INTO `ns_taobangluong` VALUES (33, 'BẢNG LƯƠNG THÁNG MỚI', 8, 2020);
INSERT INTO `ns_taobangluong` VALUES (34, 'BẢNG LƯƠNG THÁNG MỚI', 9, 2020);
INSERT INTO `ns_taobangluong` VALUES (35, 'BẢNG LƯƠNG THÁNG MỚI', 7, 2020);
INSERT INTO `ns_taobangluong` VALUES (36, 'BẢNG LƯƠNG THÁNG MỚI', 11, 2020);
INSERT INTO `ns_taobangluong` VALUES (37, 'BẢNG LƯƠNG THÁNG MỚI', 10, 2020);
INSERT INTO `ns_taobangluong` VALUES (38, 'BẢNG LƯƠNG THÁNG MỚI', 7, 2023);
INSERT INTO `ns_taobangluong` VALUES (40, 'BẢNG LƯƠNG THÁNG MỚI', 10, 2023);
INSERT INTO `ns_taobangluong` VALUES (41, 'BẢNG LƯƠNG THÁNG MỚI', 8, 2023);
INSERT INTO `ns_taobangluong` VALUES (42, 'BẢNG LƯƠNG THÁNG MỚI', 9, 2023);

-- ----------------------------
-- Table structure for ns_taobangluong_cauhinh
-- ----------------------------
DROP TABLE IF EXISTS `ns_taobangluong_cauhinh`;
CREATE TABLE `ns_taobangluong_cauhinh`  (
  `cus_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cus_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cus_status` int(11) NULL DEFAULT NULL,
  `cus_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cus_note` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `cus_h` int(11) NULL DEFAULT NULL,
  `cus_i` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`cus_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_taobangluong_cauhinh
-- ----------------------------
INSERT INTO `ns_taobangluong_cauhinh` VALUES (1, 'BHXH', 1, NULL, NULL, NULL, NULL);
INSERT INTO `ns_taobangluong_cauhinh` VALUES (2, 'Ký Quỹ', 1, NULL, NULL, NULL, NULL);
INSERT INTO `ns_taobangluong_cauhinh` VALUES (3, 'Công Nợ', 1, NULL, NULL, NULL, NULL);
INSERT INTO `ns_taobangluong_cauhinh` VALUES (4, 'Kỹ Luật', 1, NULL, NULL, NULL, NULL);
INSERT INTO `ns_taobangluong_cauhinh` VALUES (5, 'Đào Tạo', 1, NULL, NULL, NULL, NULL);
INSERT INTO `ns_taobangluong_cauhinh` VALUES (6, 'Nhắc Nhở', 1, NULL, NULL, NULL, NULL);
INSERT INTO `ns_taobangluong_cauhinh` VALUES (7, 'Máy chấm công', 1, NULL, NULL, NULL, NULL);
INSERT INTO `ns_taobangluong_cauhinh` VALUES (8, 'Ca 1 Vào', NULL, NULL, NULL, 6, 0);
INSERT INTO `ns_taobangluong_cauhinh` VALUES (9, 'Ca 1 Ra', NULL, NULL, NULL, 12, 0);
INSERT INTO `ns_taobangluong_cauhinh` VALUES (10, 'Ca 2 Vào', NULL, NULL, NULL, 13, 0);
INSERT INTO `ns_taobangluong_cauhinh` VALUES (11, 'Ca 2 Ra', NULL, NULL, NULL, 17, 0);
INSERT INTO `ns_taobangluong_cauhinh` VALUES (12, 'Ca 3 Vào', NULL, NULL, NULL, 18, 0);
INSERT INTO `ns_taobangluong_cauhinh` VALUES (13, 'Ca 3 Ra', NULL, NULL, NULL, 21, 0);
INSERT INTO `ns_taobangluong_cauhinh` VALUES (14, 'hc_h_cong_chuan', NULL, NULL, NULL, 8, NULL);
INSERT INTO `ns_taobangluong_cauhinh` VALUES (15, 'tangca_h_cong_chuan', NULL, NULL, NULL, 3, NULL);
INSERT INTO `ns_taobangluong_cauhinh` VALUES (16, 'ca1_h_cong_chuan', NULL, NULL, NULL, 4, NULL);
INSERT INTO `ns_taobangluong_cauhinh` VALUES (17, 'ca2_h_cong_chuan', NULL, NULL, NULL, 4, NULL);
INSERT INTO `ns_taobangluong_cauhinh` VALUES (18, 'ca3_h_cong_chuan', NULL, NULL, NULL, 3, NULL);

-- ----------------------------
-- Table structure for ns_taobangluong_chitiet
-- ----------------------------
DROP TABLE IF EXISTS `ns_taobangluong_chitiet`;
CREATE TABLE `ns_taobangluong_chitiet`  (
  `bl_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tbl_id` int(11) NULL DEFAULT NULL,
  `bl_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `bl_status` int(11) NULL DEFAULT NULL,
  `bl_kyhieu` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `bl_type` int(11) NULL DEFAULT NULL,
  `bl_congthuc` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `bl_default` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `hople` int(11) NULL DEFAULT NULL,
  `sd_toan_cty` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`bl_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 49 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_taobangluong_chitiet
-- ----------------------------
INSERT INTO `ns_taobangluong_chitiet` VALUES (20, 33, 'Lương Cơ Bản', 0, NULL, 1, '@LCB@', NULL, 1, NULL);
INSERT INTO `ns_taobangluong_chitiet` VALUES (21, 0, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL);
INSERT INTO `ns_taobangluong_chitiet` VALUES (22, 33, 'Chuyên Cần', 0, NULL, 0, '@GC@ * 1000', NULL, 1, 0);
INSERT INTO `ns_taobangluong_chitiet` VALUES (23, 33, NULL, 0, NULL, 1, NULL, NULL, NULL, 0);
INSERT INTO `ns_taobangluong_chitiet` VALUES (24, 34, 'Giờ làm', 0, NULL, 0, '@TCGL@', NULL, 1, 1);
INSERT INTO `ns_taobangluong_chitiet` VALUES (25, 35, NULL, 0, NULL, 1, NULL, NULL, NULL, NULL);
INSERT INTO `ns_taobangluong_chitiet` VALUES (26, 33, NULL, 0, NULL, 1, NULL, NULL, NULL, NULL);
INSERT INTO `ns_taobangluong_chitiet` VALUES (27, 33, NULL, 0, NULL, 1, NULL, NULL, NULL, 0);
INSERT INTO `ns_taobangluong_chitiet` VALUES (28, 34, '', 0, NULL, 0, '', NULL, 1, NULL);
INSERT INTO `ns_taobangluong_chitiet` VALUES (29, 34, 'Ca', 0, NULL, 0, '', NULL, 1, NULL);
INSERT INTO `ns_taobangluong_chitiet` VALUES (30, 34, 'Ca - Đi trể', 0, NULL, 0, '', NULL, 1, NULL);
INSERT INTO `ns_taobangluong_chitiet` VALUES (31, 36, NULL, 0, NULL, 1, NULL, NULL, NULL, NULL);
INSERT INTO `ns_taobangluong_chitiet` VALUES (32, 36, NULL, 0, NULL, 1, NULL, NULL, NULL, NULL);
INSERT INTO `ns_taobangluong_chitiet` VALUES (33, 36, NULL, 0, NULL, 1, NULL, NULL, NULL, NULL);
INSERT INTO `ns_taobangluong_chitiet` VALUES (34, 37, 'Lương Bốc hàng', 1, NULL, 0, '@LCB@', NULL, 1, NULL);
INSERT INTO `ns_taobangluong_chitiet` VALUES (35, 38, 'lương ngày thường', 0, NULL, 1, '@LCB@ - @U', NULL, 0, 0);
INSERT INTO `ns_taobangluong_chitiet` VALUES (36, 38, NULL, 0, NULL, 1, NULL, NULL, NULL, NULL);
INSERT INTO `ns_taobangluong_chitiet` VALUES (37, 38, NULL, 0, NULL, 0, NULL, NULL, NULL, NULL);
INSERT INTO `ns_taobangluong_chitiet` VALUES (38, 38, NULL, 0, NULL, 1, NULL, NULL, NULL, NULL);
INSERT INTO `ns_taobangluong_chitiet` VALUES (39, 38, NULL, 0, NULL, 1, NULL, NULL, NULL, NULL);
INSERT INTO `ns_taobangluong_chitiet` VALUES (40, 38, NULL, 0, NULL, 1, NULL, NULL, NULL, NULL);
INSERT INTO `ns_taobangluong_chitiet` VALUES (41, 38, NULL, 0, NULL, 1, NULL, NULL, NULL, NULL);
INSERT INTO `ns_taobangluong_chitiet` VALUES (42, 38, NULL, 0, NULL, 1, NULL, NULL, NULL, NULL);
INSERT INTO `ns_taobangluong_chitiet` VALUES (43, 39, 'sdsadsad', 0, NULL, 0, NULL, NULL, NULL, 0);
INSERT INTO `ns_taobangluong_chitiet` VALUES (44, 39, NULL, 0, NULL, 0, NULL, NULL, NULL, 0);
INSERT INTO `ns_taobangluong_chitiet` VALUES (45, 40, NULL, 0, NULL, 0, '', NULL, 1, NULL);
INSERT INTO `ns_taobangluong_chitiet` VALUES (46, 41, 'ABCD', 1, NULL, 1, NULL, NULL, NULL, NULL);
INSERT INTO `ns_taobangluong_chitiet` VALUES (47, 40, NULL, 0, NULL, 1, NULL, NULL, NULL, NULL);
INSERT INTO `ns_taobangluong_chitiet` VALUES (48, 42, 'ABCD', 1, NULL, 1, '', NULL, 1, NULL);

-- ----------------------------
-- Table structure for ns_thetu
-- ----------------------------
DROP TABLE IF EXISTS `ns_thetu`;
CREATE TABLE `ns_thetu`  (
  `thetu_id` int(11) NOT NULL AUTO_INCREMENT,
  `thetu_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `nv_id` int(11) NULL DEFAULT NULL,
  `thetu_status` bit(1) NOT NULL,
  `danhmuc_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`thetu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ns_thetu
-- ----------------------------
INSERT INTO `ns_thetu` VALUES (1, '012345678', 16, b'1', 11);
INSERT INTO `ns_thetu` VALUES (2, '032423524', 17, b'0', 12);
INSERT INTO `ns_thetu` VALUES (3, '032442535', 18, b'0', 12);
INSERT INTO `ns_thetu` VALUES (8, '032423434', 19, b'0', 13);
INSERT INTO `ns_thetu` VALUES (10, '021435343', 17, b'1', 12);
INSERT INTO `ns_thetu` VALUES (11, '0564736457', 21, b'1', 13);
INSERT INTO `ns_thetu` VALUES (12, '0965467856', 20, b'0', 13);
INSERT INTO `ns_thetu` VALUES (13, '674434543', 12, b'0', 10);

-- ----------------------------
-- Table structure for qltl_items
-- ----------------------------
DROP TABLE IF EXISTS `qltl_items`;
CREATE TABLE `qltl_items`  (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `qltl_nhanvien` int(11) NULL DEFAULT NULL,
  `link_file` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `parent_id` int(11) NULL DEFAULT NULL,
  `item_isDirectory` int(11) NULL DEFAULT 1,
  `file_type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `item_status` int(11) NULL DEFAULT 1,
  `created_at` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `deleted_at` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`item_id`) USING BTREE,
  INDEX `parent_id`(`parent_id`) USING BTREE,
  INDEX `qltl_nhanvien`(`qltl_nhanvien`) USING BTREE,
  CONSTRAINT `qltl_items_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `qltl_items` (`item_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `qltl_items_ibfk_2` FOREIGN KEY (`qltl_nhanvien`) REFERENCES `qltl_nhanvien` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qltl_items
-- ----------------------------
INSERT INTO `qltl_items` VALUES (5, 'Thư mục 09/27', 2, NULL, NULL, 1, NULL, 1, NULL, NULL);
INSERT INTO `qltl_items` VALUES (7, 'weosID.sql', 2, './public/upload/1697470198812-860081641-file-weosID.sql', 5, 0, 'sql', 1, NULL, NULL);

-- ----------------------------
-- Table structure for qltl_nhanvien
-- ----------------------------
DROP TABLE IF EXISTS `qltl_nhanvien`;
CREATE TABLE `qltl_nhanvien`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `chuc_vu` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `chuc_vu`(`chuc_vu`) USING BTREE,
  CONSTRAINT `qltl_nhanvien_ibfk_1` FOREIGN KEY (`chuc_vu`) REFERENCES `ns_danhmuc_phongban` (`danhmuc_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qltl_nhanvien
-- ----------------------------
INSERT INTO `qltl_nhanvien` VALUES (2, 'root', '1234', 'Root', 10);
INSERT INTO `qltl_nhanvien` VALUES (3, 'root2', '1234', 'Root2', 10);

-- ----------------------------
-- Table structure for qltl_quyen_sua
-- ----------------------------
DROP TABLE IF EXISTS `qltl_quyen_sua`;
CREATE TABLE `qltl_quyen_sua`  (
  `qltl_nhanvien` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `quyensua_status` int(11) NULL DEFAULT 1,
  `created_at` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `deleted_at` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`qltl_nhanvien`, `item_id`) USING BTREE,
  INDEX `qltl_nhanvien`(`qltl_nhanvien`) USING BTREE,
  INDEX `item_id`(`item_id`) USING BTREE,
  CONSTRAINT `qltl_quyen_sua_ibfk_2` FOREIGN KEY (`qltl_nhanvien`) REFERENCES `qltl_nhanvien` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `qltl_quyen_sua_ibfk_3` FOREIGN KEY (`item_id`) REFERENCES `qltl_items` (`item_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qltl_quyen_sua
-- ----------------------------
INSERT INTO `qltl_quyen_sua` VALUES (2, 5, 1, NULL, NULL);
INSERT INTO `qltl_quyen_sua` VALUES (2, 7, 1, NULL, NULL);
INSERT INTO `qltl_quyen_sua` VALUES (3, 5, 1, NULL, NULL);

-- ----------------------------
-- Table structure for qltl_quyen_xem
-- ----------------------------
DROP TABLE IF EXISTS `qltl_quyen_xem`;
CREATE TABLE `qltl_quyen_xem`  (
  `qltl_nhanvien` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `quyenxem_status` int(11) NULL DEFAULT 1,
  `created_at` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `deleted_at` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`qltl_nhanvien`, `item_id`) USING BTREE,
  INDEX `qltl_nhanvien`(`qltl_nhanvien`) USING BTREE,
  INDEX `item_id`(`item_id`) USING BTREE,
  CONSTRAINT `qltl_quyen_xem_ibfk_2` FOREIGN KEY (`qltl_nhanvien`) REFERENCES `qltl_nhanvien` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `qltl_quyen_xem_ibfk_3` FOREIGN KEY (`item_id`) REFERENCES `qltl_items` (`item_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qltl_quyen_xem
-- ----------------------------
INSERT INTO `qltl_quyen_xem` VALUES (2, 5, 1, NULL, NULL);
INSERT INTO `qltl_quyen_xem` VALUES (3, 5, 1, NULL, NULL);

-- ----------------------------
-- Table structure for tbl_notice
-- ----------------------------
DROP TABLE IF EXISTS `tbl_notice`;
CREATE TABLE `tbl_notice`  (
  `notice_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `notice_text` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`notice_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tbl_notice
-- ----------------------------
INSERT INTO `tbl_notice` VALUES (1, 'Phần mềm quản lý kho hàng và chấm công');

-- ----------------------------
-- Table structure for tbl_users
-- ----------------------------
DROP TABLE IF EXISTS `tbl_users`;
CREATE TABLE `tbl_users`  (
  `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_username` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_company` int(11) NULL DEFAULT 0,
  `user_password` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '123456',
  `user_fullname` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_status` int(11) NULL DEFAULT NULL,
  `user_level` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'AGENT',
  `user_createdate` datetime NULL DEFAULT NULL,
  `user_address` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_officephone` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_phone` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cp_access` int(11) NULL DEFAULT NULL,
  `user_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_longname` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_email` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_zone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_quyen_ketoan` int(11) NULL DEFAULT 0,
  `user_quyen_quanlynhanvien` int(11) NULL DEFAULT 0,
  `user_quyen_dangtai` int(11) NULL DEFAULT 0,
  `user_zone_phuochoa` int(11) NULL DEFAULT 0,
  `user_zone_miendong` int(11) NULL DEFAULT 0,
  `user_zone_phatloc` int(11) NULL DEFAULT 0,
  `user_zone_mientay` int(11) NULL DEFAULT 0,
  `user_zone_phatdat` int(11) NULL DEFAULT 0,
  `user_zone_budang` int(11) NULL DEFAULT 0,
  `user_zone_phuoclong` int(11) NULL DEFAULT 0,
  `user_zone_dongxoai` int(11) NULL DEFAULT 0,
  `user_zone_vungtau` int(11) NULL DEFAULT 0,
  `user_zone_xulyvipham` int(11) NULL DEFAULT 0,
  `user_zone_hosoxe` int(11) NULL DEFAULT 0,
  `user_zone_bunho` int(11) NULL DEFAULT 0,
  `user_zone_kiosk` int(11) NULL DEFAULT 0,
  `user_banhang_kiosk` int(11) NULL DEFAULT 0,
  `user_zone_nhansu` int(11) NULL DEFAULT 0,
  `user_ketoan_kiosk` int(11) NULL DEFAULT 0,
  `user_zone_binhduong` int(11) NULL DEFAULT 0,
  `user_zone_phurieng` int(11) NULL DEFAULT 0,
  `user_zone_phattai` int(11) NULL DEFAULT 0,
  `user_zone_chamcong` int(11) NULL DEFAULT 0,
  `showmap` int(11) NOT NULL DEFAULT 0,
  `lat` decimal(11, 7) NULL DEFAULT NULL,
  `lng` decimal(11, 7) NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'Somewhere',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 17 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tbl_users
-- ----------------------------
INSERT INTO `tbl_users` VALUES (1, 'root', 1, '123456', 'Võ Anh Triết', 1, '0', '2012-05-09 15:08:26', 'Vận hành hệ thống', NULL, '0938.056.558', 1, 'boss', 'Hoang', '-', 'MD', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, 'Somewhere', '2020-06-19 13:44:42', '2023-08-25 01:36:13', NULL);
INSERT INTO `tbl_users` VALUES (2, 'trolygd', 1, '1234', 'Võ Anh Triết', 1, '0', NULL, 'Vận hành hệ thống', NULL, NULL, 1, 'boss', '', '-', 'MD', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, 'Somewhere', NULL, '2023-09-29 14:43:36', NULL);
INSERT INTO `tbl_users` VALUES (10, 'dev', 1, '123456', 'IT', 1, '0', NULL, NULL, NULL, NULL, 1, 'boss', NULL, NULL, 'MD', 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 'Somewhere', NULL, '2023-09-29 13:55:52', NULL);
INSERT INTO `tbl_users` VALUES (11, 'devlead', 1, '123456', 'Hoàng Huy Cảnh', 1, '0', NULL, NULL, NULL, NULL, 1, 'boss', NULL, NULL, 'MD', 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 'Somewhere', NULL, '2023-10-02 10:33:08', NULL);
INSERT INTO `tbl_users` VALUES (13, 'merimag848@gekme.com', 5, '123qwe!@#QWE', NULL, 1, '0', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 'Somewhere', NULL, '2023-10-02 10:32:35', NULL);
INSERT INTO `tbl_users` VALUES (14, 'hhoaang@gmail.com', 6, 'Hoang123456@', NULL, 1, '0', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 'Somewhere', NULL, '2023-10-03 12:27:07', NULL);
INSERT INTO `tbl_users` VALUES (15, 'ciser40601@gekme.com', 7, '123qwe!@#QWE', NULL, 1, '0', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 'Somewhere', NULL, '2023-10-03 13:49:35', NULL);
INSERT INTO `tbl_users` VALUES (16, 'canhu00@yahoo.com', 8, 'VThuTrang80@', NULL, 1, '0', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 'Somewhere', NULL, '2023-10-04 12:36:36', NULL);

-- ----------------------------
-- Table structure for user_preferences
-- ----------------------------
DROP TABLE IF EXISTS `user_preferences`;
CREATE TABLE `user_preferences`  (
  `pref_user` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `pref_name` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `pref_value` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`pref_user`, `pref_name`) USING BTREE,
  INDEX `pref_user`(`pref_user`, `pref_name`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of user_preferences
-- ----------------------------
INSERT INTO `user_preferences` VALUES ('0', 'LOCALE', 'en');
INSERT INTO `user_preferences` VALUES ('0', 'TABVIEW', '1');
INSERT INTO `user_preferences` VALUES ('0', 'SHDATEFORMAT', '%d/%m/%Y');
INSERT INTO `user_preferences` VALUES ('0', 'TIMEFORMAT', '%I:%M:%p');
INSERT INTO `user_preferences` VALUES ('0', 'UISTYLE', 'default');
INSERT INTO `user_preferences` VALUES ('0', 'SHTIMEFORMAT', '%d/%m/%Y %I:%M:%p');
INSERT INTO `user_preferences` VALUES ('0', 'KEY', 'becd99d013b7b1e791061e686d48523c');
INSERT INTO `user_preferences` VALUES ('0', 'ACTIVATION', '');
INSERT INTO `user_preferences` VALUES ('0', 'NEEDRELOAD', 'false');

SET FOREIGN_KEY_CHECKS = 1;
