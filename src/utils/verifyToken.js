const jwt = require("jsonwebtoken");
const { privateKey } = require("../config/config");
const { failAuthenticationCode } = require("../config/response");

// Hàm tạo token
const createToken = (data) => {
  let token = jwt.sign({ content: data }, privateKey, {
    expiresIn: "5d",
    algorithm: "HS256",
  });
  return token;
};

// Hàm check token
const checkToken = (token) => {
  let check = jwt.verify(token, privateKey);
  return check;
};

// Tạo middleware để kiểm tra token, nếu đúng cho truy cập vào dữ liệu

const verifyToken = (req, res, next) => {
  try {
    let token = req.headers.token;
    let checkedToken = checkToken(token);
    if (checkedToken) {
      req.user = checkedToken;
      next();
    }
  } catch (err) {
    failAuthenticationCode(res, err.message);
  }
};

module.exports = {
  createToken,
  checkToken,
  verifyToken,
};
