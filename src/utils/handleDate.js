class CustomDate extends Date {
  constructor(...args) {
    super(...args);
    this.setTime(this.getTime() + 7 * 60 * 60 * 1000); // Thêm điều chỉnh múi giờ +7
  }
}

// format ngày thành yyyy-mm-dd
const formatDateYYYYMMDD = (date) => {
  const day = date.getUTCDate().toString().padStart(2, "0");
  const month = (date.getUTCMonth() + 1).toString().padStart(2, "0");
  const year = date.getUTCFullYear();

  return `${year}-${month}-${day}`;
};
// format đối tượng date thành hh:mm
const formatTimeHHMMSS = (date) => {
  const hours = date.getUTCHours().toString().padStart(2, "0");
  const minutes = date.getUTCMinutes().toString().padStart(2, "0");
  const seconds = date.getUTCSeconds().toString().padStart(2, "0");

  return `${hours}:${minutes}:${seconds}`;
};

module.exports = {
  formatDateYYYYMMDD,
  formatTimeHHMMSS,
  CustomDate,
};
