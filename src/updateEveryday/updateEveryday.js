const schedule = require("node-schedule");
const { updateStatusOfTangCa } = require("../controller/lichTangCaController");
const {
  updateStatusOfCongTac,
} = require("../controller/lichCongTacController");
const { updateStatusOfPhepNghi } = require("../controller/phepNghiController");
//<minute> <hour> <day_of_month> <month> <day_of_week>
const updateEveryday = () => {
  schedule.scheduleJob("0 0 * * *", () => {
    // Gọi hàm cập nhật trạng thái phép nghỉ
    updateStatusOfPhepNghi();
    //Gọi hàm cập nhật trạng thái tăng ca
    updateStatusOfTangCa();
    //Gọi hàm cập nhật trạng thái lịch công tác
    updateStatusOfCongTac();
  });
};

module.exports = updateEveryday;
