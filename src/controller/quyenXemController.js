const { PrismaClient } = require("@prisma/client");

const {
  successCode,
  errorCode,
  failCode,
  notFoundCode,
  failAuthenticationCode,
} = require("../config/response");
const { CustomDate, formatDateYYYYMMDD } = require("../utils/handleDate");

const prisma = new PrismaClient();

// Lấy quyền xem them item
const layQuyenXemTheoItem = async (req, res) => {
  try {
    const item_id = req.params.item_id * 1;

    let data = await prisma.qltl_quyen_xem.findMany({
      where: {
        item_id,
        quyenxem_status: { not: 0 },
      },
    });

    prisma.$disconnect();
    if (data.length > 0) {
      return successCode(res, data, "Lấy quyền xem thành công!");
    } else {
      return failCode(res, "Không có quyền xem!");
    }
  } catch (err) {
    prisma.$disconnect();
    return errorCode(res, err.message);
  }
};
// Thêm quyền xem
const themQuyenXem = async (req, res) => {
  try {
    const item_id = req.params.item_id * 1;
    const { quyenxem_list } = req.body;
    const addedItems = []; // Mảng để lưu các mục đã được thêm

    let item = await prisma.qltl_items.findFirst({
      where: {
        item_status: { not: 0 },
        item_id,
      },
    });
    if (!item) {
      return notFoundCode(res, "Thư mục hoặc file không tồn tại!");
    }
    await Promise.all(
      quyenxem_list.map(async (id) => {
        const checkUser = await prisma.qltl_nhanvien.findFirst({
          where: {
            id,
          },
        });
        if (checkUser) {
          const quyenXem = await prisma.qltl_quyen_xem.findFirst({
            where: {
              qltl_nhanvien: id,
              item_id,
            },
          });

          if (!quyenXem) {
            // Nếu quyền xem chưa tồn tại, thêm nó

            const newQuyenXem = await prisma.qltl_quyen_xem.create({
              data: {
                qltl_nhanvien: id,
                item_id,
              },
            });
            addedItems.push(newQuyenXem);
          } else if (quyenXem.quyenxem_status === 0) {
            // Nếu quyền xem đã tồn tại nhưng bị xoá, cập nhật lại
            const newQuyenXem = await prisma.qltl_quyen_xem.update({
              data: { quyenxem_status: 1 },
              where: {
                qltl_nhanvien_item_id: {
                  qltl_nhanvien: id,
                  item_id,
                },
              },
            });
            addedItems.push(newQuyenXem);
          }
        }
      })
    );

    if (addedItems.length > 0) {
      return successCode(res, addedItems, "Các quyèn đã được thêm thành công!");
    } else {
      return failCode(res, "Không có quyền nào được thêm.");
    }
  } catch (err) {
    prisma.$disconnect();
    return errorCode(res, err.message);
  }
};

// Xoá quyền xem
const xoaQuyenXem = async (req, res) => {
  try {
    const id = req.user.content.id;
    const item_id = req.params.item_id * 1;

    const quyenXem = await prisma.qltl_quyen_xem.findFirst({
      where: {
        qltl_nhanvien: id,
        item_id,
        quyenxem_status: { not: 0 },
      },
    });
    if (!quyenXem) {
      return notFoundCode(res, "Quyền xem không tồn tại!");
    }
    let data = await prisma.qltl_quyen_xem.update({
      data: { quyenxem_status: 0 },
      where: {
        qltl_nhanvien_item_id: {
          qltl_nhanvien: id,
          item_id,
        },
      },
    });

    if (data) {
      prisma.$disconnect();
      return successCode(res, data, "Xoá thành công!");
    } else {
      return failCode(res, "Đã có lỗi, liên hệ quản trị viên!");
    }
  } catch (err) {
    prisma.$disconnect();
    return errorCode(res, err.message);
  }
};

module.exports = {
  layQuyenXemTheoItem,
  themQuyenXem,
  xoaQuyenXem,
};
