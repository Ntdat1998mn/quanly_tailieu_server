const { PrismaClient } = require("@prisma/client");

const {
  successCode,
  errorCode,
  failCode,
  notFoundCode,
  failAuthenticationCode,
} = require("../config/response");
const { CustomDate, formatDateYYYYMMDD } = require("../utils/handleDate");

const prisma = new PrismaClient();

// Lấy quyền sửa theo item
const layQuyenSuaTheoItem = async (req, res) => {
  try {
    const item_id = req.params.item_id * 1;

    let data = await prisma.qltl_quyen_sua.findMany({
      where: {
        item_id,
        quyensua_status: { not: 0 },
      },
    });

    prisma.$disconnect();
    if (data.length > 0) {
      return successCode(res, data, "Lấy quyền sửa thành công!");
    } else {
      return failCode(res, "Không có quyền sửa!");
    }
  } catch (err) {
    prisma.$disconnect();
    return errorCode(res, err.message);
  }
};

// Thêm quyền sửa
const themQuyenSua = async (req, res) => {
  try {
    const item_id = req.params.item_id * 1;
    const { quyensua_list } = req.body;
    const addedItems = []; // Mảng để lưu các mục đã được thêm

    let item = await prisma.qltl_items.findFirst({
      where: {
        item_status: { not: 0 },
        item_id,
      },
    });
    if (!item) {
      return notFoundCode(res, "Thư mục hoặc file không tồn tại!");
    }
    await Promise.all(
      quyensua_list.map(async (id) => {
        const quyenSua = await prisma.qltl_quyen_sua.findFirst({
          where: {
            qltl_nhanvien: id,
            item_id,
          },
        });
        const checkUser = await prisma.qltl_nhanvien.findFirst({
          where: {
            id,
          },
        });
        if (checkUser) {
          if (!quyenSua) {
            // Nếu quyền sửa chưa tồn tại, thêm nó
            const newQuyenSua = await prisma.qltl_quyen_sua.create({
              data: {
                qltl_nhanvien: id,
                item_id,
              },
            });
            addedItems.push(newQuyenSua);
          } else if (quyenSua.quyensua_status === 0) {
            // Nếu quyền sửa đã tồn tại nhưng bị xoá, cập nhật lại
            const newQuyenSua = await prisma.qltl_quyen_sua.update({
              data: { quyensua_status: 1 },
              where: {
                qltl_nhanvien_item_id: {
                  qltl_nhanvien: id,
                  item_id,
                },
              },
            });
            addedItems.push(newQuyenSua);
          }
        }
      })
    );

    if (addedItems.length > 0) {
      return successCode(res, addedItems, "Các quyèn đã được thêm thành công!");
    } else {
      return failCode(res, "Không có quyền nào được thêm.");
    }
  } catch (err) {
    prisma.$disconnect();
    return errorCode(res, err.message);
  }
};

// Xoá quyền sửa
const xoaQuyenSua = async (req, res) => {
  try {
    const id = req.user.content.id;
    const item_id = req.params.item_id * 1;

    const quyenSua = await prisma.qltl_quyen_sua.findFirst({
      where: {
        qltl_nhanvien: id,
        item_id,
        quyensua_status: { not: 0 },
      },
    });
    if (!quyenSua) {
      return notFoundCode(res, "Quyền sửa không tồn tại!");
    }
    let data = await prisma.qltl_quyen_sua.update({
      data: { quyensua_status: 0 },
      where: {
        qltl_nhanvien_item_id: {
          qltl_nhanvien: id,
          item_id,
        },
      },
    });

    if (data) {
      prisma.$disconnect();
      return successCode(res, data, "Xoá thành công!");
    } else {
      return failCode(res, "Đã có lỗi, liên hệ quản trị viên!");
    }
  } catch (err) {
    prisma.$disconnect();
    return errorCode(res, err.message);
  }
};

module.exports = {
  layQuyenSuaTheoItem,
  themQuyenSua,
  xoaQuyenSua,
};
