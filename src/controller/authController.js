const { PrismaClient } = require("@prisma/client");
const { createToken } = require("../utils/verifyToken");
const { successCode, errorCode, failCode } = require("../config/response");
const prisma = new PrismaClient();

// Đăng nhập
const logIn = async (req, res) => {
  try {
    let { password, username } = req.body;
    let data = await prisma.qltl_nhanvien.findFirst({
      where: {
        username,
      },
      select: {
        id: true,
        name: true,
        chuc_vu: true,
        username: true,
        password: true,
        ns_danhmuc_phongban: {
          select: {
            danhmuc_id: true,
            danhmuc_name: true,
            thong_tin_company: {
              select: {
                company_id: true,
                company_name: true,
              },
            },
          },
        },
      },
    });
    prisma.$disconnect();
    if (data) {
      if (password == data.password) {
        let token = createToken(data);
        const { password, ...content } = data;
        return successCode(res, { ...content, token }, "Đăng nhập thành công!");
      } else {
        password = "";
        return failCode(res, "Mật khẩu không đúng!");
      }
    } else {
      return failCode(res, "Tài khoản không tồn tại!");
    }
  } catch (err) {
    prisma.$disconnect();
    return errorCode(res, err.message);
  }
};

module.exports = { logIn };
