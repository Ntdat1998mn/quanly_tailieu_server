const { PrismaClient } = require("@prisma/client");

const {
  successCode,
  errorCode,
  failCode,
  notFoundCode,
  failAuthenticationCode,
} = require("../config/response");
const { CustomDate, formatDateYYYYMMDD } = require("../utils/handleDate");

const prisma = new PrismaClient();

// Lấy item
const layTaiLieu = async (req, res) => {
  try {
    const id = req.user.content.id;
    const getAllItems = async (parentId) => {
      const items = await prisma.qltl_items.findMany({
        where: {
          parent_id: parentId,
          item_status: { not: 0 },
          qltl_quyen_xem: {
            some: {
              qltl_nhanvien: id,
            },
          },
        },
        include: {
          qltl_quyen_xem: {
            include: {
              thong_tin_nhan_vien: {
                select: { id: true, name: true },
              },
            },
          },
        },
      });

      if (items.length === 0) {
        return [];
      }

      const children = await Promise.all(
        items.map(async (item) => {
          const subItems = await getAllItems(item.item_id);
          return {
            ...item,
            children: subItems,
          };
        })
      );

      return children;
    };

    const data = await getAllItems(null);
    prisma.$disconnect;
    if (data.length !== 0) {
      return successCode(res, data, "Lấy tài liệu thành công!");
    } else {
      return failCode(res, "Không có tài liệu nào!");
    }
  } catch (err) {
    prisma.$disconnect();
    return errorCode(res, err.message);
  }
};
// Lấy toàn bộ file theo 1 mặt phẳng
const layToanBoFile = async (req, res) => {
  try {
    const id = req.user.content.id;

    const data = await prisma.qltl_items.findMany({
      where: {
        item_isDirectory: 0,
        item_status: { not: 0 },
        qltl_quyen_xem: {
          some: {
            qltl_nhanvien: id,
          },
        },
      },
      include: {
        qltl_quyen_xem: {
          include: {
            thong_tin_nhan_vien: {
              select: { id: true, name: true },
            },
          },
        },
      },
    });

    prisma.$disconnect;
    if (data.length !== 0) {
      return successCode(res, data, "Lấy file thành công!");
    } else {
      return failCode(res, "Không có file nào!");
    }
  } catch (err) {
    prisma.$disconnect();
    return errorCode(res, err.message);
  }
};
// Lấy item theo id
const getItemById = async (req, res) => {
  try {
    const id = req.user.content.id;
    const item_id = req.params.item_id * 1;

    const item = await prisma.qltl_items.findFirst({
      where: {
        item_id,
        item_status: { not: 0 },
        qltl_quyen_xem: {
          some: {
            qltl_nhanvien: id,
          },
        },
      },
      include: {
        qltl_quyen_xem: {
          include: {
            thong_tin_nhan_vien: {
              select: { id: true, name: true },
            },
          },
        },
      },
    });

    prisma.$disconnect;
    if (item) {
      return successCode(res, item, "Lấy tài liệu thành công!");
    } else {
      return failCode(res, "Không có tài liệu nào!");
    }
  } catch (err) {
    prisma.$disconnect();
    return errorCode(res, err.message);
  }
};

// Thêm folder
const addFolder = async (req, res) => {
  try {
    const id = req.user.content.id;
    let { item_name, parent_id } = req.body;
    item_name = item_name.trim();
    if (parent_id) {
      parent_id *= 1;
      // kiểm tra quyền truy cập của người dùng
      const item = await prisma.qltl_items.findFirst({
        where: {
          item_id: parent_id,
          item_status: { not: 0 },
          qltl_quyen_xem: {
            some: {
              qltl_nhanvien: id,
            },
          },
        },
      });
      if (!item) {
        return notFoundCode(
          res,
          "Thư mục không tồn tại hoặc người dùng không có quyền truy cập vào thư mục này!"
        );
      }
    } else parent_id = null;

    if (item_name == "") {
      return failCode(res, "Tên thư mục không được để trống");
    }
    // Kiểm tra trùng tên
    let folderSameName = await prisma.qltl_items.findFirst({
      where: {
        item_name,
        parent_id,
        item_isDirectory: 1,
        item_status: { not: 0 },
      },
    });
    if (folderSameName) {
      prisma.$disconnect();
      return failCode(res, "Tên thư mục không được trùng!");
    }
    // Tại thư mục
    data = await prisma.qltl_items.create({
      data: {
        qltl_nhanvien: id,
        parent_id,
        item_name,
      },
    });
    // Tạo quyền xem cho người tạo để dễ quản lý
    let quyenXem = await prisma.qltl_quyen_xem.create({
      data: {
        qltl_nhanvien: id,
        item_id: data.item_id,
      },
    });
    // Tạo quyền sửa cho người tạo để dễ quản lý
    let quyenSua = await prisma.qltl_quyen_sua.create({
      data: {
        qltl_nhanvien: id,
        item_id: data.item_id,
      },
    });
    prisma.$disconnect;
    if (!data) {
      return failCode(res, "Đã có lỗi, vui lòng liên hệ quản trị viên!");
    } else if (!quyenXem | !quyenSua) {
      return failCode(res, "Tạo quyền cho người tạo thư mục thất bại!");
    }
    {
      return successCode(res, data, "Thêm thư mục thành công!");
    }
  } catch (err) {
    prisma.$disconnect();
    return errorCode(res, err.message);
  }
};

// Thêm folder
const addFile = async (req, res) => {
  try {
    const id = req.user.content.id;
    let file = req.file;
    let fileInfor = JSON.parse(req.body.fileInfor);

    if (!file) {
      return failCode(res, "Không có file nào được tải lên!");
    }
    /* const baseUrl = `${req.protocol}://${req.get("host")}`; */

    const extension = file.originalname.slice(
      file.originalname.lastIndexOf(".") + 1
    );

    const inputData = {
      qltl_nhanvien: id,
      parent_id: fileInfor.parent_id * 1,
      item_isDirectory: 0,
      file_type: extension,
      item_name: file.originalname,
      link_file: `${file.destination}/${file.filename}`,
    };
    let data = await prisma.qltl_items.create({
      data: inputData,
    });
    // Tạo quyền xem cho người tạo để dễ quản lý
    let quyenXem = await prisma.qltl_quyen_xem.create({
      data: {
        qltl_nhanvien: id,
        item_id: data.item_id,
      },
    });
    // Tạo quyền sửa cho người tạo để dễ quản lý
    let quyenSua = await prisma.qltl_quyen_sua.create({
      data: {
        qltl_nhanvien: id,
        item_id: data.item_id,
      },
    });
    prisma.$disconnect();
    if (data) {
      return successCode(res, data, "Thêm file thành công!");
    } else if (!quyenXem | !quyenSua) {
      return failCode(res, "Tạo quyền cho người tạo thư mục thất bại!");
    } else {
      return failCode(res, "Đã có lỗi, liên hệ quản trị viên!");
    }
    /* inputData.nv_avatar = baseUrl + "/" + inputData.nv_avatar; */
  } catch (err) {
    prisma.$disconnect();
    return errorCode(res, err.message);
  }
};

// Xoá item
const deleteItem = async (req, res) => {
  try {
    const id = req.user.content.id;
    const item_id = req.params.item_id * 1;
    // Kiểm tra tồn tại
    let folder = await prisma.qltl_items.findFirst({
      where: {
        item_isDirectory: 1,
        item_status: { not: 0 },
        item_id: item_id,
      },
    });
    if (!folder) {
      return notFoundCode(res, "Thư mục hoặc file không tồn tại!");
    }
    let data = await prisma.qltl_items.update({
      data: { item_status: 0 },
      where: { item_id, qltl_nhanvien: id },
    });

    if (data) {
      prisma.$disconnect();
      return successCode(res, data, "Xoá thành công!");
    } else {
      return failCode(res, "Đã có lỗi, liên hệ quản trị viên!");
    }

    /*     // Tạo quyền xem cho người tạo để dễ quản lý
    let quyenXem = await prisma.qltl_quyen_xem.create({
      data: {
        qltl_nhanvien: nv_id,
        items_id: data.id,
      },
    }); */
  } catch (err) {
    prisma.$disconnect();
    return errorCode(res, err.message);
  }
};

// Đổi tên folder
const changeNameFolder = async (req, res) => {
  try {
    const id = req.user.content.id;
    const { item_name } = req.body;
    const item_id = req.params.item_id * 1;
    if (item_name == "") {
      return failCode(res, "Tên thư mục không được để trống");
    }
    // Kiểm tra tồn tại
    let folder = await prisma.qltl_items.findFirst({
      where: {
        item_isDirectory: 1,
        item_status: { not: 0 },
        item_id: item_id,
      },
    });
    if (!folder) {
      return notFoundCode(res, "Thư mục không tồn tại!");
    }
    // Kiểm tra trùng tên
    let folderSameName = await prisma.qltl_items.findFirst({
      where: {
        item_name,
        parent_id: folder.parent_id,
        item_isDirectory: 1,
        item_status: { not: 0 },
        item_id: { not: item_id },
      },
    });
    if (folderSameName) {
      prisma.$disconnect();
      return failCode(res, "Tên thư mục không được trùng!");
    }
    let data = await prisma.qltl_items.update({
      data: { item_name },
      where: { item_id, qltl_nhanvien: id, item_isDirectory: 1 },
    });

    if (data) {
      prisma.$disconnect();
      return successCode(res, data, "Đổi tên thư mục thành công!");
    } else {
      return failCode(res, "Đã có lỗi, liên hệ quản trị viên!");
    }
  } catch (err) {
    prisma.$disconnect();
    return errorCode(res, err.message);
  }
};

module.exports = {
  layTaiLieu,
  addFolder,
  deleteItem,
  addFile,
  changeNameFolder,
  layToanBoFile,
  getItemById,
};
