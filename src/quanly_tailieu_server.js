const express = require("express");
const app = express();
app.use(express.json());

app.use(express.static("."));

const cors = require("cors");
app.use(cors());

app.listen(3007);
const rootRoute = require("./Routes/rootRoute");
/* const updateEveryday = require("./updateEveryday/updateEveryday");
updateEveryday(); */
app.use("/api/quanly_tailieu", rootRoute);
