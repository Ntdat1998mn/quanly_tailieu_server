const express = require("express");
const authRoute = require("./authRoute");
const itemsRoute = require("./itemsRoute");
const quyenXemRoute = require("./quyenXemRoute");
const quyenSuaRoute = require("./quyenSuaRoute");

const rootRoute = express.Router();

rootRoute.use("/auth", authRoute);
rootRoute.use("/items", itemsRoute);
rootRoute.use("/quyen-xem", quyenXemRoute);
rootRoute.use("/quyen-sua", quyenSuaRoute);

module.exports = rootRoute;
