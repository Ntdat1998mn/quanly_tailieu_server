const express = require("express");
const {
  layTaiLieu,
  addFolder,
  deleteItem,
  changeNameFolder,
  addFile,
  getItemById,
  layToanBoFile,
} = require("../controller/itemsController");
const { verifyToken } = require("../utils/verifyToken");
const { upload } = require("../utils/upload");

const itemsRoute = express.Router();

itemsRoute.get("/lay-item-list", verifyToken, layTaiLieu);
itemsRoute.get("/lay-item-theo-id/:item_id", verifyToken, getItemById);
itemsRoute.get("/lay-file-list", verifyToken, layToanBoFile);
itemsRoute.post("/them-thu-muc", verifyToken, addFolder);
itemsRoute.put("/doi-ten-thu-muc/:item_id", verifyToken, changeNameFolder);
itemsRoute.post("/them-file", upload.single("file"), verifyToken, addFile);
itemsRoute.put("/xoa-item/:item_id", verifyToken, deleteItem);

module.exports = itemsRoute;
