const express = require("express");

const { verifyToken } = require("../utils/verifyToken");
const {
  themQuyenXem,
  xoaQuyenXem,
  layQuyenXemTheoItem,
} = require("../controller/quyenXemController");

const quyenXemRoute = express.Router();

quyenXemRoute.get(
  "/lay-quyen-xem-theo-item/:item_id",
  verifyToken,
  layQuyenXemTheoItem
);
quyenXemRoute.post("/them-quyen-xem/:item_id", verifyToken, themQuyenXem);
quyenXemRoute.put("/xoa-quyen-xem/:item_id", verifyToken, xoaQuyenXem);

module.exports = quyenXemRoute;
