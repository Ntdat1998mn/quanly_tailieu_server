const express = require("express");

const { verifyToken } = require("../utils/verifyToken");
const {
  themQuyenSua,
  xoaQuyenSua,
  layQuyenSuaTheoItem,
} = require("../controller/quyenSuaController");

const quyenSuaRoute = express.Router();

quyenSuaRoute.get(
  "/lay-quyen-sua-theo-item/:item_id",
  verifyToken,
  layQuyenSuaTheoItem
);
quyenSuaRoute.post("/them-quyen-sua/:item_id", verifyToken, themQuyenSua);
quyenSuaRoute.put("/xoa-quyen-sua/:item_id", verifyToken, xoaQuyenSua);

module.exports = quyenSuaRoute;
